﻿using EposNowAPI.Architecture.Attributes;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace EposNowAPI.Core.Json
{
    public class PrimaryKeyContractResolver : DefaultContractResolver
    {
        public PrimaryKeyContractResolver()
        {
        }

        protected override List<MemberInfo> GetSerializableMembers(Type objectType)
        {
            List<MemberInfo> membersToSerialize = base.GetSerializableMembers(objectType);
            membersToSerialize.Remove(membersToSerialize.FirstOrDefault(it => it.GetCustomAttributes().FirstOrDefault(attr => attr.GetType() == typeof(EposID)) != null));
            return membersToSerialize;
        }
    }
}
