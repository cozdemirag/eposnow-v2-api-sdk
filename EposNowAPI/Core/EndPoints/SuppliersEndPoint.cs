using EposNowAPI.Architecture.BaseClasses;
using EposNowAPI.Architecture.Extensions;
using EposNowAPI.Architecture.Interfaces;
using EposNowAPI.Core.Responses;
using EposNowAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace EposNowAPI.Core.EndPoints
{
    public sealed class SuppliersEndPoint : EndPointBase<SuppliersEndPoint, Supplier> 
	, IUpdateEndPoint<Supplier>
	, IDeleteEndPoint<Supplier>
	, ICreateEndPoint<Supplier>
    {
        private SuppliersResponse Response { get; set; }

        public SuppliersEndPoint(Authenticator auth) : base()
        {
            Response = new SuppliersResponse(auth);
        }

        public Supplier Create(Supplier item)
        {
            return Response.Create(item);
        }
		public Supplier Update(Supplier item)
        {
            return Response.Update(item);
        }
		public bool Delete(int id)
        {
            return Response.Delete(id);
        }

		public bool Delete(Supplier item)
        {
            return Response.Delete(item);
        }
		
        public override Supplier Find(Int32 id)
        {
            return Response.Find(id);
        }

        public override IEnumerable<Supplier> Find()
        {
            IEnumerable<Supplier> listToReturn = Response.Find(QueryParameters);
            QueryParameters.Reset();
            return listToReturn;
        }

        public override IEnumerable<Supplier> FindAll()
        {
			QueryParameters.GetAll = true;
            QueryParameters.PageNo = 1;
            IEnumerable<Supplier> listToReturn = Response.FindAll(QueryParameters);
            QueryParameters.Reset();
            return listToReturn;
        }

        public override SuppliersEndPoint Where(Expression<Func<Supplier, Boolean>> expression)
        {
            QueryParameters.Where.AddRange(expression.ToEposQueries());
            return this;
        }

        public override SuppliersEndPoint And(Expression<Func<Supplier, Boolean>> expression)
        {
            return Where(expression);
        }

        public override SuppliersEndPoint OrderBy<T>(Expression<Func<Supplier, T>> expression)
        {
            QueryParameters.Order = expression.ToEposOrderByQuery();
            return this;
        }

        public override SuppliersEndPoint OrderByDesc<T>(Expression<Func<Supplier, T>> expression)
        {
            QueryParameters.Order = expression.ToEposOrderByQuery(true);
            return this;
        }

        public override SuppliersEndPoint Page(Int32 pageNo)
        {
            QueryParameters.PageNo = pageNo;
            return this;
        }
    }
}
