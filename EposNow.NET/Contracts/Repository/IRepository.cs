﻿using EposNowNET.Contracts.Entities;

namespace EposNowNET.Contracts.Repository
{
    public interface IRepository<T> where T : class, IEposEntity
    {
    }
}
