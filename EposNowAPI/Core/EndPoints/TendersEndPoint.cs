using EposNowAPI.Architecture.BaseClasses;
using EposNowAPI.Architecture.Extensions;
using EposNowAPI.Architecture.Interfaces;
using EposNowAPI.Core.Responses;
using EposNowAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace EposNowAPI.Core.EndPoints
{
    public sealed class TendersEndPoint : EndPointBase<TendersEndPoint, Tender> 
	, IUpdateEndPoint<Tender>
	, IDeleteEndPoint<Tender>
	, ICreateEndPoint<Tender>
    {
        private TendersResponse Response { get; set; }

        public TendersEndPoint(Authenticator auth) : base()
        {
            Response = new TendersResponse(auth);
        }

        public Tender Create(Tender item)
        {
            return Response.Create(item);
        }
		public Tender Update(Tender item)
        {
            return Response.Update(item);
        }
		public bool Delete(int id)
        {
            return Response.Delete(id);
        }

		public bool Delete(Tender item)
        {
            return Response.Delete(item);
        }
		
        public override Tender Find(Int32 id)
        {
            return Response.Find(id);
        }

        public override IEnumerable<Tender> Find()
        {
            IEnumerable<Tender> listToReturn = Response.Find(QueryParameters);
            QueryParameters.Reset();
            return listToReturn;
        }

        public override IEnumerable<Tender> FindAll()
        {
			QueryParameters.GetAll = true;
            QueryParameters.PageNo = 1;
            IEnumerable<Tender> listToReturn = Response.FindAll(QueryParameters);
            QueryParameters.Reset();
            return listToReturn;
        }

        public override TendersEndPoint Where(Expression<Func<Tender, Boolean>> expression)
        {
            QueryParameters.Where.AddRange(expression.ToEposQueries());
            return this;
        }

        public override TendersEndPoint And(Expression<Func<Tender, Boolean>> expression)
        {
            return Where(expression);
        }

        public override TendersEndPoint OrderBy<T>(Expression<Func<Tender, T>> expression)
        {
            QueryParameters.Order = expression.ToEposOrderByQuery();
            return this;
        }

        public override TendersEndPoint OrderByDesc<T>(Expression<Func<Tender, T>> expression)
        {
            QueryParameters.Order = expression.ToEposOrderByQuery(true);
            return this;
        }

        public override TendersEndPoint Page(Int32 pageNo)
        {
            QueryParameters.PageNo = pageNo;
            return this;
        }
    }
}
