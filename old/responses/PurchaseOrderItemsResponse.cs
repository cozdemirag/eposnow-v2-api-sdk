using EposNowAPI.Models;
using System;
using System.Collections.Generic;
using EposNowAPI.Architecture.Interfaces;

namespace EposNowAPI.Core.Responses
{
    public class PurchaseOrderItemsResponse : EposNowResponse<PurchaseOrderItem>, IEndPoint<PurchaseOrderItem>
    {
        public PurchaseOrderItemsResponse() : base() { }

        public PurchaseOrderItemsResponse(Authenticator auth)
            : base(auth)
        {

        }

        public new PurchaseOrderItem Create(PurchaseOrderItem itemToCreate)
        {
            return base.Create<PurchaseOrderItemsResponse>(itemToCreate);
        }

        public new PurchaseOrderItem Update(PurchaseOrderItem itemToUpdate)
        {
            return base.Update<PurchaseOrderItemsResponse>(itemToUpdate);
        }

        public bool Delete(PurchaseOrderItem item)
        {
            return base.Delete<PurchaseOrderItemsResponse>(item);
        }

        public new IEnumerable<PurchaseOrderItem> Get()
        {
            return base.Get<PurchaseOrderItemsResponse>();
        }

        public new IEnumerable<PurchaseOrderItem> Get(Int32 id)
        {
            return base.Get<PurchaseOrderItemsResponse>(id);
        }

        public IEnumerable<PurchaseOrderItem> Get(Int32 page, String where, String order)
        {
            return base.Get<PurchaseOrderItemsResponse>(page, where, order);
        }

        public IEnumerable<PurchaseOrderItem> Get<TKey1, TKey2>(System.Linq.Expressions.Expression<Func<PurchaseOrderItem, TKey1>> whereClause = null, System.Linq.Expressions.Expression<Func<PurchaseOrderItem, TKey2>> orderByClause = null)
        {
            return base.Get<PurchaseOrderItemsResponse, TKey1, TKey2>(whereClause, orderByClause);
        }
    }
}
