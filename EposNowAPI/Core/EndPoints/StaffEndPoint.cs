using EposNowAPI.Architecture.BaseClasses;
using EposNowAPI.Architecture.Extensions;
using EposNowAPI.Architecture.Interfaces;
using EposNowAPI.Core.Responses;
using EposNowAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace EposNowAPI.Core.EndPoints
{
    public sealed class StaffEndPoint : EndPointBase<StaffEndPoint, Staff> 
	, IUpdateEndPoint<Staff>
	, IDeleteEndPoint<Staff>
	, ICreateEndPoint<Staff>
    {
        private StaffResponse Response { get; set; }

        public StaffEndPoint(Authenticator auth) : base()
        {
            Response = new StaffResponse(auth);
        }

        public Staff Create(Staff item)
        {
            return Response.Create(item);
        }
		public Staff Update(Staff item)
        {
            return Response.Update(item);
        }
		public bool Delete(int id)
        {
            return Response.Delete(id);
        }

		public bool Delete(Staff item)
        {
            return Response.Delete(item);
        }
		
        public override Staff Find(Int32 id)
        {
            return Response.Find(id);
        }

        public override IEnumerable<Staff> Find()
        {
            IEnumerable<Staff> listToReturn = Response.Find(QueryParameters);
            QueryParameters.Reset();
            return listToReturn;
        }

        public override IEnumerable<Staff> FindAll()
        {
			QueryParameters.GetAll = true;
            QueryParameters.PageNo = 1;
            IEnumerable<Staff> listToReturn = Response.FindAll(QueryParameters);
            QueryParameters.Reset();
            return listToReturn;
        }

        public override StaffEndPoint Where(Expression<Func<Staff, Boolean>> expression)
        {
            QueryParameters.Where.AddRange(expression.ToEposQueries());
            return this;
        }

        public override StaffEndPoint And(Expression<Func<Staff, Boolean>> expression)
        {
            return Where(expression);
        }

        public override StaffEndPoint OrderBy<T>(Expression<Func<Staff, T>> expression)
        {
            QueryParameters.Order = expression.ToEposOrderByQuery();
            return this;
        }

        public override StaffEndPoint OrderByDesc<T>(Expression<Func<Staff, T>> expression)
        {
            QueryParameters.Order = expression.ToEposOrderByQuery(true);
            return this;
        }

        public override StaffEndPoint Page(Int32 pageNo)
        {
            QueryParameters.PageNo = pageNo;
            return this;
        }
    }
}
