using EposNowAPI.Models;
using System;
using System.Collections.Generic;
using EposNowAPI.Architecture.Interfaces;

namespace EposNowAPI.Core.Responses
{
    public class MultipleChoiceProductGroupsResponse : EposNowResponse<MultipleChoiceProductGroup>, IEndPoint<MultipleChoiceProductGroup>
    {
        public MultipleChoiceProductGroupsResponse() : base() { }

        public MultipleChoiceProductGroupsResponse(Authenticator auth)
            : base(auth)
        {

        }

        public new MultipleChoiceProductGroup Create(MultipleChoiceProductGroup itemToCreate)
        {
            return base.Create<MultipleChoiceProductGroupsResponse>(itemToCreate);
        }

        public new MultipleChoiceProductGroup Update(MultipleChoiceProductGroup itemToUpdate)
        {
            return base.Update<MultipleChoiceProductGroupsResponse>(itemToUpdate);
        }

        public bool Delete(MultipleChoiceProductGroup item)
        {
            return base.Delete<MultipleChoiceProductGroupsResponse>(item);
        }

        public new IEnumerable<MultipleChoiceProductGroup> Get()
        {
            return base.Get<MultipleChoiceProductGroupsResponse>();
        }

        public new IEnumerable<MultipleChoiceProductGroup> Get(Int32 id)
        {
            return base.Get<MultipleChoiceProductGroupsResponse>(id);
        }

        public IEnumerable<MultipleChoiceProductGroup> Get(Int32 page, String where, String order)
        {
            return base.Get<MultipleChoiceProductGroupsResponse>(page, where, order);
        }

        public IEnumerable<MultipleChoiceProductGroup> Get<TKey1, TKey2>(System.Linq.Expressions.Expression<Func<MultipleChoiceProductGroup, TKey1>> whereClause = null, System.Linq.Expressions.Expression<Func<MultipleChoiceProductGroup, TKey2>> orderByClause = null)
        {
            return base.Get<MultipleChoiceProductGroupsResponse, TKey1, TKey2>(whereClause, orderByClause);
        }
    }
}
