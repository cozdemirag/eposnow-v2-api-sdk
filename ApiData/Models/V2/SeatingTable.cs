﻿using System;
using ApiData.Attributes;

namespace ApiData.Models
{
    [EposModel("SeatingTable", "SeatingTables")]
    public class SeatingTable
    {
        [EposID("TableID")]
        public Int32 TableID { get; set; }
        public String Name { get; set; }
        public Int32 Seats { get; set; }
        public Int32 SeatingAreaID { get; set; }
        public Int32 PosX { get; set; }
        public Int32 PosY { get; set; }
    }
}
