﻿using EposNow.NET.Contracts.Expressions.Translation.Rules;
using System.Linq.Expressions;

namespace EposNow.NET.Core.Rules.VisitBinary
{
    public class VisitBinaryLeftMemberRightCallRule : TranslationRule
    {
        public override bool IsMatch(Expression left, Expression origin = null, Expression right = null)
        {
            return origin != null && (origin.NodeType != ExpressionType.AndAlso && origin.NodeType != ExpressionType.And && origin.NodeType != ExpressionType.OrElse && origin.NodeType != ExpressionType.Or) &&
                    left.NodeType == ExpressionType.MemberAccess && right.NodeType == ExpressionType.Call;
        }

        public override object Translate(Expression left, Expression origin = null, Expression right = null)
        {
            MemberExpression leftMember = (MemberExpression)left;
            return TranslateIntoEposQuery(leftMember.Member.Name, TranslateNodeType(origin.NodeType), Evaluate(right as MethodCallExpression));
        }
    }
}
