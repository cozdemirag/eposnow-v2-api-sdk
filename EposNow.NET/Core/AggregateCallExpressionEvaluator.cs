﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace EposNow.NET.Core
{
    public class AggregateCallExpressionEvaluator : ExpressionVisitor
    {
        private string Query { get; set; }
        private List<string> AllowedMethodNames = new List<string>() { "Contains", "StartsWith", "EndsWith" };
        public bool CanEvaluate(Expression exp)
        {
            var method = exp as MethodCallExpression;
            var member = method?.Object as MemberExpression;
            return method != null && AllowedMethodNames.Contains(method.Method.Name) && member != null && member.Type == typeof(string) && member.Member.DeclaringType.Name == "Product";
        }


        public string EvaluateExpression(Expression exp)
        {
            this.Query = string.Empty;

            this.Visit(exp);

            return this.Query;
        }


        protected override Expression VisitMethodCall(MethodCallExpression method)
        {
            var member = method.Object as MemberExpression;
            var value = method.Arguments[0] as ConstantExpression;
            this.Query = $"search=({member.Member.Name}|{method.Method.Name}|{value.Value})";
            return method;
        }
    }
}
