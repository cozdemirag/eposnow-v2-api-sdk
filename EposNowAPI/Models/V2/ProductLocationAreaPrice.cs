﻿using System;
using EposNowAPI.Architecture.Attributes;

namespace EposNowAPI.Models
{
    [EposModel("ProductLocationAreaPrice", "ProductLocationAreaPrices")]
    public class ProductLocationAreaPrice
    {
        [EposID("ProductLocationAreaPriceID")]
        public Int32 ProductLocationAreaPriceID { get; set; }
        public Int32 ProductID { get; set; }
        public Int32 LocationAreaID { get; set; }
        public Decimal SalePrice { get; set; }
        public Decimal EatOutPrice { get; set; }
    }
}
