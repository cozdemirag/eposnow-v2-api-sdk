﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EposNowAPI
{
    public class Authenticator
    {
        public String AuthenticationToken { get; set; }
        public String BaseUrl { get; set; }

        public String AutherizationHeader { get { return String.Format("Basic {0}", AuthenticationToken); } }
    }
}
