﻿using ApiData.Attributes;

namespace ApiData.Models.Constants
{
    public enum EatOutType
    {
        [EnumValue(0)]
        WalkIn, 
        [EnumValue(1)]
        TakeAway,
        [EnumValue(2)]
        Delivery
    }
}
