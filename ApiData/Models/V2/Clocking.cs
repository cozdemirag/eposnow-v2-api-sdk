﻿using System;
using ApiData.Attributes;

namespace ApiData.Models
{
    [EposModel("Clocking", "Clockings")]
    public class Clocking
    {
        [EposID("ClockingID")]
        public Int32 ClockingID { get; set; }
        public Int32 StaffID { get; set; }
        public DateTime InTime { get; set; }
        public DateTime? OutTime { get; set; }
        public Decimal? HoursWorked { get; set; }
        public String Notes { get; set; }
        public Int32? ClockingTypeID { get; set; }
    }
}
