﻿using ApiData.Attributes;
using ApiData.BaseClasses;
using System;

namespace ApiData.Models
{
    [EposModel("EndOfDayReferenceCode", "EndOfDayReferenceCodes")]
    public class EndOfDayReferenceCode : ModelBase
    {
        [EposID("EndOfDayReferenceCodeID")]
        public Int32 EndOfDayReferenceCodeID { get; set; }
        public Int32 EndOfDayID { get; set; }
        public Int32 AppID { get; set; }
        public String ReferenceCode { get; set; }
    }
}
