﻿using EposNowAPI.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Linq.Expressions;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using EposNowAPI.Architecture.Extensions;
using System.Threading;

namespace EposNowAPI.Core
{
    public class EposNowProvider : IQueryProvider
    {
        private Authenticator Authenticator { get; set; }

        public EposNowProvider()
        {
            // Read From App.Config
        }

        public EposNowProvider(Authenticator auth)
        {
            Authenticator = auth;
        }

        public IQueryable CreateQuery(Expression expression)
        {
            return (IQueryable)Activator.CreateInstance(typeof(EposNowQuery<>).MakeGenericType(FindModelType(expression)), new object[] { this, expression });
        }

        public IQueryable<TElement> CreateQuery<TElement>(Expression expression)
        {
            return new EposNowQuery<TElement>(this, expression);
        }

        public object Execute(Expression expression)
        {
            return GetType().GetMethod("Execute", new[] { FindModelType(expression) }).Invoke(this, new object[] { expression });
        }

        public TResult Execute<TResult>(Expression expression)
        {
            return MakeRequest<TResult>("", string.Join("&", expression.ToEposQueries()));
        }

        private Type FindModelType(Expression expression)
        {
            return expression.Type.GenericTypeArguments[0];
        }


        // HTTP Methods
        private TResult MakeRequest<TResult>(String endPointUrl, string urlParams)
        {
            var result = default(TResult);
            using (HttpClient client = new HttpClient(new TestHandler(new HttpClientHandler())))
            {
                client.BaseAddress = new Uri(Authenticator.BaseUrl);
                client.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Basic", Authenticator.AuthenticationToken);
                var resultString = client.GetStringAsync($"Product?{urlParams}").Result; 
                result = JsonConvert.DeserializeObject<TResult>(FixContentString(resultString));
            }

            return result;
        }

        private String FixContentString(String input)
        {
            if (!input.StartsWith("[") && !input.EndsWith("]"))
            {
                input = "[" + input + "]";
            }

            return input;
        }

    }

    public class TestHandler : DelegatingHandler
    {
        public TestHandler(HttpMessageHandler innerHandler) : base(innerHandler)
        {
        }

        protected override Task<HttpResponseMessage> SendAsync(HttpRequestMessage request, CancellationToken cancellationToken)
        {
            Console.WriteLine(request.RequestUri);
            Console.WriteLine(request.Content);

            return base.SendAsync(request, cancellationToken);
        }
    }
}
