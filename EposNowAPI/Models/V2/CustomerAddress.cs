﻿using System;
using EposNowAPI.Architecture.Attributes;

namespace EposNowAPI.Models
{
    [EposModel("CustomerAddress", "CustomerAddresses")]
    public class CustomerAddress
    {
        [EposID("CustomerAddressID")]
        public Int32 CustomerAddressID { get; set; }
        public Int32 CustomerID { get; set; }
        public String Name { get; set; }
        public String AddressLine1 { get; set; }
        public String AddressLine2 { get; set; }
        public String Town { get; set; }
        public String County { get; set; }
        public String PostCode { get; set; }
    }
}
