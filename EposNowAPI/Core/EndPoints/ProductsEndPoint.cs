using EposNowAPI.Architecture.BaseClasses;
using EposNowAPI.Architecture.Extensions;
using EposNowAPI.Architecture.Interfaces;
using EposNowAPI.Core.Responses;
using EposNowAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace EposNowAPI.Core.EndPoints
{
    public sealed class ProductsEndPoint : EndPointBase<ProductsEndPoint, Product> 
	, IUpdateEndPoint<Product>
	, IDeleteEndPoint<Product>
	, ICreateEndPoint<Product>
    {
        private ProductsResponse Response { get; set; }

        public ProductsEndPoint(Authenticator auth) : base()
        {
            Response = new ProductsResponse(auth);
        }

        public Product Create(Product item)
        {
            return Response.Create(item);
        }
		public Product Update(Product item)
        {
            return Response.Update(item);
        }
		public bool Delete(int id)
        {
            return Response.Delete(id);
        }

		public bool Delete(Product item)
        {
            return Response.Delete(item);
        }
		
        public override Product Find(Int32 id)
        {
            return Response.Find(id);
        }

        public override IEnumerable<Product> Find()
        {
            IEnumerable<Product> listToReturn = Response.Find(QueryParameters);
            QueryParameters.Reset();
            return listToReturn;
        }

        public override IEnumerable<Product> FindAll()
        {
			QueryParameters.GetAll = true;
            QueryParameters.PageNo = 1;
            IEnumerable<Product> listToReturn = Response.FindAll(QueryParameters);
            QueryParameters.Reset();
            return listToReturn;
        }

        public override ProductsEndPoint Where(Expression<Func<Product, Boolean>> expression)
        {
            QueryParameters.Where.AddRange(expression.ToEposQueries());
            return this;
        }

        public override ProductsEndPoint And(Expression<Func<Product, Boolean>> expression)
        {
            return Where(expression);
        }

        public override ProductsEndPoint OrderBy<T>(Expression<Func<Product, T>> expression)
        {
            QueryParameters.Order = expression.ToEposOrderByQuery();
            return this;
        }

        public override ProductsEndPoint OrderByDesc<T>(Expression<Func<Product, T>> expression)
        {
            QueryParameters.Order = expression.ToEposOrderByQuery(true);
            return this;
        }

        public override ProductsEndPoint Page(Int32 pageNo)
        {
            QueryParameters.PageNo = pageNo;
            return this;
        }
    }
}
