﻿using System;
using ApiData.Attributes;
using ApiData.BaseClasses;

namespace ApiData.Models
{
    [EposModel("Brand", "Brands", IsCreatable = true, IsUpdatable = false)]
    public class Brand :ModelBase
    {
        [EposID("BrandID")]
        public Int32 BrandID { get; set; }
        public String Name { get; set; }
        public String Description { get; set; }
    }
}
