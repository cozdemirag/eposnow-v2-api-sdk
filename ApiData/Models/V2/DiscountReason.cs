﻿using System;
using ApiData.Attributes;

namespace ApiData.Models
{
    [EposModel("DiscountReason", "DiscountReasons")]
    public class DiscountReason
    {
        [EposID("DiscountReasonID")]
        public Int32 DiscountReasonID { get; set; }
        public String Reason { get; set; }
        public Decimal DefaultVal { get; set; } 
    }
}
