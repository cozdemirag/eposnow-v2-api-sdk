﻿//using Microsoft.VisualStudio.TestTools.UnitTesting;
using EposNowNET.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Linq.Expressions;
using NUnit.Framework;
using AutoPoco;
using EposNowAPI.Models;
using AutoPoco.Engine;

namespace EposNowNET.Core.Tests
{
    [TestFixture]
    public class ExpressionEvaluatorTests
    {
        [Test]
        public void VisitTest()
        {
            Int32 test1 = 0;

            List<String> queries = _sut.EvaluateWhereExpression<Product>(it => it.ProductID == test1);

            Assert.AreEqual(true, queries[0] == "ProductID = 0");
        }

        [Test]
        public void ReturnFalseForImplicitFalseBoolean()
        {
            List<String> queries = _sut.EvaluateWhereExpression<Product>(it => !it.SellOnTill);

            Assert.AreEqual(true, queries[0] == "SellOnTill=false");
        }

        [Test]
        public void ReturnTrueForImplicitTrueBoolean()
        {
            List<String> queries = _sut.EvaluateWhereExpression<Product>(it => it.SellOnTill);

            Assert.AreEqual(true, queries[0] == "SellOnTill=true");
        }

        [Test]
        public void GivenAStringPropertyEqualityPredicate_WhenUsedInWhereExpression_ReturnPropertyNameEqualSignAndTheValue()
        {
            var propertyValue = "Test Name Property";
            var expected = $"Name={propertyValue}";
            var queries = this._sut.EvaluateWhereExpression<Product>(it => it.Name == "Test Name Property");

            Assert.AreEqual(expected, queries[0]);
        }

        [Test]
        public void TestPoco()
        {
            int count = products.Count(it => it.Description == "Test Product");

            Assert.That(count == 20);
        }

        private Product p;
        private List<Product> products;
        private ExpressionEvaluator _sut;

        [SetUp]
        public void Setup()
        {
            _sut = new ExpressionEvaluator();

            // Perform factory set up (once for entire test run)
            IGenerationSessionFactory factory = AutoPocoContainer.Configure(x =>
            {
                x.Conventions(c =>
                {
                    c.UseDefaultConventions();
                });
                x.AddFromAssemblyContainingType<Product>();
            });

            // Generate one of these per test (factory will be a static variable most likely)
            IGenerationSession session = factory.CreateSession();

            // Get a single user
            p = session.Single<Product>().Get();

            // Get a collection of users
            products = session.List<Product>(100)
                               .Random(20).Impose(it => it.Description, "Test Product")
                               .Next(20).Impose(it => it.CostPrice, 5)
                               .Next(60).Impose(it => it.CategoryID, 1)
                               .Invoke(it => it.ButtonColourID.GetValueOrDefault())
                               .All()
                               .Get()
                               .ToList();
                              

            //// Get a collection of users, but set their role manually
            //mSession.List<SimpleUser>(10)
            //               .Impose(x => x.Role, sharedRole)
            //               .Get();
        }
    }
}