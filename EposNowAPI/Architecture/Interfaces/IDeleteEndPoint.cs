﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EposNowAPI.Architecture.Interfaces
{
    public interface IDeleteEndPoint<TModel>
    {
        Boolean Delete(TModel item);
        Boolean Delete(Int32 id);
    }
}
