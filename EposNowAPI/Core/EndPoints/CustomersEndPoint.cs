using EposNowAPI.Architecture.BaseClasses;
using EposNowAPI.Architecture.Extensions;
using EposNowAPI.Architecture.Interfaces;
using EposNowAPI.Core.Responses;
using EposNowAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace EposNowAPI.Core.EndPoints
{
    public sealed class CustomersEndPoint : EndPointBase<CustomersEndPoint, Customer> 
	, IUpdateEndPoint<Customer>
	, IDeleteEndPoint<Customer>
	, ICreateEndPoint<Customer>
    {
        private CustomersResponse Response { get; set; }

        public CustomersEndPoint(Authenticator auth) : base()
        {
            Response = new CustomersResponse(auth);
        }

        public Customer Create(Customer item)
        {
            return Response.Create(item);
        }
		public Customer Update(Customer item)
        {
            return Response.Update(item);
        }
		public bool Delete(int id)
        {
            return Response.Delete(id);
        }

		public bool Delete(Customer item)
        {
            return Response.Delete(item);
        }
		
        public override Customer Find(Int32 id)
        {
            return Response.Find(id);
        }

        public override IEnumerable<Customer> Find()
        {
            IEnumerable<Customer> listToReturn = Response.Find(QueryParameters);
            QueryParameters.Reset();
            return listToReturn;
        }

        public override IEnumerable<Customer> FindAll()
        {
			QueryParameters.GetAll = true;
            QueryParameters.PageNo = 1;
            IEnumerable<Customer> listToReturn = Response.FindAll(QueryParameters);
            QueryParameters.Reset();
            return listToReturn;
        }

        public override CustomersEndPoint Where(Expression<Func<Customer, Boolean>> expression)
        {
            QueryParameters.Where.AddRange(expression.ToEposQueries());
            return this;
        }

        public override CustomersEndPoint And(Expression<Func<Customer, Boolean>> expression)
        {
            return Where(expression);
        }

        public override CustomersEndPoint OrderBy<T>(Expression<Func<Customer, T>> expression)
        {
            QueryParameters.Order = expression.ToEposOrderByQuery();
            return this;
        }

        public override CustomersEndPoint OrderByDesc<T>(Expression<Func<Customer, T>> expression)
        {
            QueryParameters.Order = expression.ToEposOrderByQuery(true);
            return this;
        }

        public override CustomersEndPoint Page(Int32 pageNo)
        {
            QueryParameters.PageNo = pageNo;
            return this;
        }
    }
}
