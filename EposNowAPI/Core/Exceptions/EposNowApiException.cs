﻿using System;

namespace EposNowAPI.Core.Exceptions
{
    public class EposNowApiException : Exception
    {
        public EposNowApiException(String message) : base(message)
        {
        }
    }
}
