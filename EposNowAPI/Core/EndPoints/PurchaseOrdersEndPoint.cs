using EposNowAPI.Architecture.BaseClasses;
using EposNowAPI.Architecture.Extensions;
using EposNowAPI.Architecture.Interfaces;
using EposNowAPI.Core.Responses;
using EposNowAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace EposNowAPI.Core.EndPoints
{
    public sealed class PurchaseOrdersEndPoint : EndPointBase<PurchaseOrdersEndPoint, PurchaseOrder> 
    {
        private PurchaseOrdersResponse Response { get; set; }

        public PurchaseOrdersEndPoint(Authenticator auth) : base()
        {
            Response = new PurchaseOrdersResponse(auth);
        }


        public override PurchaseOrder Find(Int32 id)
        {
            return Response.Find(id);
        }

        public override IEnumerable<PurchaseOrder> Find()
        {
            IEnumerable<PurchaseOrder> listToReturn = Response.Find(QueryParameters);
            QueryParameters.Reset();
            return listToReturn;
        }

        public override IEnumerable<PurchaseOrder> FindAll()
        {
			QueryParameters.GetAll = true;
            QueryParameters.PageNo = 1;
            IEnumerable<PurchaseOrder> listToReturn = Response.FindAll(QueryParameters);
            QueryParameters.Reset();
            return listToReturn;
        }

        public override PurchaseOrdersEndPoint Where(Expression<Func<PurchaseOrder, Boolean>> expression)
        {
            QueryParameters.Where.AddRange(expression.ToEposQueries());
            return this;
        }

        public override PurchaseOrdersEndPoint And(Expression<Func<PurchaseOrder, Boolean>> expression)
        {
            return Where(expression);
        }

        public override PurchaseOrdersEndPoint OrderBy<T>(Expression<Func<PurchaseOrder, T>> expression)
        {
            QueryParameters.Order = expression.ToEposOrderByQuery();
            return this;
        }

        public override PurchaseOrdersEndPoint OrderByDesc<T>(Expression<Func<PurchaseOrder, T>> expression)
        {
            QueryParameters.Order = expression.ToEposOrderByQuery(true);
            return this;
        }

        public override PurchaseOrdersEndPoint Page(Int32 pageNo)
        {
            QueryParameters.PageNo = pageNo;
            return this;
        }
    }
}
