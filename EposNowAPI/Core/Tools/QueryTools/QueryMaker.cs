﻿using EposNowAPI.Architecture.Attributes;
using System;
using System.Linq;
using System.Reflection;

namespace EposNowAPI.Core.Tools.QueryTools
{
    public static class QueryMaker<T> 
    {
        public static String MakeUrlPaged(String url, Int32 page)
        {
            if (page > 0)
            {
                if (url.EndsWith("?"))
                {
                    url += "page=" + page;
                }
                else
                {
                url += ((!String.IsNullOrWhiteSpace(url) && !url.EndsWith("&") ? "&" : "") + "page=" + page);

                }
            }

            return url;
        }

        public static String MakeGetManyUrl(String baseUrl, String where, String order)
        {
            return String.Format(Constants.GetManyQueryFormat, MakeBaseUrl(baseUrl, GetEndPointVersion()), GetEndPointName(), MakeQuery(where, order));
        }

        public static String MakeGetSingleUrl(String baseUrl, Int32 itemID)
        {
            return String.Format(Constants.GetSingleQueryFormat, MakeBaseUrl(baseUrl, GetEndPointVersion()), GetEndPointName(), itemID);
        }

        public static String MakeCreateUrl(String baseUrl, T item)
        {
            return String.Format(Constants.CreateQueryFormat, MakeBaseUrl(baseUrl, GetEndPointVersion()), GetEndPointName());
        }

        public static String MakeUpdateUrl(String baseUrl, T item)
        {
            return String.Format(Constants.UpdateQueryFormat, MakeBaseUrl(baseUrl, GetEndPointVersion()), GetEndPointName(), GetItemID(item));
        }

        public static String MakeDeleteUrl(String baseUrl, T item)
        {
            return String.Format(Constants.DeleteQueryFormat, MakeBaseUrl(baseUrl, GetEndPointVersion()), GetEndPointName(), GetItemID(item));
        }

        public static String MakeDeleteUrl(String baseUrl, Int32 itemID)
        {
            return String.Format(Constants.DeleteQueryFormat, MakeBaseUrl(baseUrl, GetEndPointVersion()), GetEndPointName(), itemID);
        }

        private static String MakeBaseUrl(String url, String endPointVersion)
        {
            return String.Format(Constants.BaseUrlFormat, url, endPointVersion);
        }

        private static String MakeQuery(String where, String order, Int32 page = 0)
        {
            String query = String.Empty;

            if (!String.IsNullOrWhiteSpace(where))
            {
                query += where;
            }

            if (!String.IsNullOrWhiteSpace(order))
            {
                query += (!String.IsNullOrWhiteSpace(query) && !query.EndsWith("&") ? "&" : order);
            }

            if (page > 0)
            {
                query += (!String.IsNullOrWhiteSpace(query) && !query.EndsWith("&") ? "&" : "page=" + page);
            }

            return query;
        }

        private static Int32? GetItemID(T item)
        {
            try
            {
                return (Int32?)typeof(T).GetProperties().FirstOrDefault(it => it.CustomAttributes.Any(attr => attr.AttributeType == typeof(EposID))).GetValue(item);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return null;
            }
        }

        private static String GetEndPointName() 
        {
            try
            {
                return ((EposModel)typeof(T).GetCustomAttribute(typeof(EposModel))).EndPointName;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return "UnknownEndPoint";
            }
        }

        private static String GetEndPointVersion()
        {
            try
            {
                return ((EposModel)typeof(T).GetCustomAttribute(typeof(EposModel))).EndPointVersion;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return "UnknownEndPointVersion";
            }
        }
    }
}
