using EposNowAPI.Models;
using System;
using System.Collections.Generic;
using EposNowAPI.Architecture.Interfaces;

namespace EposNowAPI.Core.Responses
{
    public class DiscountReasonsResponse : EposNowResponse<DiscountReason>, IEndPoint<DiscountReason>
    {
        public DiscountReasonsResponse() : base() { }

        public DiscountReasonsResponse(Authenticator auth)
            : base(auth)
        {

        }

        public new DiscountReason Create(DiscountReason itemToCreate)
        {
            return base.Create<DiscountReasonsResponse>(itemToCreate);
        }

        public new DiscountReason Update(DiscountReason itemToUpdate)
        {
            return base.Update<DiscountReasonsResponse>(itemToUpdate);
        }

        public bool Delete(DiscountReason item)
        {
            return base.Delete<DiscountReasonsResponse>(item);
        }

        public new IEnumerable<DiscountReason> Get()
        {
            return base.Get<DiscountReasonsResponse>();
        }

        public new IEnumerable<DiscountReason> Get(Int32 id)
        {
            return base.Get<DiscountReasonsResponse>(id);
        }

        public IEnumerable<DiscountReason> Get(Int32 page, String where, String order)
        {
            return base.Get<DiscountReasonsResponse>(page, where, order);
        }

        public IEnumerable<DiscountReason> Get<TKey1, TKey2>(System.Linq.Expressions.Expression<Func<DiscountReason, TKey1>> whereClause = null, System.Linq.Expressions.Expression<Func<DiscountReason, TKey2>> orderByClause = null)
        {
            return base.Get<DiscountReasonsResponse, TKey1, TKey2>(whereClause, orderByClause);
        }
    }
}
