﻿using System;
using EposNowAPI.Architecture.Attributes;
using EposNowAPI.Architecture.BaseClasses;

namespace EposNowAPI.Models
{
    [EposModel("Location", "Locations")]
    public class Location : ModelBase
    {
        [EposID("LocationID")]
        public Int32 LocationID { get; set; }
        public String Name { get; set; }
        public String Description { get; set; }
        public String Address1 { get; set; }
        public String Address2 { get; set; }
        public String Town { get; set; }
        public String County { get; set; }
        public String PostCode { get; set; }
        public String EmailAddress { get; set; }
    }
}
