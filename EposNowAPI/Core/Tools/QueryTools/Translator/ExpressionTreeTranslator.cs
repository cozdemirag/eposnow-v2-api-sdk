﻿using EposNowAPI.Architecture.Attributes;
using EposNowAPI.Architecture.BaseClasses;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Text.RegularExpressions;

namespace EposNowAPI.Core.Tools.QueryTools.Translator
{
    public class ExpressionTreeTranslator : ExpressionVisitorBase
    {
        public Expression expressionToReturn { get; private set; }
        public List<String> TranslatedQueryList { get; set; }

        public ExpressionTreeTranslator()
        {
            TranslatedQueryList = new List<String>();
        }

        public List<String> GetQueryStrings<T>(Expression<Func<T, Boolean>> expression)
        {
            Visit(expression);

            return TranslatedQueryList;
        }

        public List<String> GetQueryStrings(Expression expression)
        {
            Visit(expression);

            return TranslatedQueryList;
        }

        public String GetOrderByQueryString<TModel, TPropertyType>(Expression<Func<TModel, TPropertyType>> expression, Boolean orderByDescending = false)
        {
            String OrderByQueryStringToReturn = String.Empty;

            if (expression.Body as MemberExpression != null)
            {
                MemberExpression member = (MemberExpression)expression.Body;
                OrderByQueryStringToReturn = String.Format("{0}={1}", orderByDescending ? TranslatorConstants.OrderByTypes.OrderByDescending : TranslatorConstants.OrderByTypes.OrderByAscending, member.Member.Name);
            }

            return OrderByQueryStringToReturn;
        }

        protected override Expression VisitLambda(LambdaExpression l)
        {
            if (l.Body.NodeType == ExpressionType.Not)
            {
                UnaryExpression unary = (UnaryExpression)l.Body;

                if (unary.Operand.NodeType == ExpressionType.MemberAccess)
                {
                    MemberExpression leftMember = ((MemberExpression)unary.Operand);
                    TranslatedQueryList.Add(TranslateIntoEposQuery(leftMember.Member.Name, TranslateNodeType(ExpressionType.Equal), false));
                }
                else
                {
                    Visit(unary.Operand);
                }
            }
            else if (l.Body.NodeType == ExpressionType.MemberAccess)
            {
                MemberExpression leftMember = (MemberExpression)l.Body;
                TranslatedQueryList.Add(TranslateIntoEposQuery(leftMember.Member.Name, TranslateNodeType(ExpressionType.Equal), true));
            }
            else
            {
                Visit(l.Body);
            }

            return l;
        }

        protected override Expression VisitBinary(BinaryExpression b)
        {
            if (b.NodeType != ExpressionType.And || b.NodeType != ExpressionType.AndAlso) throw new InvalidOperationException();

            Expression left = Visit(b.Left);
            Expression right = Visit(b.Right);

            if (left.NodeType != ExpressionType.MemberAccess && right.NodeType == ExpressionType.MemberAccess)
            {
                Expression tmp = left;
                left = right;
                right = tmp;
                tmp = null;
            }

            if (left.NodeType == ExpressionType.MemberAccess && (b.NodeType != ExpressionType.AndAlso && b.NodeType != ExpressionType.And && b.NodeType != ExpressionType.OrElse && b.NodeType != ExpressionType.Or))
            {
                MemberExpression leftMember = (MemberExpression)left;

                if (right.NodeType == ExpressionType.MemberAccess)
                {
                    MemberExpression rightMember = (MemberExpression)right;
                    TranslatedQueryList.Add(TranslateIntoEposQuery(leftMember.Member.Name, TranslateNodeType(b.NodeType), EvaluateMemberValue(rightMember)));
                    return b;
                }
                else if (right.NodeType == ExpressionType.Constant)
                {
                    ConstantExpression rightConstant = (ConstantExpression)right;
                    TranslatedQueryList.Add(TranslateIntoEposQuery(leftMember.Member.Name, TranslateNodeType(b.NodeType), rightConstant.Value));
                    return b;
                }
                else if (right.NodeType == ExpressionType.Call)
                {
                    MethodCallExpression rightMethod = (MethodCallExpression)right;
                    TranslatedQueryList.Add(TranslateIntoEposQuery(leftMember.Member.Name, TranslateNodeType(b.NodeType), EvaluateMethodValue(rightMethod)));
                }
                else if (right.NodeType == ExpressionType.Convert)
                {
                    UnaryExpression rightConvert = (UnaryExpression)right;
                    Expression operand = this.Visit(rightConvert.Operand);

                    if (operand.NodeType == ExpressionType.Constant)
                    {
                        ConstantExpression operandConstant = (ConstantExpression)operand;
                        TranslatedQueryList.Add(TranslateIntoEposQuery(leftMember.Member.Name, TranslateNodeType(b.NodeType), operandConstant.Value));
                        return b;
                    }
                }
                else
                {
                    return Visit(right);
                }
            }
            else if(left.NodeType == ExpressionType.Convert && right.NodeType == ExpressionType.Convert)
            {
                MemberExpression leftMember = ((UnaryExpression)left).Operand as MemberExpression;
                Expression f = Visit(left);
                UnaryExpression rightConvert = (UnaryExpression)right;
                Expression operand = this.Visit(rightConvert.Operand);

                if (leftMember != null && operand.NodeType == ExpressionType.Constant)
                {
                    ConstantExpression operandConstant = (ConstantExpression)operand;
                    TranslatedQueryList.Add(TranslateIntoEposQuery(leftMember.Member.Name, TranslateNodeType(b.NodeType), GetEnumValueOf(operandConstant)));
                    return b;
                }
                return b;
            }
            else if (left.NodeType == ExpressionType.Not)
            {
                MemberExpression leftMember = ((MemberExpression)((UnaryExpression)left).Operand);
                TranslatedQueryList.Add(TranslateIntoEposQuery(leftMember.Member.Name, TranslateNodeType(ExpressionType.Equal), false));
                return b;
            }
            else if (left.NodeType == ExpressionType.MemberAccess)
            {
                MemberExpression leftMember = (MemberExpression)left;
                TranslatedQueryList.Add(TranslateIntoEposQuery(leftMember.Member.Name, TranslateNodeType(ExpressionType.Equal), true));
                return b;
            }

            if ((b.NodeType != ExpressionType.Equal && b.NodeType != ExpressionType.Not) && right.NodeType == ExpressionType.MemberAccess && right.Type == typeof(Boolean))
            {
                MemberExpression rightMember = (MemberExpression)right;
                TranslatedQueryList.Add(TranslateIntoEposQuery(rightMember.Member.Name, TranslateNodeType(ExpressionType.Equal), true));
                return b;
            }
            else if ((b.NodeType != ExpressionType.Equal && b.NodeType != ExpressionType.Not) && right.NodeType == ExpressionType.Not && right.Type == typeof(Boolean))
            {
                MemberExpression rightMember = ((MemberExpression)((UnaryExpression)right).Operand);
                TranslatedQueryList.Add(TranslateIntoEposQuery(rightMember.Member.Name, TranslateNodeType(ExpressionType.Equal), false));
                return b;
            }

            return b;
        }

        public Object GetEnumValueOf(ConstantExpression enumConstant)
        {
            return ((EnumValue)(enumConstant.Type.GetField(enumConstant.Value.ToString()).GetCustomAttribute(typeof(EnumValue)))).Value;
        }

        private static object EvaluateMethodValue(MethodCallExpression expression)
        {
            var objectMember = Expression.Convert(expression, typeof(object));

            var getterLambda = Expression.Lambda<Func<object>>(objectMember);

            var getter = getterLambda.Compile();

            return getter();
        }

        public Object EvaluateMemberValue(MemberExpression member)
        {
            switch (member.Type.Name)
            {
                case "DateTime":
                    return String.Format("{0:dd/MM/yyyy HH:mm:ss}", (Expression.Lambda<Func<DateTime>>(Expression.Convert(member, typeof(DateTime)))).Compile()());//.ToString();//.ToString("MM/dd/yyyy HH:mm:ss");
                default:
                    return (Expression.Lambda<Func<object>>(Expression.Convert(member, typeof(object)))).Compile()();
            }
        }

        public T EvaluateMemberValue<T>(MemberExpression member)
        {
            return (Expression.Lambda<Func<T>>(Expression.Convert(member, typeof(T)))).Compile()();
        }

        public String TranslateNodeType(ExpressionType nodeType)
        {
            switch (nodeType)
            {
                case ExpressionType.GreaterThan:
                    return TranslatorConstants.BinaryOperators.GreaterThan;
                case ExpressionType.GreaterThanOrEqual:
                    return TranslatorConstants.BinaryOperators.GreaterThanOrEqual;
                case ExpressionType.LessThan:
                    return TranslatorConstants.BinaryOperators.LessThan;
                case ExpressionType.LessThanOrEqual:
                    return TranslatorConstants.BinaryOperators.LessThanOrEqual;
                case ExpressionType.Not:
                case ExpressionType.NotEqual:
                    return TranslatorConstants.BinaryOperators.NotEqual;
                case ExpressionType.Equal:
                default:
                    return TranslatorConstants.BinaryOperators.Equal;
            }
        }

        public String TranslateIntoEposQuery(String propertyName, String operatorType, Object value)
        {
            if (value == null && (operatorType != TranslatorConstants.BinaryOperators.Equal || operatorType != TranslatorConstants.BinaryOperators.NotEqual))
            {
                return String.Format(TranslatorConstants.NullSearchQueryFormat, propertyName, (operatorType == TranslatorConstants.BinaryOperators.Equal ? TranslatorConstants.SearchOperators.IsNull : TranslatorConstants.SearchOperators.IsNotNull));
            }
            else
            {
                if (operatorType != TranslatorConstants.BinaryOperators.Equal)
                {
                    return String.Format(TranslatorConstants.SearchQueryFormat, propertyName, operatorType, value);
                }
                else
                {
                    return String.Format(TranslatorConstants.BinaryQueryFormat, propertyName, operatorType, value);
                }
            }
        }
    }
}
