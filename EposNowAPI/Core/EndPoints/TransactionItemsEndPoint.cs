using EposNowAPI.Architecture.BaseClasses;
using EposNowAPI.Architecture.Extensions;
using EposNowAPI.Architecture.Interfaces;
using EposNowAPI.Core.Responses;
using EposNowAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace EposNowAPI.Core.EndPoints
{
    public sealed class TransactionItemsEndPoint : EndPointBase<TransactionItemsEndPoint, TransactionItem> 
    {
        private TransactionItemsResponse Response { get; set; }

        public TransactionItemsEndPoint(Authenticator auth) : base()
        {
            Response = new TransactionItemsResponse(auth);
        }


        public override TransactionItem Find(Int32 id)
        {
            return Response.Find(id);
        }

        public override IEnumerable<TransactionItem> Find()
        {
            IEnumerable<TransactionItem> listToReturn = Response.Find(QueryParameters);
            QueryParameters.Reset();
            return listToReturn;
        }

        public override IEnumerable<TransactionItem> FindAll()
        {
			QueryParameters.GetAll = true;
            QueryParameters.PageNo = 1;
            IEnumerable<TransactionItem> listToReturn = Response.FindAll(QueryParameters);
            QueryParameters.Reset();
            return listToReturn;
        }

        public override TransactionItemsEndPoint Where(Expression<Func<TransactionItem, Boolean>> expression)
        {
            QueryParameters.Where.AddRange(expression.ToEposQueries());
            return this;
        }

        public override TransactionItemsEndPoint And(Expression<Func<TransactionItem, Boolean>> expression)
        {
            return Where(expression);
        }

        public override TransactionItemsEndPoint OrderBy<T>(Expression<Func<TransactionItem, T>> expression)
        {
            QueryParameters.Order = expression.ToEposOrderByQuery();
            return this;
        }

        public override TransactionItemsEndPoint OrderByDesc<T>(Expression<Func<TransactionItem, T>> expression)
        {
            QueryParameters.Order = expression.ToEposOrderByQuery(true);
            return this;
        }

        public override TransactionItemsEndPoint Page(Int32 pageNo)
        {
            QueryParameters.PageNo = pageNo;
            return this;
        }
    }
}
