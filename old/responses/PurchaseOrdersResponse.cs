using EposNowAPI.Models;
using System;
using System.Collections.Generic;
using EposNowAPI.Architecture.Interfaces;

namespace EposNowAPI.Core.Responses
{
    public class PurchaseOrdersResponse : EposNowResponse<PurchaseOrder>, IEndPoint<PurchaseOrder>
    {
        public PurchaseOrdersResponse() : base() { }

        public PurchaseOrdersResponse(Authenticator auth)
            : base(auth)
        {

        }

        public new PurchaseOrder Create(PurchaseOrder itemToCreate)
        {
            return base.Create<PurchaseOrdersResponse>(itemToCreate);
        }

        public new PurchaseOrder Update(PurchaseOrder itemToUpdate)
        {
            return base.Update<PurchaseOrdersResponse>(itemToUpdate);
        }

        public bool Delete(PurchaseOrder item)
        {
            return base.Delete<PurchaseOrdersResponse>(item);
        }

        public new IEnumerable<PurchaseOrder> Get()
        {
            return base.Get<PurchaseOrdersResponse>();
        }

        public new IEnumerable<PurchaseOrder> Get(Int32 id)
        {
            return base.Get<PurchaseOrdersResponse>(id);
        }

        public IEnumerable<PurchaseOrder> Get(Int32 page, String where, String order)
        {
            return base.Get<PurchaseOrdersResponse>(page, where, order);
        }

        public IEnumerable<PurchaseOrder> Get<TKey1, TKey2>(System.Linq.Expressions.Expression<Func<PurchaseOrder, TKey1>> whereClause = null, System.Linq.Expressions.Expression<Func<PurchaseOrder, TKey2>> orderByClause = null)
        {
            return base.Get<PurchaseOrdersResponse, TKey1, TKey2>(whereClause, orderByClause);
        }
    }
}
