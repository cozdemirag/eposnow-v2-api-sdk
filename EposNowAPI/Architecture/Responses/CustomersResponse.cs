using EposNowAPI.Architecture.BaseClasses;
using EposNowAPI.Core.Tools.QueryTools;
using EposNowAPI.Models;
using System;
using System.Collections.Generic;

namespace EposNowAPI.Core.Responses
{
    public sealed class CustomersResponse : ResponseBase<Customer>
    {
        public CustomersResponse() : base() { }

        public CustomersResponse(Authenticator auth)
            : base(auth)
        {
        }

        public Customer Create(Customer itemToCreate)
        {
            return base.Create<CustomersResponse>(itemToCreate);
        }

        public Customer Update(Customer itemToUpdate)
        {
            return base.Update<CustomersResponse>(itemToUpdate);
        }

        public Boolean Delete(Customer itemToDelete)
        {
            return base.Delete<CustomersResponse>(itemToDelete);
        }

        public Boolean Delete(Int32 id)
        {
            return base.Delete<CustomersResponse>(id);
        }

        public Customer Find(Int32 id)
        {
            return base.Find<CustomersResponse>(id);
        }

        public IEnumerable<Customer> Find(QueryParameters<Customer> parameters)
        {
            return base.Find<CustomersResponse>(parameters);
        }

        public IEnumerable<Customer> FindAll(QueryParameters<Customer> parameters)
        {
            return base.Find<CustomersResponse>(parameters);
        }
    }
}
