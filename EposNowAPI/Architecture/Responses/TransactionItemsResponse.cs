using EposNowAPI.Architecture.BaseClasses;
using EposNowAPI.Core.Tools.QueryTools;
using EposNowAPI.Models;
using System;
using System.Collections.Generic;

namespace EposNowAPI.Core.Responses
{
    public sealed class TransactionItemsResponse : ResponseBase<TransactionItem>
    {
        public TransactionItemsResponse() : base() { }

        public TransactionItemsResponse(Authenticator auth)
            : base(auth)
        {
        }

        public TransactionItem Create(TransactionItem itemToCreate)
        {
            return base.Create<TransactionItemsResponse>(itemToCreate);
        }

        public TransactionItem Update(TransactionItem itemToUpdate)
        {
            return base.Update<TransactionItemsResponse>(itemToUpdate);
        }

        public Boolean Delete(TransactionItem itemToDelete)
        {
            return base.Delete<TransactionItemsResponse>(itemToDelete);
        }

        public Boolean Delete(Int32 id)
        {
            return base.Delete<TransactionItemsResponse>(id);
        }

        public TransactionItem Find(Int32 id)
        {
            return base.Find<TransactionItemsResponse>(id);
        }

        public IEnumerable<TransactionItem> Find(QueryParameters<TransactionItem> parameters)
        {
            return base.Find<TransactionItemsResponse>(parameters);
        }

        public IEnumerable<TransactionItem> FindAll(QueryParameters<TransactionItem> parameters)
        {
            return base.Find<TransactionItemsResponse>(parameters);
        }
    }
}
