using EposNowAPI.Models;
using System;
using System.Collections.Generic;
using EposNowAPI.Architecture.Interfaces;

namespace EposNowAPI.Core.Responses
{
    public class EndOfDayReportsResponse : EposNowResponse<EndOfDayReport>, IEndPoint<EndOfDayReport>
    {
        public EndOfDayReportsResponse() : base() { }

        public EndOfDayReportsResponse(Authenticator auth)
            : base(auth)
        {

        }

        public new EndOfDayReport Create(EndOfDayReport itemToCreate)
        {
            return base.Create<EndOfDayReportsResponse>(itemToCreate);
        }

        public new EndOfDayReport Update(EndOfDayReport itemToUpdate)
        {
            return base.Update<EndOfDayReportsResponse>(itemToUpdate);
        }

        public bool Delete(EndOfDayReport item)
        {
            return base.Delete<EndOfDayReportsResponse>(item);
        }

        public new IEnumerable<EndOfDayReport> Get()
        {
            return base.Get<EndOfDayReportsResponse>();
        }

        public new IEnumerable<EndOfDayReport> Get(Int32 id)
        {
            return base.Get<EndOfDayReportsResponse>(id);
        }

        public IEnumerable<EndOfDayReport> Get(Int32 page, String where, String order)
        {
            return base.Get<EndOfDayReportsResponse>(page, where, order);
        }

        public IEnumerable<EndOfDayReport> Get<TKey1, TKey2>(System.Linq.Expressions.Expression<Func<EndOfDayReport, TKey1>> whereClause = null, System.Linq.Expressions.Expression<Func<EndOfDayReport, TKey2>> orderByClause = null)
        {
            return base.Get<EndOfDayReportsResponse, TKey1, TKey2>(whereClause, orderByClause);
        }
    }
}
