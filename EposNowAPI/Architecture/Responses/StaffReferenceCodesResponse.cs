using EposNowAPI.Architecture.BaseClasses;
using EposNowAPI.Core.Tools.QueryTools;
using EposNowAPI.Models;
using System;
using System.Collections.Generic;

namespace EposNowAPI.Core.Responses
{
    public sealed class StaffReferenceCodesResponse : ResponseBase<StaffReferenceCode>
    {
        public StaffReferenceCodesResponse() : base() { }

        public StaffReferenceCodesResponse(Authenticator auth)
            : base(auth)
        {
        }

        public StaffReferenceCode Create(StaffReferenceCode itemToCreate)
        {
            return base.Create<StaffReferenceCodesResponse>(itemToCreate);
        }

        public StaffReferenceCode Update(StaffReferenceCode itemToUpdate)
        {
            return base.Update<StaffReferenceCodesResponse>(itemToUpdate);
        }

        public Boolean Delete(StaffReferenceCode itemToDelete)
        {
            return base.Delete<StaffReferenceCodesResponse>(itemToDelete);
        }

        public Boolean Delete(Int32 id)
        {
            return base.Delete<StaffReferenceCodesResponse>(id);
        }

        public StaffReferenceCode Find(Int32 id)
        {
            return base.Find<StaffReferenceCodesResponse>(id);
        }

        public IEnumerable<StaffReferenceCode> Find(QueryParameters<StaffReferenceCode> parameters)
        {
            return base.Find<StaffReferenceCodesResponse>(parameters);
        }

        public IEnumerable<StaffReferenceCode> FindAll(QueryParameters<StaffReferenceCode> parameters)
        {
            return base.Find<StaffReferenceCodesResponse>(parameters);
        }
    }
}
