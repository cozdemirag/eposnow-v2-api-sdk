﻿using EposNowAPI.Architecture.Attributes;
using EposNowAPI.Architecture.BaseClasses;
using System;

namespace EposNowAPI.Models
{
    [EposModel("CustomerReferenceCode", "CustomerReferenceCodes")]
    public class CustomerReferenceCode : ModelBase
    {
        [EposID("CustomerReferenceCodeID")]
        public Int32 CustomerReferenceCodeID { get; set; }
        public Int32 CustomerID { get; set; }
        public Int32 AppID { get; set; }
        public String ReferenceCode { get; set; }
    }
}
