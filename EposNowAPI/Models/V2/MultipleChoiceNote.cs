﻿using System;
using EposNowAPI.Architecture.Attributes;

namespace EposNowAPI.Models
{
    [EposModel("MultipleChoiceNote", "MultipleChoiceNotes")]
    public class MultipleChoiceNote
    {
        [EposID("MultipleChoiceNoteID")]
        public Int32 MultipleChoiceNoteID { get; set; }
        public String Name { get; set; }
        public Boolean PopupByDefault { get; set; }
    }
}
