﻿using ApiData.Attributes;
using ApiData.Models.Constants;
using System;

namespace ApiData.Models
{
    [EposModel("BaseItem", "BaseItems")]
    public class BaseItem
    {
        [EposID("BaseItemID")]
        public Int32 BaseItemID { get; set; }
        public Int32 TransactionID { get; set; }
        public BaseItemType ItemTypeID { get; set; }
        public Decimal Amount { get; set; }
    }
}
