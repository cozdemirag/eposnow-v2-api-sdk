﻿using System;

namespace EposNowNET.Contracts.Attributes
{
    public sealed class EnumValue : Attribute
    {
        public Object Value { get; internal set; }

        public EnumValue(Object value)
        {
            Value = value;
        }
    }
}
