using EposNowAPI.Core.Responses;
using EposNowAPI.Models;
using System.Collections.Generic;
using System;
using System.Linq.Expressions;
using System.Text.RegularExpressions;
using EposNowAPI.Architecture.Interfaces;

namespace EposNowAPI.Core.EndPoints
{
    public class StockTransferItemsEndPoint : IEndPoint<StockTransferItem>
    {
        private StockTransferItemsResponse Response;
        private Int32 pageNo;
        private String whereParams;
        private String orderParams;

        public StockTransferItemsEndPoint(Authenticator auth)
        {
            Response = new StockTransferItemsResponse(auth);
        }

        public StockTransferItem Create(StockTransferItem item)
        {
            return Response.Create(item);
        }

        public bool Delete(StockTransferItem item)
        {
            return Response.Delete(item);
        }

        public IEnumerable<StockTransferItem> Get()
        {
            IEnumerable<StockTransferItem> listToReturn = Response.Get(pageNo, whereParams, orderParams);
            pageNo = 0;
            whereParams = "";
            orderParams = "";
            return listToReturn;
        }

        public IEnumerable<StockTransferItem> Get(int id)
        {
            return Response.Get(id);
        }

        public IEnumerable<StockTransferItem> Get<TKey1, TKey2>(System.Linq.Expressions.Expression<Func<StockTransferItem, TKey1>> whereClause = null, System.Linq.Expressions.Expression<Func<StockTransferItem, TKey2>> orderByClause = null)
        {
            return Response.Get<TKey1, TKey2>(whereClause, orderByClause);
        }

        public StockTransferItem Update(StockTransferItem item)
        {
            return Response.Update(item);
        }


        public StockTransferItemsEndPoint Where(String param)
        {
            whereParams += param;
            return this;
        }

        public StockTransferItemsEndPoint OrderBy(String param)
        {
            orderParams = "OrderBy=" + param;
            return this;
        }

        public StockTransferItemsEndPoint OrderByDesc(String param)
        {
            orderParams = "OrderByDesc=" + param;
            return this;
        }

        public StockTransferItemsEndPoint OrderByDesc<T>(Expression<Func<StockTransferItem, T>> order)
        {
            orderParams = "OrderByDesc=" + new Regex(@"\s*").Replace(new Regex(@"(==)+").Replace(new Regex(@"([\(\)""]+)").Replace(new Regex(@"(" + order.Parameters[0].Name + @"{1}[\.]{1})").Replace(new Regex(@"(Convert\()+").Replace(order.Body.ToString(), "", 99), "", 99), "", 99), "=", 99), "", 99);
            return this;
        }

        public StockTransferItemsEndPoint OrderBy<T>(Expression<Func<StockTransferItem, T>> order)
        {
            orderParams = "OrderByDesc=" + new Regex(@"\s*").Replace(new Regex(@"(==)+").Replace(new Regex(@"([\(\)""]+)").Replace(new Regex(@"(" + order.Parameters[0].Name + @"{1}[\.]{1})").Replace(new Regex(@"(Convert\()+").Replace(order.Body.ToString(), "", 99), "", 99), "", 99), "=", 99), "", 99);
            return this;
        }

        public StockTransferItemsEndPoint Page(Int32 page)
        {
            pageNo = page;
            return this;
        }

        public StockTransferItemsEndPoint Where(params Expression<Func<StockTransferItem, Boolean>>[] e)
        {
            whereParams = String.IsNullOrWhiteSpace(whereParams) ? "?" : whereParams;

            foreach (Expression<Func<StockTransferItem, Boolean>> exp in e)
            {
                Func<StockTransferItem, Boolean> daaa = exp.Compile();

                if (whereParams != "?" && !whereParams.EndsWith("&"))
                {
                    whereParams += "&";
                }

                if (exp.Body.ToString().Contains(".Contains("))
                {
                    whereParams += String.Format("{0}{1}{2}", "search=(", new Regex(@"\s*").Replace(new Regex(@"(==)+").Replace(new Regex(@"([\(\)""]+)").Replace(new Regex(@"(" + exp.Parameters[0].Name + @"{1}[\.]{1})").Replace(new Regex(@"(.Contains\()+").Replace(exp.Body.ToString(), "|like|", 99), "", 99), "", 99), "=", 99), "", 99), ")");
                }
                else if (exp.Body.ToString().Contains(".EndsWith("))
                {
                    whereParams += String.Format("{0}{1}{2}", "search=(", new Regex(@"\s*").Replace(new Regex(@"(==)+").Replace(new Regex(@"([\(\)""]+)").Replace(new Regex(@"(" + exp.Parameters[0].Name + @"{1}[\.]{1})").Replace(new Regex(@"(.EndsWith\()+").Replace(exp.Body.ToString(), "|EndsWith|", 99), "", 99), "", 99), "=", 99), "", 99), ")");
                }
                else if (exp.Body.ToString().Contains(".StartsWith("))
                {
                    whereParams += String.Format("{0}{1}{2}", "search=(", new Regex(@"\s*").Replace(new Regex(@"(==)+").Replace(new Regex(@"([\(\)""]+)").Replace(new Regex(@"(" + exp.Parameters[0].Name + @"{1}[\.]{1})").Replace(new Regex(@"(.StartsWith\()+").Replace(exp.Body.ToString(), "|StartsWith|", 99), "", 99), "", 99), "=", 99), "", 99), ")");
                }
                else if (exp.Body.ToString().Contains(">"))
                {
                    whereParams += String.Format("{0}{1}{2}", "search=(", new Regex(@"\s*").Replace(new Regex(@"(>)+").Replace(new Regex(@"([\(\)""]+)").Replace(new Regex(@"(" + exp.Parameters[0].Name + @"{1}[\.]{1})").Replace(new Regex(@"(Convert\()+").Replace(exp.Body.ToString(), "", 99), "", 99), "", 99), "|>|", 99), "", 99), ")");
                }
                else if (exp.Body.ToString().Contains(">="))
                {
                    whereParams += String.Format("{0}{1}{2}", "search=(", new Regex(@"\s*").Replace(new Regex(@"(>=)+").Replace(new Regex(@"([\(\)""]+)").Replace(new Regex(@"(" + exp.Parameters[0].Name + @"{1}[\.]{1})").Replace(new Regex(@"(Convert\()+").Replace(exp.Body.ToString(), "", 99), "", 99), "", 99), "|>=|", 99), "", 99), ")");
                }
                else if (exp.Body.ToString().Contains("<"))
                {
                    whereParams += String.Format("{0}{1}{2}", "search=(", new Regex(@"\s*").Replace(new Regex(@"(<)+").Replace(new Regex(@"([\(\)""]+)").Replace(new Regex(@"(" + exp.Parameters[0].Name + @"{1}[\.]{1})").Replace(new Regex(@"(Convert\()+").Replace(exp.Body.ToString(), "", 99), "", 99), "", 99), "|<|", 99), "", 99), ")");
                }
                else if (exp.Body.ToString().Contains("<="))
                {
                    whereParams += String.Format("{0}{1}{2}", "search=(", new Regex(@"\s*").Replace(new Regex(@"(<=)+").Replace(new Regex(@"([\(\)""]+)").Replace(new Regex(@"(" + exp.Parameters[0].Name + @"{1}[\.]{1})").Replace(new Regex(@"(Convert\()+").Replace(exp.Body.ToString(), "", 99), "", 99), "", 99), "|<=|", 99), "", 99), ")");
                }
                else if (exp.Body.ToString().Contains("=="))
                {
                    whereParams += new Regex(@"\s*").Replace(new Regex(@"(==)+").Replace(new Regex(@"([\(\)""]+)").Replace(new Regex(@"(" + exp.Parameters[0].Name + @"{1}[\.]{1})").Replace(new Regex(@"(Convert\()+").Replace(exp.Body.ToString(), "", 99), "", 99), "", 99), "=", 99), "", 99);
                }
                else
                {
                    throw new Exception(exp.Body + " has invalid operations for Epos Now API");
                }
            }
            return this;
        }
    }
}
