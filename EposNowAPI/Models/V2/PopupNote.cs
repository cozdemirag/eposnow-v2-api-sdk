﻿using System;
using EposNowAPI.Architecture.Attributes;

namespace EposNowAPI.Models
{
    [EposModel("PopupNote", "PopupNotes")]
    public class PopupNote
    {
        [EposID("PopupNoteID")]
        public Int32 PopupNoteID { get; set; }
        public String ShortDesc { get; set; }
        public String Description { get; set; }
    }
}
