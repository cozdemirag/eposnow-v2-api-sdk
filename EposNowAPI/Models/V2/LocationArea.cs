﻿using System;
using EposNowAPI.Architecture.Attributes;
using EposNowAPI.Architecture.BaseClasses;

namespace EposNowAPI.Models
{
    [EposModel("LocationArea", "LocationAreas")]
    public class LocationArea : ModelBase
    {
        [EposID("LocationAreaID")]
        public Int32 LocationAreaID { get; set; }
        public String Name { get; set; }
        public String Description { get; set; }
        public String EmailAddress { get; set; }
    }
}
