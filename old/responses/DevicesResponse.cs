using EposNowAPI.Models;
using System;
using System.Collections.Generic;
using EposNowAPI.Architecture.Interfaces;

namespace EposNowAPI.Core.Responses
{
    public class DevicesResponse : EposNowResponse<Device>, IEndPoint<Device>
    {
        public DevicesResponse() : base() { }

        public DevicesResponse(Authenticator auth)
            : base(auth)
        {

        }

        public new Device Create(Device itemToCreate)
        {
            return base.Create<DevicesResponse>(itemToCreate);
        }

        public new Device Update(Device itemToUpdate)
        {
            return base.Update<DevicesResponse>(itemToUpdate);
        }

        public bool Delete(Device item)
        {
            return base.Delete<DevicesResponse>(item);
        }

        public new IEnumerable<Device> Get()
        {
            return base.Get<DevicesResponse>();
        }

        public new IEnumerable<Device> Get(Int32 id)
        {
            return base.Get<DevicesResponse>(id);
        }

        public IEnumerable<Device> Get(Int32 page, String where, String order)
        {
            return base.Get<DevicesResponse>(page, where, order);
        }

        public IEnumerable<Device> Get<TKey1, TKey2>(System.Linq.Expressions.Expression<Func<Device, TKey1>> whereClause = null, System.Linq.Expressions.Expression<Func<Device, TKey2>> orderByClause = null)
        {
            return base.Get<DevicesResponse, TKey1, TKey2>(whereClause, orderByClause);
        }
    }
}
