﻿using System;
using ApiData.Attributes;
using ApiData.BaseClasses;

namespace ApiData.Models
{
    [EposModel("DailySales", "DailySales", IsCreatable = false, IsDeletable = false, IsUpdatable = false)]
    public class DailySalesReport : ModelBase
    {
        public Int32 LocationAreaID { get; set; }
        public Int32 LocationID { get; set; }
        public Int32 DeviceID { get; set; }
        public Int32 StaffID { get; set; }
        public Int32 ProductID { get; set; }
        public DateTime FromDate { get; set; }
        public DateTime ToDate { get; set; }
        public String Search { get; set; }
        public Int32 DeviceGroupID { get; set; }
    }
}
