﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace EposNow.NET.Contracts.Expressions.Translation.Rules
{
    public interface ITranslationRule
    {
        bool IsMatch(Expression left, Expression origin = null, Expression right = null);
        Object Translate(Expression left, Expression origin = null, Expression right = null);
    }
}
