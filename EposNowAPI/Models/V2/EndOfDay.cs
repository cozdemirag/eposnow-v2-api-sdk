﻿using System;
using System.Collections.Generic;
using EposNowAPI.Architecture.Attributes;

namespace EposNowAPI.Models
{
    public class EndOfDay
    {
        public Int32 DeviceID { get; private set; }
        public Int32 OpenedByID { get; private set; }
        public Int32 ClosedByID { get; private set; }
        public Int32 LocationID { get; private set; }
        public Int32 EndOfDayID { get; private set; }
        public String TillName { get; private set; }
        public String Location { get; private set; }
        public DateTime StartDate { get; private set; }
        public DateTime EndDate { get; private set; }
        public String OpenedBy { get; private set; }
        public String ClosedBy { get; private set; }
        public Decimal Sales { get; private set; }
        public Decimal Actual { get; private set; }
        public Decimal Variance { get; private set; }
        public Decimal PayOuts { get; private set; }
        public Decimal PettyCash { get; private set; }
        public Decimal Expected { get; private set; }
        public Int32 StaffOpen { get; set; }
        public Decimal Float { get; set; }
        public Int32 StaffClose { get; set; }
        public Decimal InTill { get; set; }
        public List<Tender> Tenders { get; set; }
    }
}
