using EposNowAPI.Models;
using System;
using System.Collections.Generic;

namespace EposNowAPI.Core.Responses
{
    public class TransactionDetailssResponse : EposNowResponse<TransactionDetails>, IEndPoint<TransactionDetails>
    {
        public TransactionDetailssResponse() : base() { }

        public TransactionDetailssResponse(Authenticator auth)
            : base(auth)
        {

        }

        public new TransactionDetails Create(TransactionDetails itemToCreate)
        {
            return base.Create<TransactionDetailssResponse>(itemToCreate);
        }

        public new TransactionDetails Update(TransactionDetails itemToUpdate)
        {
            return base.Update<TransactionDetailssResponse>(itemToUpdate);
        }

        public bool Delete(TransactionDetails item)
        {
            return base.Delete<TransactionDetailssResponse>(item);
        }

        public new IEnumerable<TransactionDetails> Get()
        {
            return base.Get<TransactionDetailssResponse>();
        }

        public new IEnumerable<TransactionDetails> Get(Int32 id)
        {
            return base.Get<TransactionDetailssResponse>(id);
        }

        public IEnumerable<TransactionDetails> Get(Int32 page, String where, String order)
        {
            return base.Get<TransactionDetailssResponse>(page, where, order);
        }

        public IEnumerable<TransactionDetails> Get<TKey1, TKey2>(System.Linq.Expressions.Expression<Func<TransactionDetails, TKey1>> whereClause = null, System.Linq.Expressions.Expression<Func<TransactionDetails, TKey2>> orderByClause = null)
        {
            return base.Get<TransactionDetailssResponse, TKey1, TKey2>(whereClause, orderByClause);
        }
    }
}
