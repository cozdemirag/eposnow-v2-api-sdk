﻿using System;
using EposNowAPI.Architecture.Attributes;

namespace EposNowAPI.Models
{
    [EposModel("MultipleChoiceProduct", "MultipleChoiceProducts")]
    public class MultipleChoiceProduct
    {
        [EposID("MultipleChoiceProductID")]
        public Int32 MultipleChoiceProductID { get; set; }
        public Int32 ProductID { get; set; }
        public Int32 MultipleChoiceProductGroupID { get; set; }
        public Decimal SalePrice { get; set; }
        public Decimal EatOutPrice { get; set; }
    }
}
