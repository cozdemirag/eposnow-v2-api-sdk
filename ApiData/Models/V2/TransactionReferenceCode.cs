﻿using ApiData.Attributes;
using ApiData.BaseClasses;
using System;

namespace ApiData.Models
{
    [EposModel("TransactionReferenceCode", "TransactionReferenceCodes")]
    public class TransactionReferenceCode : ModelBase
    {
        [EposID("TransactionReferenceCodeID")]
        public Int32 TransactionReferenceCodeID { get; set; }
        public Int32 TransactionID { get; set; }
        public Int32 AppID { get; set; }
        public String ReferenceCode { get; set; }
    }
}
