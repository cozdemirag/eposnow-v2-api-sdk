using EposNowAPI.Architecture.BaseClasses;
using EposNowAPI.Core.Tools.QueryTools;
using EposNowAPI.Models;
using System;
using System.Collections.Generic;

namespace EposNowAPI.Core.Responses
{
    public sealed class TenderTypesResponse : ResponseBase<TenderType>
    {
        public TenderTypesResponse() : base() { }

        public TenderTypesResponse(Authenticator auth)
            : base(auth)
        {
        }

        public TenderType Create(TenderType itemToCreate)
        {
            return base.Create<TenderTypesResponse>(itemToCreate);
        }

        public TenderType Update(TenderType itemToUpdate)
        {
            return base.Update<TenderTypesResponse>(itemToUpdate);
        }

        public Boolean Delete(TenderType itemToDelete)
        {
            return base.Delete<TenderTypesResponse>(itemToDelete);
        }

        public Boolean Delete(Int32 id)
        {
            return base.Delete<TenderTypesResponse>(id);
        }

        public TenderType Find(Int32 id)
        {
            return base.Find<TenderTypesResponse>(id);
        }

        public IEnumerable<TenderType> Find(QueryParameters<TenderType> parameters)
        {
            return base.Find<TenderTypesResponse>(parameters);
        }

        public IEnumerable<TenderType> FindAll(QueryParameters<TenderType> parameters)
        {
            return base.Find<TenderTypesResponse>(parameters);
        }
    }
}
