using EposNowAPI.Models;
using System;
using System.Collections.Generic;
using EposNowAPI.Architecture.Interfaces;

namespace EposNowAPI.Core.Responses
{
    public class TransactionItemsResponse : EposNowResponse<TransactionItem>, IEndPoint<TransactionItem>
    {
        public TransactionItemsResponse() : base() { }

        public TransactionItemsResponse(Authenticator auth)
            : base(auth)
        {

        }

        public new TransactionItem Create(TransactionItem itemToCreate)
        {
            return base.Create<TransactionItemsResponse>(itemToCreate);
        }

        public new TransactionItem Update(TransactionItem itemToUpdate)
        {
            return base.Update<TransactionItemsResponse>(itemToUpdate);
        }

        public bool Delete(TransactionItem item)
        {
            return base.Delete<TransactionItemsResponse>(item);
        }

        public new IEnumerable<TransactionItem> Get()
        {
            return base.Get<TransactionItemsResponse>();
        }

        public new IEnumerable<TransactionItem> Get(Int32 id)
        {
            return base.Get<TransactionItemsResponse>(id);
        }

        public IEnumerable<TransactionItem> Get(Int32 page, String where, String order)
        {
            return base.Get<TransactionItemsResponse>(page, where, order);
        }

        public IEnumerable<TransactionItem> Get<TKey1, TKey2>(System.Linq.Expressions.Expression<Func<TransactionItem, TKey1>> whereClause = null, System.Linq.Expressions.Expression<Func<TransactionItem, TKey2>> orderByClause = null)
        {
            return base.Get<TransactionItemsResponse, TKey1, TKey2>(whereClause, orderByClause);
        }
    }
}
