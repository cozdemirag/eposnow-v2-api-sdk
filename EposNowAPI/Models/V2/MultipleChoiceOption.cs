﻿using System;
using EposNowAPI.Architecture.Attributes;

namespace EposNowAPI.Models
{
    [EposModel("MultipleChoiceOption", "MultipleChoiceOptions")]
    public class MultipleChoiceOption
    {
        [EposID("MultipleChoiceOptionID")]
        public Int32 MultipleChoiceOptionID { get; set; }
        public String Message { get; set; }
        public Int32 NoteID { get; set; }
    }
}
