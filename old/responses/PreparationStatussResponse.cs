using EposNowAPI.Models;
using System;
using System.Collections.Generic;
using EposNowAPI.Architecture.Interfaces;

namespace EposNowAPI.Core.Responses
{
    public class PreparationStatussResponse : EposNowResponse<PreparationStatus>, IEndPoint<PreparationStatus>
    {
        public PreparationStatussResponse() : base() { }

        public PreparationStatussResponse(Authenticator auth)
            : base(auth)
        {

        }

        public new PreparationStatus Create(PreparationStatus itemToCreate)
        {
            return base.Create<PreparationStatussResponse>(itemToCreate);
        }

        public new PreparationStatus Update(PreparationStatus itemToUpdate)
        {
            return base.Update<PreparationStatussResponse>(itemToUpdate);
        }

        public bool Delete(PreparationStatus item)
        {
            return base.Delete<PreparationStatussResponse>(item);
        }

        public new IEnumerable<PreparationStatus> Get()
        {
            return base.Get<PreparationStatussResponse>();
        }

        public new IEnumerable<PreparationStatus> Get(Int32 id)
        {
            return base.Get<PreparationStatussResponse>(id);
        }

        public IEnumerable<PreparationStatus> Get(Int32 page, String where, String order)
        {
            return base.Get<PreparationStatussResponse>(page, where, order);
        }

        public IEnumerable<PreparationStatus> Get<TKey1, TKey2>(System.Linq.Expressions.Expression<Func<PreparationStatus, TKey1>> whereClause = null, System.Linq.Expressions.Expression<Func<PreparationStatus, TKey2>> orderByClause = null)
        {
            return base.Get<PreparationStatussResponse, TKey1, TKey2>(whereClause, orderByClause);
        }
    }
}
