﻿using EposNow.NET.Contracts.Expressions.Translation.Rules;
using EposNow.NET.Core;
using EposNow.NET.Core.Rules.VisitBinary;
using EposNow.NET.Core.Rules.VisitLambda;
using EposNowNET.Contracts.Expressions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace EposNowNET.Core
{
    public class ExpressionEvaluator : ExpressionVisitor
    {
        private List<Expression> expressionToParse = new List<Expression>();

        private List<String> TranslatedQueryList { get; set; }
        private List<TranslationRule> VisitBinaryTranslationRules { get; set; }
        private List<TranslationRule> VisitLambdaTranslationRules { get; set; }

        public ExpressionEvaluator()
        {
            TranslatedQueryList = new List<String>();

            VisitLambdaTranslationRules = new List<TranslationRule>()
            {
                new VisitLambdaFalseBooleanMemberAccessRule(),
                new VisitLambdaTrueBooleanMemberAccessRule()
            };

            VisitBinaryTranslationRules = new List<TranslationRule>()
            {
                new VisitBinaryLeftMemberRightCallRule(),
                new VisitBinaryLeftMemberRightConstantRule(),
                new VisitBinaryLeftMemberRightConvertRule(),
                new VisitBinaryLeftNotRule(),
                new VisitBinaryLeftRightMemberRule(),
                new VisitBinaryLeftTrueRule()
            };
        }

        public List<String> EvaluateWhereExpression<T>(Expression<Func<T, Boolean>> expression)
        {
            this.expressionToParse = new List<Expression>();
            this.TranslatedQueryList = new List<string>();


            recurj(expression.Body);


            var booleanMemberEvaluator = new BooleanExpressionEvaluator();
            var aggregateCallEvaluator = new AggregateCallExpressionEvaluator();

            foreach (var exp in expressionToParse)
            {
                //var member = exp as MemberExpression;
                //var notMember = exp as UnaryExpression;
                //if(member != null && member.Type == typeof(bool))
                //{
                //    TranslatedQueryList.Add($"search=({member.Member.Name}|=|True)");
                //    continue;
                //}

                //if(notMember != null)
                //{
                //    var operandMember = notMember.Operand as MemberExpression;
                //    TranslatedQueryList.Add($"search=({operandMember.Member.Name}|=|False)");
                //    continue;
                //}
                if(booleanMemberEvaluator.CanEvaluate(exp))
                {
                    TranslatedQueryList.Add(booleanMemberEvaluator.EvaluateExpression(exp));
                }else if (aggregateCallEvaluator.CanEvaluate(exp))
                {
                    TranslatedQueryList.Add(aggregateCallEvaluator.EvaluateExpression(exp));
                }else if(false)
                {
                    Visit(exp);
                }else
                {
                    throw new ArgumentException(exp.ToString() + " expression cannot be processed");
                }
            }

            //Visit(expression);
            return TranslatedQueryList.Where(it => !it.Equals(string.Empty)).ToList();
        }

        private void recurj(Expression expression)
        {
            var originalBinary = expression as BinaryExpression;

            var leftBinary = originalBinary?.Left as BinaryExpression;
            var rightBinary = originalBinary?.Right as BinaryExpression;

            if(originalBinary != null)
            {
                if (leftBinary == null)
                {
                    expressionToParse.Add(originalBinary.Left);
                }
                else
                {
                    if (leftBinary.NodeType == ExpressionType.AndAlso)
                    {
                        recurj(leftBinary);
                    }
                    else
                    {
                        expressionToParse.Add(originalBinary.Left);
                    }
                }

                if (rightBinary == null)
                {
                    expressionToParse.Add(originalBinary.Right);
                }
                else
                {
                    if (rightBinary.NodeType == ExpressionType.AndAlso)
                    {
                        recurj(rightBinary);
                    }
                    else
                    {
                        expressionToParse.Add(originalBinary.Right);
                    }
                }
            }
            else
            {
                expressionToParse.Add(expression);
            }


        }

        public String EvaluateOrderExpression<TEntity, TProperty>(Expression<Func<TEntity, TProperty>> expression)
        {
            return null;
        }

        protected override Expression VisitLambda<T>(Expression<T> lambda)
        {
            TranslationRule expRule = VisitLambdaTranslationRules.FirstOrDefault(it => it.IsMatch(lambda.Body));

            if (expRule != null)
            {
                TranslatedQueryList.Add(expRule.Translate(lambda.Body).ToString());
            }
            else
            {
                Visit(lambda.Body);
            }

            return lambda;
        }

        ////
        //// Summary:
        ////     Visits the children of the System.Linq.Expressions.MemberExpression.
        ////
        //// Parameters:
        ////   node:
        ////     The expression to visit.
        ////
        //// Returns:
        ////     The modified expression, if it or any subexpression was modified; otherwise,
        ////     returns the original expression.
        //protected override Expression VisitMember(MemberExpression node)
        //{
        //    if (node != null)
        //    {
        //        TranslatedQueryList.Add($"search=({node.Member.Name}|=|True)");
        //    }

        //    return node;
        //}

        protected override Expression VisitBinary(BinaryExpression b)
        {
            Expression left = b.Left;
            Expression right = b.Right;

            if (left.NodeType != ExpressionType.MemberAccess)
            {
                left = Visit(b.Left);
            }

            if(right.NodeType != ExpressionType.MemberAccess)
            {
                right = Visit(b.Right);
            }

            if (left.NodeType != ExpressionType.MemberAccess && right.NodeType == ExpressionType.MemberAccess)
            {
                Expression tmp = left;
                left = right;
                right = tmp;
                tmp = null;
            }

            var rule = VisitBinaryTranslationRules.FirstOrDefault(it => it.IsMatch(left, b, right));

            if(rule != null)
            {
                TranslatedQueryList.Add(rule.Translate(left, b, right).ToString());
            }

            return base.VisitBinary(b);
        }
    }
}
