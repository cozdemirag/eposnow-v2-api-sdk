﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EposNowNET.Contracts.Convertors
{
    public interface IDataTransformer
    {
        String TransformToString<TEntity>(TEntity entity);
        IEnumerable<TEntity> TransformToEntity<TEntity>(String input);
    }
}
