using EposNowAPI.Architecture.BaseClasses;
using EposNowAPI.Architecture.Extensions;
using EposNowAPI.Architecture.Interfaces;
using EposNowAPI.Core.Responses;
using EposNowAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace EposNowAPI.Core.EndPoints
{
    public sealed class TransactionDetailsEndPoint : EndPointBase<TransactionDetailsEndPoint, TransactionDetail> 
    {
        private TransactionDetailsResponse Response { get; set; }

        public TransactionDetailsEndPoint(Authenticator auth) : base()
        {
            Response = new TransactionDetailsResponse(auth);
        }


        public override TransactionDetail Find(Int32 id)
        {
            return Response.Find(id);
        }

        public override IEnumerable<TransactionDetail> Find()
        {
            IEnumerable<TransactionDetail> listToReturn = Response.Find(QueryParameters);
            QueryParameters.Reset();
            return listToReturn;
        }

        public override IEnumerable<TransactionDetail> FindAll()
        {
			QueryParameters.GetAll = true;
            QueryParameters.PageNo = 1;
            IEnumerable<TransactionDetail> listToReturn = Response.FindAll(QueryParameters);
            QueryParameters.Reset();
            return listToReturn;
        }

        public override TransactionDetailsEndPoint Where(Expression<Func<TransactionDetail, Boolean>> expression)
        {
            QueryParameters.Where.AddRange(expression.ToEposQueries());
            return this;
        }

        public override TransactionDetailsEndPoint And(Expression<Func<TransactionDetail, Boolean>> expression)
        {
            return Where(expression);
        }

        public override TransactionDetailsEndPoint OrderBy<T>(Expression<Func<TransactionDetail, T>> expression)
        {
            QueryParameters.Order = expression.ToEposOrderByQuery();
            return this;
        }

        public override TransactionDetailsEndPoint OrderByDesc<T>(Expression<Func<TransactionDetail, T>> expression)
        {
            QueryParameters.Order = expression.ToEposOrderByQuery(true);
            return this;
        }

        public override TransactionDetailsEndPoint Page(Int32 pageNo)
        {
            QueryParameters.PageNo = pageNo;
            return this;
        }
    }
}
