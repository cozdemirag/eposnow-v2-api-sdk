using EposNowAPI.Architecture.BaseClasses;
using EposNowAPI.Architecture.Extensions;
using EposNowAPI.Architecture.Interfaces;
using EposNowAPI.Core.Responses;
using EposNowAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace EposNowAPI.Core.EndPoints
{
    public sealed class EndOfDayReportsEndPoint : EndPointBase<EndOfDayReportsEndPoint, EndOfDayReport> 
    {
        private EndOfDayReportsResponse Response { get; set; }

        public EndOfDayReportsEndPoint(Authenticator auth) : base()
        {
            Response = new EndOfDayReportsResponse(auth);
        }


        public override EndOfDayReport Find(Int32 id)
        {
            return Response.Find(id);
        }

        public override IEnumerable<EndOfDayReport> Find()
        {
            IEnumerable<EndOfDayReport> listToReturn = Response.Find(QueryParameters);
            QueryParameters.Reset();
            return listToReturn;
        }

        public override IEnumerable<EndOfDayReport> FindAll()
        {
			QueryParameters.GetAll = true;
            QueryParameters.PageNo = 1;
            IEnumerable<EndOfDayReport> listToReturn = Response.FindAll(QueryParameters);
            QueryParameters.Reset();
            return listToReturn;
        }

        public override EndOfDayReportsEndPoint Where(Expression<Func<EndOfDayReport, Boolean>> expression)
        {
            QueryParameters.Where.AddRange(expression.ToEposQueries());
            return this;
        }

        public override EndOfDayReportsEndPoint And(Expression<Func<EndOfDayReport, Boolean>> expression)
        {
            return Where(expression);
        }

        public override EndOfDayReportsEndPoint OrderBy<T>(Expression<Func<EndOfDayReport, T>> expression)
        {
            QueryParameters.Order = expression.ToEposOrderByQuery();
            return this;
        }

        public override EndOfDayReportsEndPoint OrderByDesc<T>(Expression<Func<EndOfDayReport, T>> expression)
        {
            QueryParameters.Order = expression.ToEposOrderByQuery(true);
            return this;
        }

        public override EndOfDayReportsEndPoint Page(Int32 pageNo)
        {
            QueryParameters.PageNo = pageNo;
            return this;
        }
    }
}
