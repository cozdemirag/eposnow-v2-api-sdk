﻿using System;
using ApiData.Attributes;
using ApiData.BaseClasses;

namespace ApiData.Models
{
    [EposModel("Device", "Devices")]
    public class Device : ModelBase
    {
        [EposID("DeviceID")]
        public Int32 DeviceID { get; set; }
        public String Name { get; set; }
        public String Description { get; set; }
        public Int32 LocationID { get; set; }
        public Boolean Enabled { get; set; }
    }
}
