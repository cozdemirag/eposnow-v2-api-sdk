using EposNowAPI.Architecture.BaseClasses;
using EposNowAPI.Core.Tools.QueryTools;
using EposNowAPI.Models;
using System;
using System.Collections.Generic;

namespace EposNowAPI.Core.Responses
{
    public sealed class TransactionReferenceCodesResponse : ResponseBase<TransactionReferenceCode>
    {
        public TransactionReferenceCodesResponse() : base() { }

        public TransactionReferenceCodesResponse(Authenticator auth)
            : base(auth)
        {
        }

        public TransactionReferenceCode Create(TransactionReferenceCode itemToCreate)
        {
            return base.Create<TransactionReferenceCodesResponse>(itemToCreate);
        }

        public TransactionReferenceCode Update(TransactionReferenceCode itemToUpdate)
        {
            return base.Update<TransactionReferenceCodesResponse>(itemToUpdate);
        }

        public Boolean Delete(TransactionReferenceCode itemToDelete)
        {
            return base.Delete<TransactionReferenceCodesResponse>(itemToDelete);
        }

        public Boolean Delete(Int32 id)
        {
            return base.Delete<TransactionReferenceCodesResponse>(id);
        }

        public TransactionReferenceCode Find(Int32 id)
        {
            return base.Find<TransactionReferenceCodesResponse>(id);
        }

        public IEnumerable<TransactionReferenceCode> Find(QueryParameters<TransactionReferenceCode> parameters)
        {
            return base.Find<TransactionReferenceCodesResponse>(parameters);
        }

        public IEnumerable<TransactionReferenceCode> FindAll(QueryParameters<TransactionReferenceCode> parameters)
        {
            return base.Find<TransactionReferenceCodesResponse>(parameters);
        }
    }
}
