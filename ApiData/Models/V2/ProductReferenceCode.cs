﻿using ApiData.Attributes;
using ApiData.BaseClasses;
using System;

namespace ApiData.Models
{
    [EposModel("ProductReferenceCode", "ProductReferenceCodes")]
    public class ProductReferenceCode : ModelBase
    {
        [EposID("ProductReferenceCodeID")]
        public Int32 ProductReferenceCodeID { get; set; }
        public Int32 ProductID { get; set; }
        public Int32 AppID { get; set; }
        public String ReferenceCode { get; set; }
    }
}
