﻿using System;
using EposNowAPI.Architecture.Attributes;
using EposNowAPI.Architecture.BaseClasses;

namespace EposNowAPI.Models
{
    [EposModel("ProductStock", "ProductStocks")]
    public class ProductStock : ModelBase
    {
        [EposID("ProductStockID")]
        public Int32 ProductStockID { get; set; }
        public Int32 LocationID { get; set; }
        public Int32 ProductID { get; set; }
        public Int32 CurrentStock { get; set; }
        public Int32? MinStock { get; set; }
        public Int32? MaxStock { get; set; }
        public Int32 CurrentVolume { get; set; }
        public Boolean Alerts { get; set; }
    }
}
