﻿using EposNowAPI.Architecture.Attributes;

namespace EposNowAPI.Models.Constants
{
    public enum EatOutType
    {
        [EnumValue(0)]
        WalkIn, 
        [EnumValue(1)]
        TakeAway,
        [EnumValue(2)]
        Delivery
    }
}
