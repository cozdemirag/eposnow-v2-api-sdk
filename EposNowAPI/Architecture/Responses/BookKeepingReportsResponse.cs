using EposNowAPI.Architecture.BaseClasses;
using EposNowAPI.Core.Tools.QueryTools;
using EposNowAPI.Models;
using System;
using System.Collections.Generic;

namespace EposNowAPI.Core.Responses
{
    public sealed class BookKeepingReportsResponse : ResponseBase<BookKeepingReport>
    {
        public BookKeepingReportsResponse() : base() { }

        public BookKeepingReportsResponse(Authenticator auth)
            : base(auth)
        {
        }

        public BookKeepingReport Create(BookKeepingReport itemToCreate)
        {
            return base.Create<BookKeepingReportsResponse>(itemToCreate);
        }

        public BookKeepingReport Update(BookKeepingReport itemToUpdate)
        {
            return base.Update<BookKeepingReportsResponse>(itemToUpdate);
        }

        public Boolean Delete(BookKeepingReport itemToDelete)
        {
            return base.Delete<BookKeepingReportsResponse>(itemToDelete);
        }

        public Boolean Delete(Int32 id)
        {
            return base.Delete<BookKeepingReportsResponse>(id);
        }

        public BookKeepingReport Find(Int32 id)
        {
            return base.Find<BookKeepingReportsResponse>(id);
        }

        public IEnumerable<BookKeepingReport> Find(QueryParameters<BookKeepingReport> parameters)
        {
            return base.Find<BookKeepingReportsResponse>(parameters);
        }

        public IEnumerable<BookKeepingReport> FindAll(QueryParameters<BookKeepingReport> parameters)
        {
            return base.Find<BookKeepingReportsResponse>(parameters);
        }
    }
}
