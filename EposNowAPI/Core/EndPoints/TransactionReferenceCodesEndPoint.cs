using EposNowAPI.Architecture.BaseClasses;
using EposNowAPI.Architecture.Extensions;
using EposNowAPI.Architecture.Interfaces;
using EposNowAPI.Core.Responses;
using EposNowAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace EposNowAPI.Core.EndPoints
{
    public sealed class TransactionReferenceCodesEndPoint : EndPointBase<TransactionReferenceCodesEndPoint, TransactionReferenceCode> 
    {
        private TransactionReferenceCodesResponse Response { get; set; }

        public TransactionReferenceCodesEndPoint(Authenticator auth) : base()
        {
            Response = new TransactionReferenceCodesResponse(auth);
        }


        public override TransactionReferenceCode Find(Int32 id)
        {
            return Response.Find(id);
        }

        public override IEnumerable<TransactionReferenceCode> Find()
        {
            IEnumerable<TransactionReferenceCode> listToReturn = Response.Find(QueryParameters);
            QueryParameters.Reset();
            return listToReturn;
        }

        public override IEnumerable<TransactionReferenceCode> FindAll()
        {
			QueryParameters.GetAll = true;
            QueryParameters.PageNo = 1;
            IEnumerable<TransactionReferenceCode> listToReturn = Response.FindAll(QueryParameters);
            QueryParameters.Reset();
            return listToReturn;
        }

        public override TransactionReferenceCodesEndPoint Where(Expression<Func<TransactionReferenceCode, Boolean>> expression)
        {
            QueryParameters.Where.AddRange(expression.ToEposQueries());
            return this;
        }

        public override TransactionReferenceCodesEndPoint And(Expression<Func<TransactionReferenceCode, Boolean>> expression)
        {
            return Where(expression);
        }

        public override TransactionReferenceCodesEndPoint OrderBy<T>(Expression<Func<TransactionReferenceCode, T>> expression)
        {
            QueryParameters.Order = expression.ToEposOrderByQuery();
            return this;
        }

        public override TransactionReferenceCodesEndPoint OrderByDesc<T>(Expression<Func<TransactionReferenceCode, T>> expression)
        {
            QueryParameters.Order = expression.ToEposOrderByQuery(true);
            return this;
        }

        public override TransactionReferenceCodesEndPoint Page(Int32 pageNo)
        {
            QueryParameters.PageNo = pageNo;
            return this;
        }
    }
}
