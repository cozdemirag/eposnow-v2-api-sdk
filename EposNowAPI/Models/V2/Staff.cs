﻿using System;
using EposNowAPI.Architecture.Attributes;
using EposNowAPI.Architecture.BaseClasses;

namespace EposNowAPI.Models
{
    [EposModel("Staff", "Staff")]
    public class Staff : ModelBase
    {
        [EposID("StaffID")]
        public Int32 StaffID { get; set; }
        public String Name { get; set; }
        public String Password { get; set; }
        public Int32 AccessRightID { get; set; }
        public Decimal HourlyRate { get; set; }
    }
}
