﻿using System;
using ApiData.Attributes;
using ApiData.BaseClasses;

namespace ApiData.Models
{
    [EposModel("TaxRate", "TaxRates")]
    public class TaxRate : ModelBase
    {
        [EposID("TaxRateID")]
        public Int32 TaxRateID { get; set; }
        public Decimal Percentage { get; set; }
        public String Name { get; set; }
        public String Description { get; set; }
        public String TaxCode { get; set; }
    }
}
