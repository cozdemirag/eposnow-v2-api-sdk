using EposNowAPI.Architecture.BaseClasses;
using EposNowAPI.Core.Tools.QueryTools;
using EposNowAPI.Models;
using System;
using System.Collections.Generic;

namespace EposNowAPI.Core.Responses
{
    public sealed class CustomerReferenceCodesResponse : ResponseBase<CustomerReferenceCode>
    {
        public CustomerReferenceCodesResponse() : base() { }

        public CustomerReferenceCodesResponse(Authenticator auth)
            : base(auth)
        {
        }

        public CustomerReferenceCode Create(CustomerReferenceCode itemToCreate)
        {
            return base.Create<CustomerReferenceCodesResponse>(itemToCreate);
        }

        public CustomerReferenceCode Update(CustomerReferenceCode itemToUpdate)
        {
            return base.Update<CustomerReferenceCodesResponse>(itemToUpdate);
        }

        public Boolean Delete(CustomerReferenceCode itemToDelete)
        {
            return base.Delete<CustomerReferenceCodesResponse>(itemToDelete);
        }

        public Boolean Delete(Int32 id)
        {
            return base.Delete<CustomerReferenceCodesResponse>(id);
        }

        public CustomerReferenceCode Find(Int32 id)
        {
            return base.Find<CustomerReferenceCodesResponse>(id);
        }

        public IEnumerable<CustomerReferenceCode> Find(QueryParameters<CustomerReferenceCode> parameters)
        {
            return base.Find<CustomerReferenceCodesResponse>(parameters);
        }

        public IEnumerable<CustomerReferenceCode> FindAll(QueryParameters<CustomerReferenceCode> parameters)
        {
            return base.Find<CustomerReferenceCodesResponse>(parameters);
        }
    }
}
