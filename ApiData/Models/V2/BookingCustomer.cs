﻿using ApiData.Attributes;
using System;

namespace ApiData.Models
{
    [EposModel("BookingCustomer", "BookingCustomers")]
    public class BookingCustomer
    {
        [EposID("BookingCustomerID")]
        public Int32 BookingCustomerID { get; set; }
        public Int32 BookingID { get; set; }
        public Int32 CustomerID { get; set; }
        public Int32 GroupSize { get; set; }
        public String Notes { get; set; }
    }
}
