﻿using ApiData.Attributes;
using ApiData.BaseClasses;
using System;

namespace ApiData.Models
{
    [EposModel("StaffReferenceCode", "StaffReferenceCodes")]
    public class StaffReferenceCode : ModelBase
    {
        [EposID("StaffReferenceCodeID")]
        public Int32 StaffReferenceCodeID { get; set; }
        public Int32 StaffID { get; set; }
        public Int32 AppID { get; set; }
        public String ReferenceCode { get; set; }
    }
}
