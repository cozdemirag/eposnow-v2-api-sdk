using EposNowAPI.Models;
using System;
using System.Collections.Generic;
using EposNowAPI.Architecture.Interfaces;

namespace EposNowAPI.Core.Responses
{
    public class MultipleChoiceOptionsResponse : EposNowResponse<MultipleChoiceOption>, IEndPoint<MultipleChoiceOption>
    {
        public MultipleChoiceOptionsResponse() : base() { }

        public MultipleChoiceOptionsResponse(Authenticator auth)
            : base(auth)
        {

        }

        public new MultipleChoiceOption Create(MultipleChoiceOption itemToCreate)
        {
            return base.Create<MultipleChoiceOptionsResponse>(itemToCreate);
        }

        public new MultipleChoiceOption Update(MultipleChoiceOption itemToUpdate)
        {
            return base.Update<MultipleChoiceOptionsResponse>(itemToUpdate);
        }

        public bool Delete(MultipleChoiceOption item)
        {
            return base.Delete<MultipleChoiceOptionsResponse>(item);
        }

        public new IEnumerable<MultipleChoiceOption> Get()
        {
            return base.Get<MultipleChoiceOptionsResponse>();
        }

        public new IEnumerable<MultipleChoiceOption> Get(Int32 id)
        {
            return base.Get<MultipleChoiceOptionsResponse>(id);
        }

        public IEnumerable<MultipleChoiceOption> Get(Int32 page, String where, String order)
        {
            return base.Get<MultipleChoiceOptionsResponse>(page, where, order);
        }

        public IEnumerable<MultipleChoiceOption> Get<TKey1, TKey2>(System.Linq.Expressions.Expression<Func<MultipleChoiceOption, TKey1>> whereClause = null, System.Linq.Expressions.Expression<Func<MultipleChoiceOption, TKey2>> orderByClause = null)
        {
            return base.Get<MultipleChoiceOptionsResponse, TKey1, TKey2>(whereClause, orderByClause);
        }
    }
}
