using EposNowAPI.Architecture.BaseClasses;
using EposNowAPI.Core.Tools.QueryTools;
using EposNowAPI.Models;
using System;
using System.Collections.Generic;

namespace EposNowAPI.Core.Responses
{
    public sealed class PurchaseOrderItemsResponse : ResponseBase<PurchaseOrderItem>
    {
        public PurchaseOrderItemsResponse() : base() { }

        public PurchaseOrderItemsResponse(Authenticator auth)
            : base(auth)
        {
        }

        public PurchaseOrderItem Create(PurchaseOrderItem itemToCreate)
        {
            return base.Create<PurchaseOrderItemsResponse>(itemToCreate);
        }

        public PurchaseOrderItem Update(PurchaseOrderItem itemToUpdate)
        {
            return base.Update<PurchaseOrderItemsResponse>(itemToUpdate);
        }

        public Boolean Delete(PurchaseOrderItem itemToDelete)
        {
            return base.Delete<PurchaseOrderItemsResponse>(itemToDelete);
        }

        public Boolean Delete(Int32 id)
        {
            return base.Delete<PurchaseOrderItemsResponse>(id);
        }

        public PurchaseOrderItem Find(Int32 id)
        {
            return base.Find<PurchaseOrderItemsResponse>(id);
        }

        public IEnumerable<PurchaseOrderItem> Find(QueryParameters<PurchaseOrderItem> parameters)
        {
            return base.Find<PurchaseOrderItemsResponse>(parameters);
        }

        public IEnumerable<PurchaseOrderItem> FindAll(QueryParameters<PurchaseOrderItem> parameters)
        {
            return base.Find<PurchaseOrderItemsResponse>(parameters);
        }
    }
}
