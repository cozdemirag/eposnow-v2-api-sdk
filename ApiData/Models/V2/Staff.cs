﻿using System;
using ApiData.Attributes;
using ApiData.BaseClasses;

namespace ApiData.Models
{
    [EposModel("Staff", "Staff", IsCreatable = true, IsDeletable = true, IsUpdatable = true)]
    public class Staff : ModelBase
    {
        [EposID("StaffID")]
        public Int32 StaffID { get; set; }
        public String Name { get; set; }
        public String Password { get; set; }
        public Int32 AccessRightID { get; set; }
        public Decimal HourlyRate { get; set; }
    }
}
