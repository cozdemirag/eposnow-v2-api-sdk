﻿using System;
using ApiData.Attributes;

namespace ApiData.Models
{
    [EposModel("CustomerCredit", "CustomerCredits")]
    public class CustomerCredit
    {
        [EposID("CustomerCreditID")]
        public Int32 CustomerID { get; set; }
        public Decimal CreditToAdd { get; set; }
        public Int32 StaffID { get; set; }
        public Int32 TenderTypeID { get; set; }
    }
}
