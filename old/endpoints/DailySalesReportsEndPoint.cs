using EposNowAPI.Core.Responses;
using EposNowAPI.Models;
using System.Collections.Generic;
using System;
using System.Linq.Expressions;
using System.Text.RegularExpressions;
using EposNowAPI.Architecture.Interfaces;

namespace EposNowAPI.Core.EndPoints
{
    public class DailySalesReportsEndPoint : IEndPoint<DailySalesReport>
    {
        private DailySalesReportsResponse Response;
        private Int32 pageNo;
        private String whereParams;
        private String orderParams;

        public DailySalesReportsEndPoint(Authenticator auth)
        {
            Response = new DailySalesReportsResponse(auth);
        }

        public DailySalesReport Create(DailySalesReport item)
        {
            return Response.Create(item);
        }

        public bool Delete(DailySalesReport item)
        {
            return Response.Delete(item);
        }

        public IEnumerable<DailySalesReport> Get()
        {
            IEnumerable<DailySalesReport> listToReturn = Response.Get(pageNo, whereParams, orderParams);
            pageNo = 0;
            whereParams = "";
            orderParams = "";
            return listToReturn;
        }

        public IEnumerable<DailySalesReport> Get(int id)
        {
            return Response.Get(id);
        }

        public IEnumerable<DailySalesReport> Get<TKey1, TKey2>(System.Linq.Expressions.Expression<Func<DailySalesReport, TKey1>> whereClause = null, System.Linq.Expressions.Expression<Func<DailySalesReport, TKey2>> orderByClause = null)
        {
            return Response.Get<TKey1, TKey2>(whereClause, orderByClause);
        }

        public DailySalesReport Update(DailySalesReport item)
        {
            return Response.Update(item);
        }


        public DailySalesReportsEndPoint Where(String param)
        {
            whereParams += param;
            return this;
        }

        public DailySalesReportsEndPoint OrderBy(String param)
        {
            orderParams = "OrderBy=" + param;
            return this;
        }

        public DailySalesReportsEndPoint OrderByDesc(String param)
        {
            orderParams = "OrderByDesc=" + param;
            return this;
        }

        public DailySalesReportsEndPoint OrderByDesc<T>(Expression<Func<DailySalesReport, T>> order)
        {
            orderParams = "OrderByDesc=" + new Regex(@"\s*").Replace(new Regex(@"(==)+").Replace(new Regex(@"([\(\)""]+)").Replace(new Regex(@"(" + order.Parameters[0].Name + @"{1}[\.]{1})").Replace(new Regex(@"(Convert\()+").Replace(order.Body.ToString(), "", 99), "", 99), "", 99), "=", 99), "", 99);
            return this;
        }

        public DailySalesReportsEndPoint OrderBy<T>(Expression<Func<DailySalesReport, T>> order)
        {
            orderParams = "OrderByDesc=" + new Regex(@"\s*").Replace(new Regex(@"(==)+").Replace(new Regex(@"([\(\)""]+)").Replace(new Regex(@"(" + order.Parameters[0].Name + @"{1}[\.]{1})").Replace(new Regex(@"(Convert\()+").Replace(order.Body.ToString(), "", 99), "", 99), "", 99), "=", 99), "", 99);
            return this;
        }

        public DailySalesReportsEndPoint Page(Int32 page)
        {
            pageNo = page;
            return this;
        }

        public DailySalesReportsEndPoint Where(params Expression<Func<DailySalesReport, Boolean>>[] e)
        {
            whereParams = String.IsNullOrWhiteSpace(whereParams) ? "?" : whereParams;

            foreach (Expression<Func<DailySalesReport, Boolean>> exp in e)
            {
                Func<DailySalesReport, Boolean> daaa = exp.Compile();

                if (whereParams != "?" && !whereParams.EndsWith("&"))
                {
                    whereParams += "&";
                }

                if (exp.Body.ToString().Contains(".Contains("))
                {
                    whereParams += String.Format("{0}{1}{2}", "search=(", new Regex(@"\s*").Replace(new Regex(@"(==)+").Replace(new Regex(@"([\(\)""]+)").Replace(new Regex(@"(" + exp.Parameters[0].Name + @"{1}[\.]{1})").Replace(new Regex(@"(.Contains\()+").Replace(exp.Body.ToString(), "|like|", 99), "", 99), "", 99), "=", 99), "", 99), ")");
                }
                else if (exp.Body.ToString().Contains(".EndsWith("))
                {
                    whereParams += String.Format("{0}{1}{2}", "search=(", new Regex(@"\s*").Replace(new Regex(@"(==)+").Replace(new Regex(@"([\(\)""]+)").Replace(new Regex(@"(" + exp.Parameters[0].Name + @"{1}[\.]{1})").Replace(new Regex(@"(.EndsWith\()+").Replace(exp.Body.ToString(), "|EndsWith|", 99), "", 99), "", 99), "=", 99), "", 99), ")");
                }
                else if (exp.Body.ToString().Contains(".StartsWith("))
                {
                    whereParams += String.Format("{0}{1}{2}", "search=(", new Regex(@"\s*").Replace(new Regex(@"(==)+").Replace(new Regex(@"([\(\)""]+)").Replace(new Regex(@"(" + exp.Parameters[0].Name + @"{1}[\.]{1})").Replace(new Regex(@"(.StartsWith\()+").Replace(exp.Body.ToString(), "|StartsWith|", 99), "", 99), "", 99), "=", 99), "", 99), ")");
                }
                else if (exp.Body.ToString().Contains(">"))
                {
                    whereParams += String.Format("{0}{1}{2}", "search=(", new Regex(@"\s*").Replace(new Regex(@"(>)+").Replace(new Regex(@"([\(\)""]+)").Replace(new Regex(@"(" + exp.Parameters[0].Name + @"{1}[\.]{1})").Replace(new Regex(@"(Convert\()+").Replace(exp.Body.ToString(), "", 99), "", 99), "", 99), "|>|", 99), "", 99), ")");
                }
                else if (exp.Body.ToString().Contains(">="))
                {
                    whereParams += String.Format("{0}{1}{2}", "search=(", new Regex(@"\s*").Replace(new Regex(@"(>=)+").Replace(new Regex(@"([\(\)""]+)").Replace(new Regex(@"(" + exp.Parameters[0].Name + @"{1}[\.]{1})").Replace(new Regex(@"(Convert\()+").Replace(exp.Body.ToString(), "", 99), "", 99), "", 99), "|>=|", 99), "", 99), ")");
                }
                else if (exp.Body.ToString().Contains("<"))
                {
                    whereParams += String.Format("{0}{1}{2}", "search=(", new Regex(@"\s*").Replace(new Regex(@"(<)+").Replace(new Regex(@"([\(\)""]+)").Replace(new Regex(@"(" + exp.Parameters[0].Name + @"{1}[\.]{1})").Replace(new Regex(@"(Convert\()+").Replace(exp.Body.ToString(), "", 99), "", 99), "", 99), "|<|", 99), "", 99), ")");
                }
                else if (exp.Body.ToString().Contains("<="))
                {
                    whereParams += String.Format("{0}{1}{2}", "search=(", new Regex(@"\s*").Replace(new Regex(@"(<=)+").Replace(new Regex(@"([\(\)""]+)").Replace(new Regex(@"(" + exp.Parameters[0].Name + @"{1}[\.]{1})").Replace(new Regex(@"(Convert\()+").Replace(exp.Body.ToString(), "", 99), "", 99), "", 99), "|<=|", 99), "", 99), ")");
                }
                else if (exp.Body.ToString().Contains("=="))
                {
                    whereParams += new Regex(@"\s*").Replace(new Regex(@"(==)+").Replace(new Regex(@"([\(\)""]+)").Replace(new Regex(@"(" + exp.Parameters[0].Name + @"{1}[\.]{1})").Replace(new Regex(@"(Convert\()+").Replace(exp.Body.ToString(), "", 99), "", 99), "", 99), "=", 99), "", 99);
                }
                else
                {
                    throw new Exception(exp.Body + " has invalid operations for Epos Now API");
                }
            }
            return this;
        }
    }
}
