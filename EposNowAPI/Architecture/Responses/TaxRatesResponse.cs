using EposNowAPI.Architecture.BaseClasses;
using EposNowAPI.Core.Tools.QueryTools;
using EposNowAPI.Models;
using System;
using System.Collections.Generic;

namespace EposNowAPI.Core.Responses
{
    public sealed class TaxRatesResponse : ResponseBase<TaxRate>
    {
        public TaxRatesResponse() : base() { }

        public TaxRatesResponse(Authenticator auth)
            : base(auth)
        {
        }

        public TaxRate Create(TaxRate itemToCreate)
        {
            return base.Create<TaxRatesResponse>(itemToCreate);
        }

        public TaxRate Update(TaxRate itemToUpdate)
        {
            return base.Update<TaxRatesResponse>(itemToUpdate);
        }

        public Boolean Delete(TaxRate itemToDelete)
        {
            return base.Delete<TaxRatesResponse>(itemToDelete);
        }

        public Boolean Delete(Int32 id)
        {
            return base.Delete<TaxRatesResponse>(id);
        }

        public TaxRate Find(Int32 id)
        {
            return base.Find<TaxRatesResponse>(id);
        }

        public IEnumerable<TaxRate> Find(QueryParameters<TaxRate> parameters)
        {
            return base.Find<TaxRatesResponse>(parameters);
        }

        public IEnumerable<TaxRate> FindAll(QueryParameters<TaxRate> parameters)
        {
            return base.Find<TaxRatesResponse>(parameters);
        }
    }
}
