﻿using System;
using EposNowAPI.Architecture.Attributes;
using EposNowAPI.Architecture.BaseClasses;

namespace EposNowAPI.Models
{
    [EposModel("Tender", "Tenders")]
    public class Tender : ModelBase
    {
        [EposID("TenderID")]
        public Int32 TenderID { get; set; }
        public Int32 TransactionID { get; set; }
        public Int32 TypeID { get; set; }
        public Decimal Amount { get; set; }
        public Decimal Change { get; set; }
    }
}
