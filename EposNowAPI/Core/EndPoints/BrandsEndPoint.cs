using EposNowAPI.Architecture.BaseClasses;
using EposNowAPI.Architecture.Extensions;
using EposNowAPI.Architecture.Interfaces;
using EposNowAPI.Core.Responses;
using EposNowAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace EposNowAPI.Core.EndPoints
{
    public sealed class BrandsEndPoint : EndPointBase<BrandsEndPoint, Brand> 
	, ICreateEndPoint<Brand>
    {
        private BrandsResponse Response { get; set; }

        public BrandsEndPoint(Authenticator auth) : base()
        {
            Response = new BrandsResponse(auth);
        }

        public Brand Create(Brand item)
        {
            return Response.Create(item);
        }

        public override Brand Find(Int32 id)
        {
            return Response.Find(id);
        }

        public override IEnumerable<Brand> Find()
        {
            IEnumerable<Brand> listToReturn = Response.Find(QueryParameters);
            QueryParameters.Reset();
            return listToReturn;
        }

        public override IEnumerable<Brand> FindAll()
        {
			QueryParameters.GetAll = true;
            QueryParameters.PageNo = 1;
            IEnumerable<Brand> listToReturn = Response.FindAll(QueryParameters);
            QueryParameters.Reset();
            return listToReturn;
        }

        public override BrandsEndPoint Where(Expression<Func<Brand, Boolean>> expression)
        {
            QueryParameters.Where.AddRange(expression.ToEposQueries());
            return this;
        }

        public override BrandsEndPoint And(Expression<Func<Brand, Boolean>> expression)
        {
            return Where(expression);
        }

        public override BrandsEndPoint OrderBy<T>(Expression<Func<Brand, T>> expression)
        {
            QueryParameters.Order = expression.ToEposOrderByQuery();
            return this;
        }

        public override BrandsEndPoint OrderByDesc<T>(Expression<Func<Brand, T>> expression)
        {
            QueryParameters.Order = expression.ToEposOrderByQuery(true);
            return this;
        }

        public override BrandsEndPoint Page(Int32 pageNo)
        {
            QueryParameters.PageNo = pageNo;
            return this;
        }
    }
}
