using EposNowAPI.Models;
using System;
using System.Collections.Generic;
using EposNowAPI.Architecture.Interfaces;

namespace EposNowAPI.Core.Responses
{
    public class BookKeepingReportsResponse : EposNowResponse<BookKeepingReport>, IEndPoint<BookKeepingReport>
    {
        public BookKeepingReportsResponse() : base() { }

        public BookKeepingReportsResponse(Authenticator auth)
            : base(auth)
        {

        }

        public new BookKeepingReport Create(BookKeepingReport itemToCreate)
        {
            return base.Create<BookKeepingReportsResponse>(itemToCreate);
        }

        public new BookKeepingReport Update(BookKeepingReport itemToUpdate)
        {
            return base.Update<BookKeepingReportsResponse>(itemToUpdate);
        }

        public bool Delete(BookKeepingReport item)
        {
            return base.Delete<BookKeepingReportsResponse>(item);
        }

        public new IEnumerable<BookKeepingReport> Get()
        {
            return base.Get<BookKeepingReportsResponse>();
        }

        public new IEnumerable<BookKeepingReport> Get(Int32 id)
        {
            return base.Get<BookKeepingReportsResponse>(id);
        }

        public IEnumerable<BookKeepingReport> Get(Int32 page, String where, String order)
        {
            return base.Get<BookKeepingReportsResponse>(page, where, order);
        }

        public IEnumerable<BookKeepingReport> Get<TKey1, TKey2>(System.Linq.Expressions.Expression<Func<BookKeepingReport, TKey1>> whereClause = null, System.Linq.Expressions.Expression<Func<BookKeepingReport, TKey2>> orderByClause = null)
        {
            return base.Get<BookKeepingReportsResponse, TKey1, TKey2>(whereClause, orderByClause);
        }
    }
}
