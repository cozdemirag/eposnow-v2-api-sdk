﻿using System;
using ApiData.Attributes;
using ApiData.BaseClasses;

namespace ApiData.Models
{
    [EposModel("LocationArea", "LocationAreas", IsCreatable = true, IsDeletable = true, IsUpdatable = true)]
    public class LocationArea : ModelBase
    {
        [EposID("LocationAreaID")]
        public Int32 LocationAreaID { get; set; }
        public String Name { get; set; }
        public String Description { get; set; }
        public String EmailAddress { get; set; }
    }
}
