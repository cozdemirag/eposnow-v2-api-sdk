using EposNowAPI.Models;
using System;
using System.Collections.Generic;
using EposNowAPI.Architecture.Interfaces;

namespace EposNowAPI.Core.Responses
{
    public class StockTransfersResponse : EposNowResponse<StockTransfer>, IEndPoint<StockTransfer>
    {
        public StockTransfersResponse() : base() { }

        public StockTransfersResponse(Authenticator auth)
            : base(auth)
        {

        }

        public new StockTransfer Create(StockTransfer itemToCreate)
        {
            return base.Create<StockTransfersResponse>(itemToCreate);
        }

        public new StockTransfer Update(StockTransfer itemToUpdate)
        {
            return base.Update<StockTransfersResponse>(itemToUpdate);
        }

        public bool Delete(StockTransfer item)
        {
            return base.Delete<StockTransfersResponse>(item);
        }

        public new IEnumerable<StockTransfer> Get()
        {
            return base.Get<StockTransfersResponse>();
        }

        public new IEnumerable<StockTransfer> Get(Int32 id)
        {
            return base.Get<StockTransfersResponse>(id);
        }

        public IEnumerable<StockTransfer> Get(Int32 page, String where, String order)
        {
            return base.Get<StockTransfersResponse>(page, where, order);
        }

        public IEnumerable<StockTransfer> Get<TKey1, TKey2>(System.Linq.Expressions.Expression<Func<StockTransfer, TKey1>> whereClause = null, System.Linq.Expressions.Expression<Func<StockTransfer, TKey2>> orderByClause = null)
        {
            return base.Get<StockTransfersResponse, TKey1, TKey2>(whereClause, orderByClause);
        }
    }
}
