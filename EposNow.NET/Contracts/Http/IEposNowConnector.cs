﻿using EposNowNET.Contracts.Convertors;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EposNowNET.Contracts.Http
{
    public interface IEposNowConnector
    {
        IDataTransformer DataTransformer { get; set; }
    }
}
