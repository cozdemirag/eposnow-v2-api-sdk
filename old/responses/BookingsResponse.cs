using EposNowAPI.Models;
using System;
using System.Collections.Generic;
using EposNowAPI.Architecture.Interfaces;

namespace EposNowAPI.Core.Responses
{
    public class BookingsResponse : EposNowResponse<Booking>, IEndPoint<Booking>
    {
        public BookingsResponse() : base() { }

        public BookingsResponse(Authenticator auth)
            : base(auth)
        {

        }

        public new Booking Create(Booking itemToCreate)
        {
            return base.Create<BookingsResponse>(itemToCreate);
        }

        public new Booking Update(Booking itemToUpdate)
        {
            return base.Update<BookingsResponse>(itemToUpdate);
        }

        public bool Delete(Booking item)
        {
            return base.Delete<BookingsResponse>(item);
        }

        public new IEnumerable<Booking> Get()
        {
            return base.Get<BookingsResponse>();
        }

        public new IEnumerable<Booking> Get(Int32 id)
        {
            return base.Get<BookingsResponse>(id);
        }

        public IEnumerable<Booking> Get(Int32 page, String where, String order)
        {
            return base.Get<BookingsResponse>(page, where, order);
        }

        public IEnumerable<Booking> Get<TKey1, TKey2>(System.Linq.Expressions.Expression<Func<Booking, TKey1>> whereClause = null, System.Linq.Expressions.Expression<Func<Booking, TKey2>> orderByClause = null)
        {
            return base.Get<BookingsResponse, TKey1, TKey2>(whereClause, orderByClause);
        }
    }
}
