using EposNowAPI.Models;
using System;
using System.Collections.Generic;
using EposNowAPI.Architecture.Interfaces;

namespace EposNowAPI.Core.Responses
{
    public class BookingItemsResponse : EposNowResponse<BookingItem>, IEndPoint<BookingItem>
    {
        public BookingItemsResponse() : base() { }

        public BookingItemsResponse(Authenticator auth)
            : base(auth)
        {

        }

        public new BookingItem Create(BookingItem itemToCreate)
        {
            return base.Create<BookingItemsResponse>(itemToCreate);
        }

        public new BookingItem Update(BookingItem itemToUpdate)
        {
            return base.Update<BookingItemsResponse>(itemToUpdate);
        }

        public bool Delete(BookingItem item)
        {
            return base.Delete<BookingItemsResponse>(item);
        }

        public new IEnumerable<BookingItem> Get()
        {
            return base.Get<BookingItemsResponse>();
        }

        public new IEnumerable<BookingItem> Get(Int32 id)
        {
            return base.Get<BookingItemsResponse>(id);
        }

        public IEnumerable<BookingItem> Get(Int32 page, String where, String order)
        {
            return base.Get<BookingItemsResponse>(page, where, order);
        }

        public IEnumerable<BookingItem> Get<TKey1, TKey2>(System.Linq.Expressions.Expression<Func<BookingItem, TKey1>> whereClause = null, System.Linq.Expressions.Expression<Func<BookingItem, TKey2>> orderByClause = null)
        {
            return base.Get<BookingItemsResponse, TKey1, TKey2>(whereClause, orderByClause);
        }
    }
}
