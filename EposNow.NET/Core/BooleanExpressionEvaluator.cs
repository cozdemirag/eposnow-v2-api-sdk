﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace EposNow.NET.Core
{
    public class BooleanExpressionEvaluator : ExpressionVisitor
    {
        private string Query { get; set; }


        public bool CanEvaluate(Expression exp)
        {
            var unaryExp = exp as UnaryExpression;
            var memberExp = unaryExp?.Operand as MemberExpression;
            return exp.NodeType == ExpressionType.MemberAccess || (exp.NodeType == ExpressionType.Not && memberExp != null);
        }

        public string EvaluateExpression(Expression exp)
        {
            this.Query = string.Empty;

            this.Visit(exp);

            return this.Query;
        }


        protected override Expression VisitUnary(UnaryExpression node)
        {
            var member = node.Operand as MemberExpression;

            if (member != null)
            {
                this.Query = $"search=({member.Member.Name}|=|False)";
            }
            return node;
        }

        protected override Expression VisitMember(MemberExpression member)
        {
            this.Query = $"search=({member.Member.Name}|=|True)";

            return member;
        }
    }
}
