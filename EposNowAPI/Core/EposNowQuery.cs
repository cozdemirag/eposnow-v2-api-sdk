﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace EposNowAPI.Core
{
    public class EposNowQuery<T> : IOrderedQueryable<T>
    {
        public Type ElementType { get { return typeof(T); }}
        public Expression Expression { get; internal set; }
        public IQueryProvider Provider { get; internal set; }

        public EposNowQuery(Authenticator auth)
        {
            Provider = new EposNowProvider(auth);
            Expression = Expression.Constant(this);
        }

        internal EposNowQuery(IQueryProvider provider, Expression expression)
        {
            Provider = provider;
            Expression = expression;
        }

        public IEnumerator<T> GetEnumerator()
        {
            return Provider.Execute<IEnumerable<T>>(Expression).GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }
    }
}
