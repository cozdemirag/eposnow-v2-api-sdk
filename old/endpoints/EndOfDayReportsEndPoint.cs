using EposNowAPI.Core.Responses;
using EposNowAPI.Models;
using System.Collections.Generic;
using System;
using System.Linq.Expressions;
using System.Text.RegularExpressions;
using EposNowAPI.Architecture.Interfaces;

namespace EposNowAPI.Core.EndPoints
{
    public class EndOfDayReportsEndPoint : IEndPoint<EndOfDayReport>
    {
        private EndOfDayReportsResponse Response;
        private Int32 pageNo;
        private String whereParams;
        private String orderParams;

        public EndOfDayReportsEndPoint(Authenticator auth)
        {
            Response = new EndOfDayReportsResponse(auth);
        }

        public EndOfDayReport Create(EndOfDayReport item)
        {
            return Response.Create(item);
        }

        public bool Delete(EndOfDayReport item)
        {
            return Response.Delete(item);
        }

        public IEnumerable<EndOfDayReport> Get()
        {
            IEnumerable<EndOfDayReport> listToReturn = Response.Get(pageNo, whereParams, orderParams);
            pageNo = 0;
            whereParams = "";
            orderParams = "";
            return listToReturn;
        }

        public IEnumerable<EndOfDayReport> Get(int id)
        {
            return Response.Get(id);
        }

        public IEnumerable<EndOfDayReport> Get<TKey1, TKey2>(System.Linq.Expressions.Expression<Func<EndOfDayReport, TKey1>> whereClause = null, System.Linq.Expressions.Expression<Func<EndOfDayReport, TKey2>> orderByClause = null)
        {
            return Response.Get<TKey1, TKey2>(whereClause, orderByClause);
        }

        public EndOfDayReport Update(EndOfDayReport item)
        {
            return Response.Update(item);
        }


        public EndOfDayReportsEndPoint Where(String param)
        {
            whereParams += param;
            return this;
        }

        public EndOfDayReportsEndPoint OrderBy(String param)
        {
            orderParams = "OrderBy=" + param;
            return this;
        }

        public EndOfDayReportsEndPoint OrderByDesc(String param)
        {
            orderParams = "OrderByDesc=" + param;
            return this;
        }

        public EndOfDayReportsEndPoint OrderByDesc<T>(Expression<Func<EndOfDayReport, T>> order)
        {
            orderParams = "OrderByDesc=" + new Regex(@"\s*").Replace(new Regex(@"(==)+").Replace(new Regex(@"([\(\)""]+)").Replace(new Regex(@"(" + order.Parameters[0].Name + @"{1}[\.]{1})").Replace(new Regex(@"(Convert\()+").Replace(order.Body.ToString(), "", 99), "", 99), "", 99), "=", 99), "", 99);
            return this;
        }

        public EndOfDayReportsEndPoint OrderBy<T>(Expression<Func<EndOfDayReport, T>> order)
        {
            orderParams = "OrderByDesc=" + new Regex(@"\s*").Replace(new Regex(@"(==)+").Replace(new Regex(@"([\(\)""]+)").Replace(new Regex(@"(" + order.Parameters[0].Name + @"{1}[\.]{1})").Replace(new Regex(@"(Convert\()+").Replace(order.Body.ToString(), "", 99), "", 99), "", 99), "=", 99), "", 99);
            return this;
        }

        public EndOfDayReportsEndPoint Page(Int32 page)
        {
            pageNo = page;
            return this;
        }

        public EndOfDayReportsEndPoint Where(params Expression<Func<EndOfDayReport, Boolean>>[] e)
        {
            whereParams = String.IsNullOrWhiteSpace(whereParams) ? "?" : whereParams;

            foreach (Expression<Func<EndOfDayReport, Boolean>> exp in e)
            {
                Func<EndOfDayReport, Boolean> daaa = exp.Compile();

                if (whereParams != "?" && !whereParams.EndsWith("&"))
                {
                    whereParams += "&";
                }

                if (exp.Body.ToString().Contains(".Contains("))
                {
                    whereParams += String.Format("{0}{1}{2}", "search=(", new Regex(@"\s*").Replace(new Regex(@"(==)+").Replace(new Regex(@"([\(\)""]+)").Replace(new Regex(@"(" + exp.Parameters[0].Name + @"{1}[\.]{1})").Replace(new Regex(@"(.Contains\()+").Replace(exp.Body.ToString(), "|like|", 99), "", 99), "", 99), "=", 99), "", 99), ")");
                }
                else if (exp.Body.ToString().Contains(".EndsWith("))
                {
                    whereParams += String.Format("{0}{1}{2}", "search=(", new Regex(@"\s*").Replace(new Regex(@"(==)+").Replace(new Regex(@"([\(\)""]+)").Replace(new Regex(@"(" + exp.Parameters[0].Name + @"{1}[\.]{1})").Replace(new Regex(@"(.EndsWith\()+").Replace(exp.Body.ToString(), "|EndsWith|", 99), "", 99), "", 99), "=", 99), "", 99), ")");
                }
                else if (exp.Body.ToString().Contains(".StartsWith("))
                {
                    whereParams += String.Format("{0}{1}{2}", "search=(", new Regex(@"\s*").Replace(new Regex(@"(==)+").Replace(new Regex(@"([\(\)""]+)").Replace(new Regex(@"(" + exp.Parameters[0].Name + @"{1}[\.]{1})").Replace(new Regex(@"(.StartsWith\()+").Replace(exp.Body.ToString(), "|StartsWith|", 99), "", 99), "", 99), "=", 99), "", 99), ")");
                }
                else if (exp.Body.ToString().Contains(">"))
                {
                    whereParams += String.Format("{0}{1}{2}", "search=(", new Regex(@"\s*").Replace(new Regex(@"(>)+").Replace(new Regex(@"([\(\)""]+)").Replace(new Regex(@"(" + exp.Parameters[0].Name + @"{1}[\.]{1})").Replace(new Regex(@"(Convert\()+").Replace(exp.Body.ToString(), "", 99), "", 99), "", 99), "|>|", 99), "", 99), ")");
                }
                else if (exp.Body.ToString().Contains(">="))
                {
                    whereParams += String.Format("{0}{1}{2}", "search=(", new Regex(@"\s*").Replace(new Regex(@"(>=)+").Replace(new Regex(@"([\(\)""]+)").Replace(new Regex(@"(" + exp.Parameters[0].Name + @"{1}[\.]{1})").Replace(new Regex(@"(Convert\()+").Replace(exp.Body.ToString(), "", 99), "", 99), "", 99), "|>=|", 99), "", 99), ")");
                }
                else if (exp.Body.ToString().Contains("<"))
                {
                    whereParams += String.Format("{0}{1}{2}", "search=(", new Regex(@"\s*").Replace(new Regex(@"(<)+").Replace(new Regex(@"([\(\)""]+)").Replace(new Regex(@"(" + exp.Parameters[0].Name + @"{1}[\.]{1})").Replace(new Regex(@"(Convert\()+").Replace(exp.Body.ToString(), "", 99), "", 99), "", 99), "|<|", 99), "", 99), ")");
                }
                else if (exp.Body.ToString().Contains("<="))
                {
                    whereParams += String.Format("{0}{1}{2}", "search=(", new Regex(@"\s*").Replace(new Regex(@"(<=)+").Replace(new Regex(@"([\(\)""]+)").Replace(new Regex(@"(" + exp.Parameters[0].Name + @"{1}[\.]{1})").Replace(new Regex(@"(Convert\()+").Replace(exp.Body.ToString(), "", 99), "", 99), "", 99), "|<=|", 99), "", 99), ")");
                }
                else if (exp.Body.ToString().Contains("=="))
                {
                    whereParams += new Regex(@"\s*").Replace(new Regex(@"(==)+").Replace(new Regex(@"([\(\)""]+)").Replace(new Regex(@"(" + exp.Parameters[0].Name + @"{1}[\.]{1})").Replace(new Regex(@"(Convert\()+").Replace(exp.Body.ToString(), "", 99), "", 99), "", 99), "=", 99), "", 99);
                }
                else
                {
                    throw new Exception(exp.Body + " has invalid operations for Epos Now API");
                }
            }
            return this;
        }
    }
}
