﻿using System;
using ApiData.Attributes;

namespace ApiData.Models
{
    [EposModel("CustomerType", "CustomerTypes")]
    public class CustomerType
    {
        [EposID("CustomerTypeID")]
        public Int32 CustomerTypeID { get; set; }
        public String Name { get; set; }
        public Single Discount { get; set; }
        public String Description { get; set; }
        public Int32 DefaultExpiryLength { get; set; }
        public Single DefaultMaxCredit { get; set; }
    }
}
