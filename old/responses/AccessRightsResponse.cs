using EposNowAPI.Architecture.Interfaces;
using EposNowAPI.Models;
using System;
using System.Collections.Generic;

namespace EposNowAPI.Core.Responses
{
    public class AccessRightsResponse : EposNowResponse<AccessRight>, IEndPoint<AccessRight>
    {
        public AccessRightsResponse() : base() { }

        public AccessRightsResponse(Authenticator auth)
            : base(auth)
        {

        }

        public new AccessRight Create(AccessRight itemToCreate)
        {
            return base.Create<AccessRightsResponse>(itemToCreate);
        }

        public new AccessRight Update(AccessRight itemToUpdate)
        {
            return base.Update<AccessRightsResponse>(itemToUpdate);
        }

        public bool Delete(AccessRight item)
        {
            return base.Delete<AccessRightsResponse>(item);
        }

        public new IEnumerable<AccessRight> Get()
        {
            return base.Get<AccessRightsResponse>();
        }

        public new IEnumerable<AccessRight> Get(Int32 id)
        {
            return base.Get<AccessRightsResponse>(id);
        }

        public IEnumerable<AccessRight> Get(Int32 page, String where, String order)
        {
            return base.Get<AccessRightsResponse>(page, where, order);
        }

        public IEnumerable<AccessRight> Get<TKey1, TKey2>(System.Linq.Expressions.Expression<Func<AccessRight, TKey1>> whereClause = null, System.Linq.Expressions.Expression<Func<AccessRight, TKey2>> orderByClause = null)
        {
            return base.Get<AccessRightsResponse, TKey1, TKey2>(whereClause, orderByClause);
        }
    }
}
