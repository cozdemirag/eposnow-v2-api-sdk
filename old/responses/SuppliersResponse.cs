using EposNowAPI.Models;
using System;
using System.Collections.Generic;
using EposNowAPI.Architecture.Interfaces;

namespace EposNowAPI.Core.Responses
{
    public class SuppliersResponse : EposNowResponse<Supplier>, IEndPoint<Supplier>
    {
        public SuppliersResponse() : base() { }

        public SuppliersResponse(Authenticator auth)
            : base(auth)
        {

        }

        public new Supplier Create(Supplier itemToCreate)
        {
            return base.Create<SuppliersResponse>(itemToCreate);
        }

        public new Supplier Update(Supplier itemToUpdate)
        {
            return base.Update<SuppliersResponse>(itemToUpdate);
        }

        public bool Delete(Supplier item)
        {
            return base.Delete<SuppliersResponse>(item);
        }

        public new IEnumerable<Supplier> Get()
        {
            return base.Get<SuppliersResponse>();
        }

        public new IEnumerable<Supplier> Get(Int32 id)
        {
            return base.Get<SuppliersResponse>(id);
        }

        public IEnumerable<Supplier> Get(Int32 page, String where, String order)
        {
            return base.Get<SuppliersResponse>(page, where, order);
        }

        public IEnumerable<Supplier> Get<TKey1, TKey2>(System.Linq.Expressions.Expression<Func<Supplier, TKey1>> whereClause = null, System.Linq.Expressions.Expression<Func<Supplier, TKey2>> orderByClause = null)
        {
            return base.Get<SuppliersResponse, TKey1, TKey2>(whereClause, orderByClause);
        }
    }
}
