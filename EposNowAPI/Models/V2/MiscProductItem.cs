﻿using EposNowAPI.Architecture.Attributes;
using EposNowAPI.Architecture.BaseClasses;
using System;

namespace EposNowAPI.Models.V2
{
    [EposModel("MiscProductItem", "MiscProductItems")]
    public class MiscProductItem : ModelBase
    {
        [EposID("MiscProductItemID")]
        public Int32 MiscProductItemID { get; set; }
        public String Name { get; set; }
        public Int32 TransactionID { get; set; }
        public Decimal Value { get; set; }
        public Decimal VATAmount { get; set; }
        public Int32? MiscProductID { get; set; }
        public Int32? TaxRateID { get; set; }
        public Int32? ClientID { get; set; }
    }
}
