﻿using EposNowAPI.Architecture.Attributes;

namespace EposNowAPI.Models.Constants
{
    public enum PurchaseOrderStatus
    {
        [EnumValue("Draft")]
        Draft,
        [EnumValue("Ordered")]
        Ordered,
        [EnumValue("Received")]
        Received,
        [EnumValue("Received (Partially)")]
        PartiallyReceived,
        [EnumValue("Incomplete")]
        Incomplete
    }
}
