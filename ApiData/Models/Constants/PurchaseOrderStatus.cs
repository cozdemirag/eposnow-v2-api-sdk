﻿using ApiData.Attributes;

namespace ApiData.Models.Constants
{
    public enum PurchaseOrderStatus
    {
        [EnumValue("Draft")]
        Draft,
        [EnumValue("Ordered")]
        Ordered,
        [EnumValue("Received")]
        Received,
        [EnumValue("Received (Partially")]
        PartiallyReceived,
        [EnumValue("Incomplete")]
        Incomplete
    }
}
