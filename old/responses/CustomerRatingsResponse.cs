using EposNowAPI.Models;
using System;
using System.Collections.Generic;
using EposNowAPI.Architecture.Interfaces;

namespace EposNowAPI.Core.Responses
{
    public class CustomerRatingsResponse : EposNowResponse<CustomerRating>, IEndPoint<CustomerRating>
    {
        public CustomerRatingsResponse() : base() { }

        public CustomerRatingsResponse(Authenticator auth)
            : base(auth)
        {

        }

        public new CustomerRating Create(CustomerRating itemToCreate)
        {
            return base.Create<CustomerRatingsResponse>(itemToCreate);
        }

        public new CustomerRating Update(CustomerRating itemToUpdate)
        {
            return base.Update<CustomerRatingsResponse>(itemToUpdate);
        }

        public bool Delete(CustomerRating item)
        {
            return base.Delete<CustomerRatingsResponse>(item);
        }

        public new IEnumerable<CustomerRating> Get()
        {
            return base.Get<CustomerRatingsResponse>();
        }

        public new IEnumerable<CustomerRating> Get(Int32 id)
        {
            return base.Get<CustomerRatingsResponse>(id);
        }

        public IEnumerable<CustomerRating> Get(Int32 page, String where, String order)
        {
            return base.Get<CustomerRatingsResponse>(page, where, order);
        }

        public IEnumerable<CustomerRating> Get<TKey1, TKey2>(System.Linq.Expressions.Expression<Func<CustomerRating, TKey1>> whereClause = null, System.Linq.Expressions.Expression<Func<CustomerRating, TKey2>> orderByClause = null)
        {
            return base.Get<CustomerRatingsResponse, TKey1, TKey2>(whereClause, orderByClause);
        }
    }
}
