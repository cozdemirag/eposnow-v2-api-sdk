using EposNowAPI.Architecture.BaseClasses;
using EposNowAPI.Architecture.Extensions;
using EposNowAPI.Architecture.Interfaces;
using EposNowAPI.Core.Responses;
using EposNowAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace EposNowAPI.Core.EndPoints
{
    public sealed class ProductStocksEndPoint : EndPointBase<ProductStocksEndPoint, ProductStock> 
    {
        private ProductStocksResponse Response { get; set; }

        public ProductStocksEndPoint(Authenticator auth) : base()
        {
            Response = new ProductStocksResponse(auth);
        }


        public override ProductStock Find(Int32 id)
        {
            return Response.Find(id);
        }

        public override IEnumerable<ProductStock> Find()
        {
            IEnumerable<ProductStock> listToReturn = Response.Find(QueryParameters);
            QueryParameters.Reset();
            return listToReturn;
        }

        public override IEnumerable<ProductStock> FindAll()
        {
			QueryParameters.GetAll = true;
            QueryParameters.PageNo = 1;
            IEnumerable<ProductStock> listToReturn = Response.FindAll(QueryParameters);
            QueryParameters.Reset();
            return listToReturn;
        }

        public override ProductStocksEndPoint Where(Expression<Func<ProductStock, Boolean>> expression)
        {
            QueryParameters.Where.AddRange(expression.ToEposQueries());
            return this;
        }

        public override ProductStocksEndPoint And(Expression<Func<ProductStock, Boolean>> expression)
        {
            return Where(expression);
        }

        public override ProductStocksEndPoint OrderBy<T>(Expression<Func<ProductStock, T>> expression)
        {
            QueryParameters.Order = expression.ToEposOrderByQuery();
            return this;
        }

        public override ProductStocksEndPoint OrderByDesc<T>(Expression<Func<ProductStock, T>> expression)
        {
            QueryParameters.Order = expression.ToEposOrderByQuery(true);
            return this;
        }

        public override ProductStocksEndPoint Page(Int32 pageNo)
        {
            QueryParameters.PageNo = pageNo;
            return this;
        }
    }
}
