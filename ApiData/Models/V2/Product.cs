﻿using ApiData.Attributes;
using ApiData.BaseClasses;
using ApiData.Models.Constants;
using Newtonsoft.Json;
using System;

namespace ApiData.Models
{
    [EposModel("Product", "Products", IsCreatable = true, IsDeletable = true, IsUpdatable = true)]
    public sealed class Product : ModelBase
    {
        //[JsonIgnore]
        [EposID("ProductID")]
        public Int32 ProductID { get; set; }
        public String Name { get; set; }
        public String Description { get; set; }
        public Decimal CostPrice { get; set; }
        public Decimal SalePrice { get; set; }
        public Decimal EatOutPrice { get; set; }
        public Int32? CategoryID { get; set; }
        public String Barcode { get; set; }
        public Int32? TaxRateID { get; set; }
        public Int32? EatoutTaxRateID { get; set; }
        public Int32? BrandID { get; set; }
        public Int32? SupplierID { get; set; }
        public Int32? PopupNoteID { get; set; }
        public Int32? UnitOfSale { get; set; }
        public Int32? VolumeOfSale { get; set; }
        public Int32? MultiChoiceID { get; set; }
        public Int32? ColourID { get; set; }
        public Int32? VariantGroupID { get; set; }
        public String Size { get; set; }
        public String SKU { get; set; }
        public Boolean SellOnWeb { get; set; }
        public Boolean SellOnTill { get; set; }
        public String OrderCode { get; set; }
        public Int32? ButtonColourID { get; set; }
        public Int32? SortPosition { get; set; }
        public Int32? MagentoAttributeSetID { get; set; }
        public Decimal? RRPrice { get; set; }
        public Int32? CostPriceTaxRateID { get; set; }
        [JsonProperty(PropertyName = "ProductType")]
        public ProductType Type { get; set; }
        public Int32? TareWeight { get; set; }
        public String ArticleCode { get; set; }
    }
}
