﻿using System;
using ApiData.Attributes;

namespace ApiData.Models
{
    [EposModel("MultipleChoiceProductGroupItem", "MultipleChoiceProductGroupItems")]
    public class MultipleChoiceProductGroupItem
    {
        [EposID("MultipleChoiceProductGroupItemID")]
        public Int32 MultipleChoiceProductGroupItemID { get; set; }
        public Int32 ProductID { get; set; }
        public Int32 MultipleChoiceProductGroupID { get; set; }
        public Int32 TillOrder { get; set; }
    }
}
