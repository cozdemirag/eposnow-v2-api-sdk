using EposNowAPI.Architecture.BaseClasses;
using EposNowAPI.Core.Tools.QueryTools;
using EposNowAPI.Models;
using System;
using System.Collections.Generic;

namespace EposNowAPI.Core.Responses
{
    public sealed class LocationsResponse : ResponseBase<Location>
    {
        public LocationsResponse() : base() { }

        public LocationsResponse(Authenticator auth)
            : base(auth)
        {
        }

        public Location Create(Location itemToCreate)
        {
            return base.Create<LocationsResponse>(itemToCreate);
        }

        public Location Update(Location itemToUpdate)
        {
            return base.Update<LocationsResponse>(itemToUpdate);
        }

        public Boolean Delete(Location itemToDelete)
        {
            return base.Delete<LocationsResponse>(itemToDelete);
        }

        public Boolean Delete(Int32 id)
        {
            return base.Delete<LocationsResponse>(id);
        }

        public Location Find(Int32 id)
        {
            return base.Find<LocationsResponse>(id);
        }

        public IEnumerable<Location> Find(QueryParameters<Location> parameters)
        {
            return base.Find<LocationsResponse>(parameters);
        }

        public IEnumerable<Location> FindAll(QueryParameters<Location> parameters)
        {
            return base.Find<LocationsResponse>(parameters);
        }
    }
}
