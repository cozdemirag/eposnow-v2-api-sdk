﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace EposNowAPI.Core.Tools.QueryTools.Translator
{
    class TranslatorConstants
    {
        public static class SearchOperators
        {
            public static String StartsWith = "StartsWith";
            public static String EndsWith = "EndsWith";
            public static String Contains = "Contains";
            public static String IsNull = "IsNull";
            public static String IsNotNull = "IsNotNull";
        }

        public static class BinaryOperators
        {
            public static String Equal = "=";
            public static String NotEqual= "<>";
            public static String GreaterThan = ">";
            public static String GreaterThanOrEqual = ">=";
            public static String LessThan = "<";
            public static String LessThanOrEqual = "<=";
        }

        public static class OrderByTypes
        {
            public static String OrderByAscending = "OrderBy";
            public static String OrderByDescending = "OrderByDesc";
        }

        public static String SearchQueryFormat = "search=({0}|{1}|{2})";
        public static String NullSearchQueryFormat = "search=({0}|{1}|)";
        public static String BinaryQueryFormat = "{0}{1}{2}";
        public static String ParameterSeparator = "&";
        public static List<String> AllowedMethodNames = new List<String>() { SearchOperators.StartsWith, SearchOperators.EndsWith, SearchOperators.Contains };
    }
}
