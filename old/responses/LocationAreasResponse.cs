using EposNowAPI.Models;
using System;
using System.Collections.Generic;
using EposNowAPI.Architecture.Interfaces;

namespace EposNowAPI.Core.Responses
{
    public class LocationAreasResponse : EposNowResponse<LocationArea>, IEndPoint<LocationArea>
    {
        public LocationAreasResponse() : base() { }

        public LocationAreasResponse(Authenticator auth)
            : base(auth)
        {

        }

        public new LocationArea Create(LocationArea itemToCreate)
        {
            return base.Create<LocationAreasResponse>(itemToCreate);
        }

        public new LocationArea Update(LocationArea itemToUpdate)
        {
            return base.Update<LocationAreasResponse>(itemToUpdate);
        }

        public bool Delete(LocationArea item)
        {
            return base.Delete<LocationAreasResponse>(item);
        }

        public new IEnumerable<LocationArea> Get()
        {
            return base.Get<LocationAreasResponse>();
        }

        public new IEnumerable<LocationArea> Get(Int32 id)
        {
            return base.Get<LocationAreasResponse>(id);
        }

        public IEnumerable<LocationArea> Get(Int32 page, String where, String order)
        {
            return base.Get<LocationAreasResponse>(page, where, order);
        }

        public IEnumerable<LocationArea> Get<TKey1, TKey2>(System.Linq.Expressions.Expression<Func<LocationArea, TKey1>> whereClause = null, System.Linq.Expressions.Expression<Func<LocationArea, TKey2>> orderByClause = null)
        {
            return base.Get<LocationAreasResponse, TKey1, TKey2>(whereClause, orderByClause);
        }
    }
}
