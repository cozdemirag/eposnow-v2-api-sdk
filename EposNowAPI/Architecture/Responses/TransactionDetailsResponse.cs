using EposNowAPI.Architecture.BaseClasses;
using EposNowAPI.Core.Tools.QueryTools;
using EposNowAPI.Models;
using System;
using System.Collections.Generic;

namespace EposNowAPI.Core.Responses
{
    public sealed class TransactionDetailsResponse : ResponseBase<TransactionDetail>
    {
        public TransactionDetailsResponse() : base() { }

        public TransactionDetailsResponse(Authenticator auth)
            : base(auth)
        {
        }

        public TransactionDetail Create(TransactionDetail itemToCreate)
        {
            return base.Create<TransactionDetailsResponse>(itemToCreate);
        }

        public TransactionDetail Update(TransactionDetail itemToUpdate)
        {
            return base.Update<TransactionDetailsResponse>(itemToUpdate);
        }

        public Boolean Delete(TransactionDetail itemToDelete)
        {
            return base.Delete<TransactionDetailsResponse>(itemToDelete);
        }

        public Boolean Delete(Int32 id)
        {
            return base.Delete<TransactionDetailsResponse>(id);
        }

        public TransactionDetail Find(Int32 id)
        {
            return base.Find<TransactionDetailsResponse>(id);
        }

        public IEnumerable<TransactionDetail> Find(QueryParameters<TransactionDetail> parameters)
        {
            return base.Find<TransactionDetailsResponse>(parameters);
        }

        public IEnumerable<TransactionDetail> FindAll(QueryParameters<TransactionDetail> parameters)
        {
            return base.Find<TransactionDetailsResponse>(parameters);
        }
    }
}
