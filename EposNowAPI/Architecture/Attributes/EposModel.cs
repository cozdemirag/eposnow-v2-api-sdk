﻿using System;

namespace EposNowAPI.Architecture.Attributes
{
    [AttributeUsage(AttributeTargets.Class , AllowMultiple = false)]
    public sealed class EposModel : Attribute
    {
        public String PluralName { get; internal set; }
        public String EndPointName { get; internal set; }
        public String EndPointVersion { get; internal set; }
        
        public EposModel(String endpoint, String plural = "", String version = "V2")
        {
            PluralName = plural;
            EndPointName = endpoint;
            EndPointVersion = version;
        }
    }
}
