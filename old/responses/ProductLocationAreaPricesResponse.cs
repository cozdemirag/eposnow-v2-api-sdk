using EposNowAPI.Models;
using System;
using System.Collections.Generic;
using EposNowAPI.Architecture.Interfaces;

namespace EposNowAPI.Core.Responses
{
    public class ProductLocationAreaPricesResponse : EposNowResponse<ProductLocationAreaPrice>, IEndPoint<ProductLocationAreaPrice>
    {
        public ProductLocationAreaPricesResponse() : base() { }

        public ProductLocationAreaPricesResponse(Authenticator auth)
            : base(auth)
        {

        }

        public new ProductLocationAreaPrice Create(ProductLocationAreaPrice itemToCreate)
        {
            return base.Create<ProductLocationAreaPricesResponse>(itemToCreate);
        }

        public new ProductLocationAreaPrice Update(ProductLocationAreaPrice itemToUpdate)
        {
            return base.Update<ProductLocationAreaPricesResponse>(itemToUpdate);
        }

        public bool Delete(ProductLocationAreaPrice item)
        {
            return base.Delete<ProductLocationAreaPricesResponse>(item);
        }

        public new IEnumerable<ProductLocationAreaPrice> Get()
        {
            return base.Get<ProductLocationAreaPricesResponse>();
        }

        public new IEnumerable<ProductLocationAreaPrice> Get(Int32 id)
        {
            return base.Get<ProductLocationAreaPricesResponse>(id);
        }

        public IEnumerable<ProductLocationAreaPrice> Get(Int32 page, String where, String order)
        {
            return base.Get<ProductLocationAreaPricesResponse>(page, where, order);
        }

        public IEnumerable<ProductLocationAreaPrice> Get<TKey1, TKey2>(System.Linq.Expressions.Expression<Func<ProductLocationAreaPrice, TKey1>> whereClause = null, System.Linq.Expressions.Expression<Func<ProductLocationAreaPrice, TKey2>> orderByClause = null)
        {
            return base.Get<ProductLocationAreaPricesResponse, TKey1, TKey2>(whereClause, orderByClause);
        }
    }
}
