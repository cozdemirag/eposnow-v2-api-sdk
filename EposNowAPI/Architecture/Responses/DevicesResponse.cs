using EposNowAPI.Architecture.BaseClasses;
using EposNowAPI.Core.Tools.QueryTools;
using EposNowAPI.Models;
using System;
using System.Collections.Generic;

namespace EposNowAPI.Core.Responses
{
    public sealed class DevicesResponse : ResponseBase<Device>
    {
        public DevicesResponse() : base() { }

        public DevicesResponse(Authenticator auth)
            : base(auth)
        {
        }

        public Device Create(Device itemToCreate)
        {
            return base.Create<DevicesResponse>(itemToCreate);
        }

        public Device Update(Device itemToUpdate)
        {
            return base.Update<DevicesResponse>(itemToUpdate);
        }

        public Boolean Delete(Device itemToDelete)
        {
            return base.Delete<DevicesResponse>(itemToDelete);
        }

        public Boolean Delete(Int32 id)
        {
            return base.Delete<DevicesResponse>(id);
        }

        public Device Find(Int32 id)
        {
            return base.Find<DevicesResponse>(id);
        }

        public IEnumerable<Device> Find(QueryParameters<Device> parameters)
        {
            return base.Find<DevicesResponse>(parameters);
        }

        public IEnumerable<Device> FindAll(QueryParameters<Device> parameters)
        {
            return base.Find<DevicesResponse>(parameters);
        }
    }
}
