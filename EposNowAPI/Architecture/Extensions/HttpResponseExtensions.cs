﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EposNowAPI.Architecture.Extensions
{
    public static class HttpResponseExtensions
    {
        public static IEnumerable<TModel> ConvertTo<TModel>(this String json)
        {
            try
            {
                if (!json.StartsWith("[") && !json.EndsWith("]"))
                {
                    json = String.Format("[{0}]", json);
                }

                return JsonConvert.DeserializeObject<IEnumerable<TModel>>(json);
            }
            catch (Exception ex)
            {
                return null;
            }
        }

    }
}
