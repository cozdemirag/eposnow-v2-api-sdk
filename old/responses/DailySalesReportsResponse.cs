using EposNowAPI.Models;
using System;
using System.Collections.Generic;
using EposNowAPI.Architecture.Interfaces;

namespace EposNowAPI.Core.Responses
{
    public class DailySalesReportsResponse : EposNowResponse<DailySalesReport>, IEndPoint<DailySalesReport>
    {
        public DailySalesReportsResponse() : base() { }

        public DailySalesReportsResponse(Authenticator auth)
            : base(auth)
        {

        }

        public new DailySalesReport Create(DailySalesReport itemToCreate)
        {
            return base.Create<DailySalesReportsResponse>(itemToCreate);
        }

        public new DailySalesReport Update(DailySalesReport itemToUpdate)
        {
            return base.Update<DailySalesReportsResponse>(itemToUpdate);
        }

        public bool Delete(DailySalesReport item)
        {
            return base.Delete<DailySalesReportsResponse>(item);
        }

        public new IEnumerable<DailySalesReport> Get()
        {
            return base.Get<DailySalesReportsResponse>();
        }

        public new IEnumerable<DailySalesReport> Get(Int32 id)
        {
            return base.Get<DailySalesReportsResponse>(id);
        }

        public IEnumerable<DailySalesReport> Get(Int32 page, String where, String order)
        {
            return base.Get<DailySalesReportsResponse>(page, where, order);
        }

        public IEnumerable<DailySalesReport> Get<TKey1, TKey2>(System.Linq.Expressions.Expression<Func<DailySalesReport, TKey1>> whereClause = null, System.Linq.Expressions.Expression<Func<DailySalesReport, TKey2>> orderByClause = null)
        {
            return base.Get<DailySalesReportsResponse, TKey1, TKey2>(whereClause, orderByClause);
        }
    }
}
