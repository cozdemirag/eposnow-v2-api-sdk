using EposNowAPI.Architecture.BaseClasses;
using EposNowAPI.Core.Tools.QueryTools;
using EposNowAPI.Models;
using System;
using System.Collections.Generic;

namespace EposNowAPI.Core.Responses
{
    public sealed class BrandsResponse : ResponseBase<Brand>
    {
        public BrandsResponse() : base() { }

        public BrandsResponse(Authenticator auth)
            : base(auth)
        {
        }

        public Brand Create(Brand itemToCreate)
        {
            return base.Create<BrandsResponse>(itemToCreate);
        }

        public Brand Update(Brand itemToUpdate)
        {
            return base.Update<BrandsResponse>(itemToUpdate);
        }

        public Boolean Delete(Brand itemToDelete)
        {
            return base.Delete<BrandsResponse>(itemToDelete);
        }

        public Boolean Delete(Int32 id)
        {
            return base.Delete<BrandsResponse>(id);
        }

        public Brand Find(Int32 id)
        {
            return base.Find<BrandsResponse>(id);
        }

        public IEnumerable<Brand> Find(QueryParameters<Brand> parameters)
        {
            return base.Find<BrandsResponse>(parameters);
        }

        public IEnumerable<Brand> FindAll(QueryParameters<Brand> parameters)
        {
            return base.Find<BrandsResponse>(parameters);
        }
    }
}
