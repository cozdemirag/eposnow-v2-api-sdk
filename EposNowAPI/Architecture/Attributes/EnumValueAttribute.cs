﻿using System;

namespace EposNowAPI.Architecture.Attributes
{
    public sealed class EnumValue : Attribute
    {
        public Object Value { get; internal set; }

        public EnumValue(Object value)
        {
            Value = value;
        }
    }
}
