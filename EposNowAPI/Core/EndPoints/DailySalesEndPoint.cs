using EposNowAPI.Architecture.BaseClasses;
using EposNowAPI.Architecture.Extensions;
using EposNowAPI.Architecture.Interfaces;
using EposNowAPI.Core.Responses;
using EposNowAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace EposNowAPI.Core.EndPoints
{
    public sealed class DailySalesEndPoint : EndPointBase<DailySalesEndPoint, DailySalesReport> 
    {
        private DailySalesResponse Response { get; set; }

        public DailySalesEndPoint(Authenticator auth) : base()
        {
            Response = new DailySalesResponse(auth);
        }


        public override DailySalesReport Find(Int32 id)
        {
            return Response.Find(id);
        }

        public override IEnumerable<DailySalesReport> Find()
        {
            IEnumerable<DailySalesReport> listToReturn = Response.Find(QueryParameters);
            QueryParameters.Reset();
            return listToReturn;
        }

        public override IEnumerable<DailySalesReport> FindAll()
        {
			QueryParameters.GetAll = true;
            QueryParameters.PageNo = 1;
            IEnumerable<DailySalesReport> listToReturn = Response.FindAll(QueryParameters);
            QueryParameters.Reset();
            return listToReturn;
        }

        public override DailySalesEndPoint Where(Expression<Func<DailySalesReport, Boolean>> expression)
        {
            QueryParameters.Where.AddRange(expression.ToEposQueries());
            return this;
        }

        public override DailySalesEndPoint And(Expression<Func<DailySalesReport, Boolean>> expression)
        {
            return Where(expression);
        }

        public override DailySalesEndPoint OrderBy<T>(Expression<Func<DailySalesReport, T>> expression)
        {
            QueryParameters.Order = expression.ToEposOrderByQuery();
            return this;
        }

        public override DailySalesEndPoint OrderByDesc<T>(Expression<Func<DailySalesReport, T>> expression)
        {
            QueryParameters.Order = expression.ToEposOrderByQuery(true);
            return this;
        }

        public override DailySalesEndPoint Page(Int32 pageNo)
        {
            QueryParameters.PageNo = pageNo;
            return this;
        }
    }
}
