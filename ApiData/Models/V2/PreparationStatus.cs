﻿using System;
using ApiData.Attributes;

namespace ApiData.Models
{
    [EposModel("PreparationStatus", "PreparationStatuses")]
    public class PreparationStatus
    {
        [EposID("PreparationStatusID")]
        public Int32 PreparationStatusID { get; set; }
        public Int32 TransactionID { get; set; }
        public String Barcode { get; set; }
        public Int32 State { get; set; }
        public DateTime CreatedTime { get; set; }
        public Int32 StaffID { get; set; }
        public Int32 DeviceID { get; set; }
    }
}
