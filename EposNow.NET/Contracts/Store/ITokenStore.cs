﻿using EposNowNET.Contracts.Entities;
using System;

namespace EposNowNET.Contracts.Store
{
    public interface ITokenStore
    {
        void Remove(String user);
        IEposToken Find(String user);
        IEposToken Add(String user, String token);
    }
}
