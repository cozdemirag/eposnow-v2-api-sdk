using EposNowAPI.Architecture.BaseClasses;
using EposNowAPI.Architecture.Extensions;
using EposNowAPI.Architecture.Interfaces;
using EposNowAPI.Core.Responses;
using EposNowAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace EposNowAPI.Core.EndPoints
{
    public sealed class LocationsEndPoint : EndPointBase<LocationsEndPoint, Location> 
	, IUpdateEndPoint<Location>
	, IDeleteEndPoint<Location>
	, ICreateEndPoint<Location>
    {
        private LocationsResponse Response { get; set; }

        public LocationsEndPoint(Authenticator auth) : base()
        {
            Response = new LocationsResponse(auth);
        }

        public Location Create(Location item)
        {
            return Response.Create(item);
        }
		public Location Update(Location item)
        {
            return Response.Update(item);
        }
		public bool Delete(int id)
        {
            return Response.Delete(id);
        }

		public bool Delete(Location item)
        {
            return Response.Delete(item);
        }
		
        public override Location Find(Int32 id)
        {
            return Response.Find(id);
        }

        public override IEnumerable<Location> Find()
        {
            IEnumerable<Location> listToReturn = Response.Find(QueryParameters);
            QueryParameters.Reset();
            return listToReturn;
        }

        public override IEnumerable<Location> FindAll()
        {
			QueryParameters.GetAll = true;
            QueryParameters.PageNo = 1;
            IEnumerable<Location> listToReturn = Response.FindAll(QueryParameters);
            QueryParameters.Reset();
            return listToReturn;
        }

        public override LocationsEndPoint Where(Expression<Func<Location, Boolean>> expression)
        {
            QueryParameters.Where.AddRange(expression.ToEposQueries());
            return this;
        }

        public override LocationsEndPoint And(Expression<Func<Location, Boolean>> expression)
        {
            return Where(expression);
        }

        public override LocationsEndPoint OrderBy<T>(Expression<Func<Location, T>> expression)
        {
            QueryParameters.Order = expression.ToEposOrderByQuery();
            return this;
        }

        public override LocationsEndPoint OrderByDesc<T>(Expression<Func<Location, T>> expression)
        {
            QueryParameters.Order = expression.ToEposOrderByQuery(true);
            return this;
        }

        public override LocationsEndPoint Page(Int32 pageNo)
        {
            QueryParameters.PageNo = pageNo;
            return this;
        }
    }
}
