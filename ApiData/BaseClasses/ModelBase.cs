﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ApiData.BaseClasses
{
    public abstract class ModelBase
    {
        [JsonIgnore]
        public List<String> ResponseMessages { get; set; }
        [JsonIgnore]
        public Boolean SuccessfulResponse { get; internal set; }

        public ModelBase()
        {
            ResponseMessages = new List<String>();
        }
    }
}
