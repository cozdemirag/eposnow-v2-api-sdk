﻿using ApiData.Attributes;
using ApiData.BaseClasses;
using System;

namespace ApiData.Models
{
    [EposModel("CategoryReferenceCode", "CategoryReferenceCodes")]
    public class CategoryReferenceCode : ModelBase
    {
        [EposID("CategoryReferenceCodeID")]
        public Int32 CategoryReferenceCodeID { get; set; }
        public Int32 CategoryID { get; set; }
        public Int32 AppID { get; set; }
        public String ReferenceCode { get; set; }
    }
}
