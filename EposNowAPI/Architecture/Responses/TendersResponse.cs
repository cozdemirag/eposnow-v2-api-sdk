using EposNowAPI.Architecture.BaseClasses;
using EposNowAPI.Core.Tools.QueryTools;
using EposNowAPI.Models;
using System;
using System.Collections.Generic;

namespace EposNowAPI.Core.Responses
{
    public sealed class TendersResponse : ResponseBase<Tender>
    {
        public TendersResponse() : base() { }

        public TendersResponse(Authenticator auth)
            : base(auth)
        {
        }

        public Tender Create(Tender itemToCreate)
        {
            return base.Create<TendersResponse>(itemToCreate);
        }

        public Tender Update(Tender itemToUpdate)
        {
            return base.Update<TendersResponse>(itemToUpdate);
        }

        public Boolean Delete(Tender itemToDelete)
        {
            return base.Delete<TendersResponse>(itemToDelete);
        }

        public Boolean Delete(Int32 id)
        {
            return base.Delete<TendersResponse>(id);
        }

        public Tender Find(Int32 id)
        {
            return base.Find<TendersResponse>(id);
        }

        public IEnumerable<Tender> Find(QueryParameters<Tender> parameters)
        {
            return base.Find<TendersResponse>(parameters);
        }

        public IEnumerable<Tender> FindAll(QueryParameters<Tender> parameters)
        {
            return base.Find<TendersResponse>(parameters);
        }
    }
}
