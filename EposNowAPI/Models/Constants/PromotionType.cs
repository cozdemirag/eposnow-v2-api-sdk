﻿using EposNowAPI.Architecture.Attributes;

namespace EposNowAPI.Models.Constants
{
    public enum PromotionType
    {
        [EnumValue(1)]
        XForY = 1,
        [EnumValue(2)]
        XForCurrency = 2,
        [EnumValue(11)]
        PercentagePromotion = 11,
        [EnumValue(21)]
        SpendOverGetPercentage = 21,
        [EnumValue(22)]
        SpendOverGetCurrency = 22,
        [EnumValue(51)]
        BonusPoints = 51,
        [EnumValue(52)]
        MultiplePoints = 52,
    }
}
