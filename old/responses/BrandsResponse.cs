using EposNowAPI.Models;
using System;
using System.Collections.Generic;
using EposNowAPI.Architecture.Interfaces;

namespace EposNowAPI.Core.Responses
{
    public class BrandsResponse : EposNowResponse<Brand>, IEndPoint<Brand>
    {
        public BrandsResponse() : base() { }

        public BrandsResponse(Authenticator auth)
            : base(auth)
        {

        }

        public new Brand Create(Brand itemToCreate)
        {
            return base.Create<BrandsResponse>(itemToCreate);
        }

        public new Brand Update(Brand itemToUpdate)
        {
            return base.Update<BrandsResponse>(itemToUpdate);
        }

        public bool Delete(Brand item)
        {
            return base.Delete<BrandsResponse>(item);
        }

        public new IEnumerable<Brand> Get()
        {
            return base.Get<BrandsResponse>();
        }

        public new IEnumerable<Brand> Get(Int32 id)
        {
            return base.Get<BrandsResponse>(id);
        }

        public IEnumerable<Brand> Get(Int32 page, String where, String order)
        {
            return base.Get<BrandsResponse>(page, where, order);
        }

        public IEnumerable<Brand> Get<TKey1, TKey2>(System.Linq.Expressions.Expression<Func<Brand, TKey1>> whereClause = null, System.Linq.Expressions.Expression<Func<Brand, TKey2>> orderByClause = null)
        {
            return base.Get<BrandsResponse, TKey1, TKey2>(whereClause, orderByClause);
        }
    }
}
