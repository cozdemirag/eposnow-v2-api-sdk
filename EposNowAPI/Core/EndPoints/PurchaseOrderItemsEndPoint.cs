using EposNowAPI.Architecture.BaseClasses;
using EposNowAPI.Architecture.Extensions;
using EposNowAPI.Architecture.Interfaces;
using EposNowAPI.Core.Responses;
using EposNowAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace EposNowAPI.Core.EndPoints
{
    public sealed class PurchaseOrderItemsEndPoint : EndPointBase<PurchaseOrderItemsEndPoint, PurchaseOrderItem> 
    {
        private PurchaseOrderItemsResponse Response { get; set; }

        public PurchaseOrderItemsEndPoint(Authenticator auth) : base()
        {
            Response = new PurchaseOrderItemsResponse(auth);
        }


        public override PurchaseOrderItem Find(Int32 id)
        {
            return Response.Find(id);
        }

        public override IEnumerable<PurchaseOrderItem> Find()
        {
            IEnumerable<PurchaseOrderItem> listToReturn = Response.Find(QueryParameters);
            QueryParameters.Reset();
            return listToReturn;
        }

        public override IEnumerable<PurchaseOrderItem> FindAll()
        {
			QueryParameters.GetAll = true;
            QueryParameters.PageNo = 1;
            IEnumerable<PurchaseOrderItem> listToReturn = Response.FindAll(QueryParameters);
            QueryParameters.Reset();
            return listToReturn;
        }

        public override PurchaseOrderItemsEndPoint Where(Expression<Func<PurchaseOrderItem, Boolean>> expression)
        {
            QueryParameters.Where.AddRange(expression.ToEposQueries());
            return this;
        }

        public override PurchaseOrderItemsEndPoint And(Expression<Func<PurchaseOrderItem, Boolean>> expression)
        {
            return Where(expression);
        }

        public override PurchaseOrderItemsEndPoint OrderBy<T>(Expression<Func<PurchaseOrderItem, T>> expression)
        {
            QueryParameters.Order = expression.ToEposOrderByQuery();
            return this;
        }

        public override PurchaseOrderItemsEndPoint OrderByDesc<T>(Expression<Func<PurchaseOrderItem, T>> expression)
        {
            QueryParameters.Order = expression.ToEposOrderByQuery(true);
            return this;
        }

        public override PurchaseOrderItemsEndPoint Page(Int32 pageNo)
        {
            QueryParameters.PageNo = pageNo;
            return this;
        }
    }
}
