﻿using EposNowAPI.Core.Tools.QueryTools.Translator;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace EposNowAPI.Core.Tools.QueryTools
{
    public class QueryParameters<T>
    {
        public List<String> Where { get; set; }
        public String Order { get; set; }
        public Int32 PageNo { get; set; }
        public Boolean GetAll { get; set; }

        private String QueryString { get; set; }

        public QueryParameters()
        {
            Where = new List<String>();
            QueryString = String.Empty;
            GetAll = false;
            PageNo = 1;
        }

        public String MakeWhereQueryString()
        {
            QueryString = String.Empty;

            foreach (String expression in Where)
            {
                AppendWhereParameter(expression);
            }

            //AppendOrderByParameter()
            //AppendPageParameter();

            Console.WriteLine(QueryString);
            return QueryString;
        }

        public void Reset()
        {
            Where = new List<String>();
            QueryString = String.Empty;
            PageNo = 1;
            Order = null;
            GetAll = false;
        }

        private void AppendWhereParameter(String paramToAdd)
        {
            if (!String.IsNullOrWhiteSpace(paramToAdd))
            {
                QueryString += (!QueryString.EndsWith(TranslatorConstants.ParameterSeparator) ? (String.IsNullOrWhiteSpace(QueryString) ? String.Empty : TranslatorConstants.ParameterSeparator) : String.Empty) + paramToAdd;
            }
        }

        private void AppendOrderByParameter(String paramToAdd)
        {
            if (!String.IsNullOrWhiteSpace(paramToAdd))
            {
                QueryString += (!QueryString.EndsWith(TranslatorConstants.ParameterSeparator) ? TranslatorConstants.ParameterSeparator : String.Empty) + paramToAdd;
            }
        }

        private void AppendPageParameter()
        {
            if (PageNo > 0)
            {
                QueryString += (!QueryString.EndsWith(TranslatorConstants.ParameterSeparator) ? TranslatorConstants.ParameterSeparator : String.Empty) + "page=" + PageNo;
            }
        }
    }
}
