using EposNowAPI.Models;
using System;
using System.Collections.Generic;
using EposNowAPI.Architecture.Interfaces;

namespace EposNowAPI.Core.Responses
{
    public class SeatingTablesResponse : EposNowResponse<SeatingTable>, IEndPoint<SeatingTable>
    {
        public SeatingTablesResponse() : base() { }

        public SeatingTablesResponse(Authenticator auth)
            : base(auth)
        {

        }

        public new SeatingTable Create(SeatingTable itemToCreate)
        {
            return base.Create<SeatingTablesResponse>(itemToCreate);
        }

        public new SeatingTable Update(SeatingTable itemToUpdate)
        {
            return base.Update<SeatingTablesResponse>(itemToUpdate);
        }

        public bool Delete(SeatingTable item)
        {
            return base.Delete<SeatingTablesResponse>(item);
        }

        public new IEnumerable<SeatingTable> Get()
        {
            return base.Get<SeatingTablesResponse>();
        }

        public new IEnumerable<SeatingTable> Get(Int32 id)
        {
            return base.Get<SeatingTablesResponse>(id);
        }

        public IEnumerable<SeatingTable> Get(Int32 page, String where, String order)
        {
            return base.Get<SeatingTablesResponse>(page, where, order);
        }

        public IEnumerable<SeatingTable> Get<TKey1, TKey2>(System.Linq.Expressions.Expression<Func<SeatingTable, TKey1>> whereClause = null, System.Linq.Expressions.Expression<Func<SeatingTable, TKey2>> orderByClause = null)
        {
            return base.Get<SeatingTablesResponse, TKey1, TKey2>(whereClause, orderByClause);
        }
    }
}
