﻿using System;

namespace EposNowAPI.Core
{
    public partial class EposNowApi : IDisposable
    {
        public EposNowApi(String baseUrl, String authToken) : this(new Authenticator() { BaseUrl = baseUrl, AuthenticationToken = authToken })
        {
        }

        public EposNowApi(String authToken) : this(new Authenticator() { BaseUrl = "https://api.eposnowhq.com/", AuthenticationToken = authToken })
        {
        }

        public void Dispose()
        {
        }
    }
   
}
