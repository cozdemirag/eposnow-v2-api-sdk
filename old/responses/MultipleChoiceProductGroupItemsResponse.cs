using EposNowAPI.Models;
using System;
using System.Collections.Generic;
using EposNowAPI.Architecture.Interfaces;

namespace EposNowAPI.Core.Responses
{
    public class MultipleChoiceProductGroupItemsResponse : EposNowResponse<MultipleChoiceProductGroupItem>, IEndPoint<MultipleChoiceProductGroupItem>
    {
        public MultipleChoiceProductGroupItemsResponse() : base() { }

        public MultipleChoiceProductGroupItemsResponse(Authenticator auth)
            : base(auth)
        {

        }

        public new MultipleChoiceProductGroupItem Create(MultipleChoiceProductGroupItem itemToCreate)
        {
            return base.Create<MultipleChoiceProductGroupItemsResponse>(itemToCreate);
        }

        public new MultipleChoiceProductGroupItem Update(MultipleChoiceProductGroupItem itemToUpdate)
        {
            return base.Update<MultipleChoiceProductGroupItemsResponse>(itemToUpdate);
        }

        public bool Delete(MultipleChoiceProductGroupItem item)
        {
            return base.Delete<MultipleChoiceProductGroupItemsResponse>(item);
        }

        public new IEnumerable<MultipleChoiceProductGroupItem> Get()
        {
            return base.Get<MultipleChoiceProductGroupItemsResponse>();
        }

        public new IEnumerable<MultipleChoiceProductGroupItem> Get(Int32 id)
        {
            return base.Get<MultipleChoiceProductGroupItemsResponse>(id);
        }

        public IEnumerable<MultipleChoiceProductGroupItem> Get(Int32 page, String where, String order)
        {
            return base.Get<MultipleChoiceProductGroupItemsResponse>(page, where, order);
        }

        public IEnumerable<MultipleChoiceProductGroupItem> Get<TKey1, TKey2>(System.Linq.Expressions.Expression<Func<MultipleChoiceProductGroupItem, TKey1>> whereClause = null, System.Linq.Expressions.Expression<Func<MultipleChoiceProductGroupItem, TKey2>> orderByClause = null)
        {
            return base.Get<MultipleChoiceProductGroupItemsResponse, TKey1, TKey2>(whereClause, orderByClause);
        }
    }
}
