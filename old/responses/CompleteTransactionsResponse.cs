using EposNowAPI.Models;
using System;
using System.Collections.Generic;
using EposNowAPI.Architecture.Interfaces;

namespace EposNowAPI.Core.Responses
{
    public class CompleteTransactionsResponse : EposNowResponse<CompleteTransaction>, IEndPoint<CompleteTransaction>
    {
        public CompleteTransactionsResponse() : base() { }

        public CompleteTransactionsResponse(Authenticator auth)
            : base(auth)
        {

        }

        public new CompleteTransaction Create(CompleteTransaction itemToCreate)
        {
            return base.Create<CompleteTransactionsResponse>(itemToCreate);
        }

        public new CompleteTransaction Update(CompleteTransaction itemToUpdate)
        {
            return base.Update<CompleteTransactionsResponse>(itemToUpdate);
        }

        public bool Delete(CompleteTransaction item)
        {
            return base.Delete<CompleteTransactionsResponse>(item);
        }

        public new IEnumerable<CompleteTransaction> Get()
        {
            return base.Get<CompleteTransactionsResponse>();
        }

        public new IEnumerable<CompleteTransaction> Get(Int32 id)
        {
            return base.Get<CompleteTransactionsResponse>(id);
        }

        public IEnumerable<CompleteTransaction> Get(Int32 page, String where, String order)
        {
            return base.Get<CompleteTransactionsResponse>(page, where, order);
        }

        public IEnumerable<CompleteTransaction> Get<TKey1, TKey2>(System.Linq.Expressions.Expression<Func<CompleteTransaction, TKey1>> whereClause = null, System.Linq.Expressions.Expression<Func<CompleteTransaction, TKey2>> orderByClause = null)
        {
            return base.Get<CompleteTransactionsResponse, TKey1, TKey2>(whereClause, orderByClause);
        }
    }
}
