using EposNowAPI.Architecture.BaseClasses;
using EposNowAPI.Architecture.Extensions;
using EposNowAPI.Architecture.Interfaces;
using EposNowAPI.Core.Responses;
using EposNowAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace EposNowAPI.Core.EndPoints
{
    public sealed class EndOfDayReferenceCodesEndPoint : EndPointBase<EndOfDayReferenceCodesEndPoint, EndOfDayReferenceCode> 
    {
        private EndOfDayReferenceCodesResponse Response { get; set; }

        public EndOfDayReferenceCodesEndPoint(Authenticator auth) : base()
        {
            Response = new EndOfDayReferenceCodesResponse(auth);
        }


        public override EndOfDayReferenceCode Find(Int32 id)
        {
            return Response.Find(id);
        }

        public override IEnumerable<EndOfDayReferenceCode> Find()
        {
            IEnumerable<EndOfDayReferenceCode> listToReturn = Response.Find(QueryParameters);
            QueryParameters.Reset();
            return listToReturn;
        }

        public override IEnumerable<EndOfDayReferenceCode> FindAll()
        {
			QueryParameters.GetAll = true;
            QueryParameters.PageNo = 1;
            IEnumerable<EndOfDayReferenceCode> listToReturn = Response.FindAll(QueryParameters);
            QueryParameters.Reset();
            return listToReturn;
        }

        public override EndOfDayReferenceCodesEndPoint Where(Expression<Func<EndOfDayReferenceCode, Boolean>> expression)
        {
            QueryParameters.Where.AddRange(expression.ToEposQueries());
            return this;
        }

        public override EndOfDayReferenceCodesEndPoint And(Expression<Func<EndOfDayReferenceCode, Boolean>> expression)
        {
            return Where(expression);
        }

        public override EndOfDayReferenceCodesEndPoint OrderBy<T>(Expression<Func<EndOfDayReferenceCode, T>> expression)
        {
            QueryParameters.Order = expression.ToEposOrderByQuery();
            return this;
        }

        public override EndOfDayReferenceCodesEndPoint OrderByDesc<T>(Expression<Func<EndOfDayReferenceCode, T>> expression)
        {
            QueryParameters.Order = expression.ToEposOrderByQuery(true);
            return this;
        }

        public override EndOfDayReferenceCodesEndPoint Page(Int32 pageNo)
        {
            QueryParameters.PageNo = pageNo;
            return this;
        }
    }
}
