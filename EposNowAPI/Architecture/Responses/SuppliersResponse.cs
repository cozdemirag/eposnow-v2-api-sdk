using EposNowAPI.Architecture.BaseClasses;
using EposNowAPI.Core.Tools.QueryTools;
using EposNowAPI.Models;
using System;
using System.Collections.Generic;

namespace EposNowAPI.Core.Responses
{
    public sealed class SuppliersResponse : ResponseBase<Supplier>
    {
        public SuppliersResponse() : base() { }

        public SuppliersResponse(Authenticator auth)
            : base(auth)
        {
        }

        public Supplier Create(Supplier itemToCreate)
        {
            return base.Create<SuppliersResponse>(itemToCreate);
        }

        public Supplier Update(Supplier itemToUpdate)
        {
            return base.Update<SuppliersResponse>(itemToUpdate);
        }

        public Boolean Delete(Supplier itemToDelete)
        {
            return base.Delete<SuppliersResponse>(itemToDelete);
        }

        public Boolean Delete(Int32 id)
        {
            return base.Delete<SuppliersResponse>(id);
        }

        public Supplier Find(Int32 id)
        {
            return base.Find<SuppliersResponse>(id);
        }

        public IEnumerable<Supplier> Find(QueryParameters<Supplier> parameters)
        {
            return base.Find<SuppliersResponse>(parameters);
        }

        public IEnumerable<Supplier> FindAll(QueryParameters<Supplier> parameters)
        {
            return base.Find<SuppliersResponse>(parameters);
        }
    }
}
