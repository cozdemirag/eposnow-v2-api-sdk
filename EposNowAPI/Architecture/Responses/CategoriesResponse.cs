using EposNowAPI.Architecture.BaseClasses;
using EposNowAPI.Core.Tools.QueryTools;
using EposNowAPI.Models;
using System;
using System.Collections.Generic;

namespace EposNowAPI.Core.Responses
{
    public sealed class CategoriesResponse : ResponseBase<Category>
    {
        public CategoriesResponse() : base() { }

        public CategoriesResponse(Authenticator auth)
            : base(auth)
        {
        }

        public Category Create(Category itemToCreate)
        {
            return base.Create<CategoriesResponse>(itemToCreate);
        }

        public Category Update(Category itemToUpdate)
        {
            return base.Update<CategoriesResponse>(itemToUpdate);
        }

        public Boolean Delete(Category itemToDelete)
        {
            return base.Delete<CategoriesResponse>(itemToDelete);
        }

        public Boolean Delete(Int32 id)
        {
            return base.Delete<CategoriesResponse>(id);
        }

        public Category Find(Int32 id)
        {
            return base.Find<CategoriesResponse>(id);
        }

        public IEnumerable<Category> Find(QueryParameters<Category> parameters)
        {
            return base.Find<CategoriesResponse>(parameters);
        }

        public IEnumerable<Category> FindAll(QueryParameters<Category> parameters)
        {
            return base.Find<CategoriesResponse>(parameters);
        }
    }
}
