using EposNowAPI.Models;
using System;
using System.Collections.Generic;
using EposNowAPI.Architecture.Interfaces;

namespace EposNowAPI.Core.Responses
{
    public class ProductCompositionsResponse : EposNowResponse<ProductComposition>, IEndPoint<ProductComposition>
    {
        public ProductCompositionsResponse() : base() { }

        public ProductCompositionsResponse(Authenticator auth)
            : base(auth)
        {

        }

        public new ProductComposition Create(ProductComposition itemToCreate)
        {
            return base.Create<ProductCompositionsResponse>(itemToCreate);
        }

        public new ProductComposition Update(ProductComposition itemToUpdate)
        {
            return base.Update<ProductCompositionsResponse>(itemToUpdate);
        }

        public bool Delete(ProductComposition item)
        {
            return base.Delete<ProductCompositionsResponse>(item);
        }

        public new IEnumerable<ProductComposition> Get()
        {
            return base.Get<ProductCompositionsResponse>();
        }

        public new IEnumerable<ProductComposition> Get(Int32 id)
        {
            return base.Get<ProductCompositionsResponse>(id);
        }

        public IEnumerable<ProductComposition> Get(Int32 page, String where, String order)
        {
            return base.Get<ProductCompositionsResponse>(page, where, order);
        }

        public IEnumerable<ProductComposition> Get<TKey1, TKey2>(System.Linq.Expressions.Expression<Func<ProductComposition, TKey1>> whereClause = null, System.Linq.Expressions.Expression<Func<ProductComposition, TKey2>> orderByClause = null)
        {
            return base.Get<ProductCompositionsResponse, TKey1, TKey2>(whereClause, orderByClause);
        }
    }
}
