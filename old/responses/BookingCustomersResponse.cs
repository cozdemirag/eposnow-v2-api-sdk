using EposNowAPI.Models;
using System;
using System.Collections.Generic;
using EposNowAPI.Architecture.Interfaces;

namespace EposNowAPI.Core.Responses
{
    public class BookingCustomersResponse : EposNowResponse<BookingCustomer>, IEndPoint<BookingCustomer>
    {
        public BookingCustomersResponse() : base() { }

        public BookingCustomersResponse(Authenticator auth)
            : base(auth)
        {

        }

        public new BookingCustomer Create(BookingCustomer itemToCreate)
        {
            return base.Create<BookingCustomersResponse>(itemToCreate);
        }

        public new BookingCustomer Update(BookingCustomer itemToUpdate)
        {
            return base.Update<BookingCustomersResponse>(itemToUpdate);
        }

        public bool Delete(BookingCustomer item)
        {
            return base.Delete<BookingCustomersResponse>(item);
        }

        public new IEnumerable<BookingCustomer> Get()
        {
            return base.Get<BookingCustomersResponse>();
        }

        public new IEnumerable<BookingCustomer> Get(Int32 id)
        {
            return base.Get<BookingCustomersResponse>(id);
        }

        public IEnumerable<BookingCustomer> Get(Int32 page, String where, String order)
        {
            return base.Get<BookingCustomersResponse>(page, where, order);
        }

        public IEnumerable<BookingCustomer> Get<TKey1, TKey2>(System.Linq.Expressions.Expression<Func<BookingCustomer, TKey1>> whereClause = null, System.Linq.Expressions.Expression<Func<BookingCustomer, TKey2>> orderByClause = null)
        {
            return base.Get<BookingCustomersResponse, TKey1, TKey2>(whereClause, orderByClause);
        }
    }
}
