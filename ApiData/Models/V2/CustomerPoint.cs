﻿using System;
using ApiData.Attributes;

namespace ApiData.Models
{
    [EposModel("CustomerPoints", "CustomerPoints")]
    public class CustomerPoint
    {
        public Int32 CustomerID { get; set; }
        public Int32 PointsToAdd { get; set; }
        public Int32 StaffID { get; set; }
    }
}
