﻿using System;
using EposNowAPI.Architecture.Attributes;

namespace EposNowAPI.Models
{
    [EposModel("MultipleChoiceProductGroup", "MultipleChoiceProductGroups")]
    public class MultipleChoiceProductGroup
    {
        [EposID("MultipleChoiceProductGroupID")]
        public Int32 MultipleChoiceProductGroupID { get; set; }
        public String Name { get; set; }
        public String Description { get; set; }
    }
}
