﻿using EposNowAPI.Architecture.Attributes;
using EposNowAPI.Architecture.BaseClasses;
using EposNowAPI.Models.Constants;
using System;

namespace EposNowAPI.Models
{
    [EposModel("PurchaseOrder", "PurchaseOrders")]
    public class PurchaseOrder : ModelBase
    {
        [EposID("PurchaseOrderID")]
        public Int32 PurchaseOrderID { get; set; }
        public Int32 OrderRef { get; set; }
        public DateTime DateOrdered { get; set; }
        public String Note { get; set; }
        public Int32? SupplierID { get; set; }
        public Int32 LocationID { get; set; }
        public PurchaseOrderStatus Status { get; set; }
        public DateTime? DateCompleted { get; set; }
        public DateTime? ExpectedDeliveryDate { get; set; }
    }
}
