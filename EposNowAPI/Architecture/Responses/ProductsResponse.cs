using EposNowAPI.Architecture.BaseClasses;
using EposNowAPI.Core.Tools.QueryTools;
using EposNowAPI.Models;
using System;
using System.Collections.Generic;

namespace EposNowAPI.Core.Responses
{
    public sealed class ProductsResponse : ResponseBase<Product>
    {
        public ProductsResponse() : base() { }

        public ProductsResponse(Authenticator auth)
            : base(auth)
        {
        }

        public Product Create(Product itemToCreate)
        {
            return base.Create<ProductsResponse>(itemToCreate);
        }

        public Product Update(Product itemToUpdate)
        {
            return base.Update<ProductsResponse>(itemToUpdate);
        }

        public Boolean Delete(Product itemToDelete)
        {
            return base.Delete<ProductsResponse>(itemToDelete);
        }

        public Boolean Delete(Int32 id)
        {
            return base.Delete<ProductsResponse>(id);
        }

        public Product Find(Int32 id)
        {
            return base.Find<ProductsResponse>(id);
        }

        public IEnumerable<Product> Find(QueryParameters<Product> parameters)
        {
            return base.Find<ProductsResponse>(parameters);
        }

        public IEnumerable<Product> FindAll(QueryParameters<Product> parameters)
        {
            return base.Find<ProductsResponse>(parameters);
        }
    }
}
