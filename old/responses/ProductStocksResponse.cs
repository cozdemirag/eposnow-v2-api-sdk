using EposNowAPI.Models;
using System;
using System.Collections.Generic;
using EposNowAPI.Architecture.Interfaces;

namespace EposNowAPI.Core.Responses
{
    public class ProductStocksResponse : EposNowResponse<ProductStock>, IEndPoint<ProductStock>
    {
        public ProductStocksResponse() : base() { }

        public ProductStocksResponse(Authenticator auth)
            : base(auth)
        {

        }

        public new ProductStock Create(ProductStock itemToCreate)
        {
            return base.Create<ProductStocksResponse>(itemToCreate);
        }

        public new ProductStock Update(ProductStock itemToUpdate)
        {
            return base.Update<ProductStocksResponse>(itemToUpdate);
        }

        public bool Delete(ProductStock item)
        {
            return base.Delete<ProductStocksResponse>(item);
        }

        public new IEnumerable<ProductStock> Get()
        {
            return base.Get<ProductStocksResponse>();
        }

        public new IEnumerable<ProductStock> Get(Int32 id)
        {
            return base.Get<ProductStocksResponse>(id);
        }

        public IEnumerable<ProductStock> Get(Int32 page, String where, String order)
        {
            return base.Get<ProductStocksResponse>(page, where, order);
        }

        public IEnumerable<ProductStock> Get<TKey1, TKey2>(System.Linq.Expressions.Expression<Func<ProductStock, TKey1>> whereClause = null, System.Linq.Expressions.Expression<Func<ProductStock, TKey2>> orderByClause = null)
        {
            return base.Get<ProductStocksResponse, TKey1, TKey2>(whereClause, orderByClause);
        }
    }
}
