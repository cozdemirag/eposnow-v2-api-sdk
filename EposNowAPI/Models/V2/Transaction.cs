﻿using EposNowAPI.Architecture.Attributes;
using EposNowAPI.Architecture.BaseClasses;
using EposNowAPI.Core.Json;
using EposNowAPI.Models.Constants;
using Newtonsoft.Json;
using System;

namespace EposNowAPI.Models
{
    [EposModel("Transaction", "Transactions")]
    public class Transaction : ModelBase
    {
        [EposID("TransactionID")]
        public Int32 TransactionID { get; set; }
        public Int32? CustomerID { get; set; }
        public Int32? TableID { get; set; }
        public Int32 DeviceID { get; set; }
        public DateTime DateTime { get; set; }
        [JsonConverter(typeof(EnumConverter))]
        public PaymentStatus PaymentStatus { get; set; }
        public Decimal DiscountValue { get; set; }
        public Decimal Total { get; set; }
        public Decimal ChargeTendered { get; set; }
        public String Barcode { get; set; }
        public EatOutType EatOut { get; set; }
        public Decimal NonDiscountable { get; set; }
        public Decimal NonVAT { get; set; }
    }
}
