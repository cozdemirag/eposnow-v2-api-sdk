﻿using System;
using EposNowAPI.Architecture.Attributes;

namespace EposNowAPI.Models
{
    [EposModel("ProductComposition", "ProductCompositions")]
    public class ProductComposition
    {
        [EposID("ProductCompositionID")]
        public Int32 ProductCompositionID { get; set; }
        public Int32 PurchasedProductID { get; set; }
        public Int32 MasterProductID { get; set; }
        public Int32 Amount { get; set; }
    }
}
