﻿using EposNowAPI.Architecture.Attributes;
using EposNowAPI.Architecture.BaseClasses;
using System;

namespace EposNowAPI.Models
{
    [EposModel("TransactionItem", "TransactionItems")]
    public class TransactionItem : ModelBase
    {
        [EposID("TransactionItemID")]
        public Int32 TransactionItemID { get; set; }
        public Int32 TransactionID { get; set; }
        public Int32 ProductID { get; set; }
        public Int32 Quantity { get; set; }
        public Decimal? Price { get; set; }
        public Decimal? Discount { get; set; }
        public String Notes { get; set; }
        public Decimal VATAmount { get; set; }
        public Int32? ParentTransactionItemID { get; set; }
        public Int32? MultipleChoiceProductID { get; set; }
    }
}
