﻿using System;
using EposNowAPI.Architecture.Attributes;

namespace EposNowAPI.Models
{
    [EposModel("SeatingArea", "SeatingAreas")]
    public class SeatingArea
    {
        [EposID("SeatingAreaID")]
        public Int32 SeatingAreaID { get; set; }
        public String Name { get; set; }
        public String Description { get; set; }
        public Int32 LocationID { get; set; }
    }
}
