﻿using EposNowAPI.Architecture.Extensions;
using EposNowAPI.Core.Exceptions;
using EposNowAPI.Core.Json;
using EposNowAPI.Core.Tools.QueryTools;
using EposNowAPI.Models.Constants;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Runtime.Serialization.Json;
using System.Threading.Tasks;

namespace EposNowAPI.Architecture.BaseClasses
{
    public class ResponseBase<TModel> where TModel : ModelBase, new()
    {
        public Boolean IsSuccessful { get; set; }
        public String LastErrorMessage { get; set; }
        private HttpWebRequest request { set { HandleResponse(value); } }
        private IEnumerable<TModel> items;
        private Authenticator Authentication;
        private Type TypeInfo;
        private String OrderByString { get; set; }
        private JsonSerializerSettings JsonSettings { get; set; }

        public ResponseBase(Authenticator auth)
        {
            Authentication = auth;
            TypeInfo = typeof(TModel);
            items = new List<TModel>();
            JsonSettings = new JsonSerializerSettings()
            {
                Converters = new List<JsonConverter>() { new EnumConverter() },
                ContractResolver = new PrimaryKeyContractResolver()
            };
        }

        public ResponseBase()
        {
            items = new List<TModel>();
        }

        private void HandleResponse(HttpWebRequest request)
        {
            //try
            //{
                HttpWebResponse response = (HttpWebResponse)request.GetResponse();

                using (StreamReader reader = new StreamReader(response.GetResponseStream()))
                {
                    String json = reader.ReadToEnd();

                    if (!json.StartsWith("[") && !json.EndsWith("]"))
                    {
                        json = String.Format("[{0}]", json);
                    }

                    items = JsonConvert.DeserializeObject<IEnumerable<TModel>>(json);
                }

                response.Close();
                //IsSuccessful = true;
                //LastErrorMessage = String.Empty;
            //}
            //catch (WebException wex)
            //{
            //    //Console.WriteLine(wex.Status);
            //    IsSuccessful = false;
            //    //LastErrorMessage = new StreamReader(wex.Response.GetResponseStream()).ReadToEnd();
            //    //exception = new EposNowApiException(wex.Message + " - " + LastErrorMessage);
            //    //Console.WriteLine(LastErrorMessage);
            //    HandleException(wex);
            //}
        }

        private void HandleException(WebException wex)
        {
            HttpWebResponse response;
            try
            {
                response = (HttpWebResponse)wex.Response;
            }
            catch (System.NullReferenceException)
            {
                response = null;
            }

            if (response == null)
            {
                throw new EposNowApiException(wex.Message);
            }
            else
            {
                throw new EposNowApiException(wex.Message + " : " + new StreamReader(wex.Response.GetResponseStream()).ReadToEnd());
            }
        }

        protected virtual TModel Find<TResponse>(Int32 itemID) where TResponse : ResponseBase<TModel>, new()
        {
            return Get<TResponse>(QueryMaker<TModel>.MakeGetSingleUrl(Authentication.BaseUrl, itemID)).FirstOrDefault();
        }

        protected virtual IEnumerable<TModel> Find<TResponse>(QueryParameters<TModel> parameters) where TResponse : ResponseBase<TModel>, new()
        {
            return Get<TResponse>(QueryMaker<TModel>.MakeGetManyUrl(Authentication.BaseUrl, parameters.MakeWhereQueryString(), parameters.Order), parameters.PageNo, parameters.GetAll);
        }

        protected virtual IEnumerable<TModel> FindAsync<TResponse>(QueryParameters<TModel> parameters) where TResponse : ResponseBase<TModel>, new()
        {
            return GetAsync<TResponse>(QueryMaker<TModel>.MakeGetManyUrl(Authentication.BaseUrl, parameters.MakeWhereQueryString(), parameters.Order), parameters.PageNo, parameters.GetAll);
        }

        protected virtual TModel Create<TResponse>(TModel itemToCreate) where TResponse : ResponseBase<TModel>, new()
        {
            return Create<TResponse>(QueryMaker<TModel>.MakeCreateUrl(Authentication.BaseUrl, itemToCreate), "POST", itemToCreate);
        }

        protected virtual TModel Update<TResponse>(TModel itemToUpdate) where TResponse : ResponseBase<TModel>, new()
        {
            return Create<TResponse>(QueryMaker<TModel>.MakeUpdateUrl(Authentication.BaseUrl, itemToUpdate), "PUT", itemToUpdate);
        }

        protected virtual Boolean Delete<TResponse>(Int32 itemID) where TResponse : ResponseBase<TModel>, new()
        {
            return Delete<TResponse>(QueryMaker<TModel>.MakeDeleteUrl(Authentication.BaseUrl, itemID));
        }

        protected virtual Boolean Delete<TResponse>(TModel itemToDelete) where TResponse : ResponseBase<TModel>, new()
        {
            return Delete<TResponse>(QueryMaker<TModel>.MakeDeleteUrl(Authentication.BaseUrl, itemToDelete));
        }

        private IEnumerable<TModel> GetAsync<TResponse>(String address, Int32 pageNo, Boolean canQueryAll) where TResponse : ResponseBase<TModel>, new()
        {
            IEnumerable<TModel> currentQueryItems = new List<TModel>();
            List<TModel> itemsToReturn = new List<TModel>();

            //try
            //{
                do
                {
                    currentQueryItems = RequestAsync<TResponse>(String.Format("{0}", QueryMaker<TModel>.MakeUrlPaged(address, pageNo)), "GET", String.Empty);
                    itemsToReturn.AddRange(currentQueryItems);
                    pageNo++;
                } while (canQueryAll && currentQueryItems.Count() == EposNowAPI.Core.Tools.Constants.API_MAX_RESULT_PER_REQUEST);
            //}
            //catch (Exception ex)
            //{
            //    Console.WriteLine(ex.Message);
            //}

            return itemsToReturn;
        }

        private IEnumerable<TModel> Get<TResponse>(String address, Int32 pageNo, Boolean canQueryAll) where TResponse : ResponseBase<TModel>, new()
        {
            IEnumerable<TModel> currentQueryItems = new List<TModel>();
            List<TModel> itemsToReturn = new List<TModel>();
            Console.WriteLine(address);
            try
            {
                do
                {
                    currentQueryItems = Request<TResponse>(String.Format("{0}", QueryMaker<TModel>.MakeUrlPaged(address, pageNo)), "GET", String.Empty).items;
                    itemsToReturn.AddRange(currentQueryItems);
                    pageNo++;
                } while (canQueryAll && currentQueryItems.Count() == EposNowAPI.Core.Tools.Constants.API_MAX_RESULT_PER_REQUEST);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }

            return itemsToReturn;
        }

        private IEnumerable<TModel> Get<TResponse>(String address) where TResponse : ResponseBase<TModel>, new()
        {
            IEnumerable<TModel> items = new List<TModel>();

            try
            {
                items = Request<TResponse>(address, "GET").items;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }

            return items;
        }

        private TModel Create<TResponse>(String address, String method, TModel itemToCreate) where TResponse : ResponseBase<TModel>, new()
        {
            TModel itemReceived = default(TModel);

            //try
            //{
                String json = JsonConvert.SerializeObject(itemToCreate, Formatting.None, JsonSettings);
                TResponse response = Request<TResponse>(address, method, json);

                //if(!response.IsSuccessful)
                //{
                //    itemToCreate.SuccessfulResponse = response.IsSuccessful;
                //    itemToCreate.ResponseMessages.Add(response.LastErrorMessage);
                //    itemReceived = itemToCreate;
                //}
                //else
                //{
                itemReceived = response.items.FirstOrDefault();
                //}
            //}
            //catch (WebException ex)
            //{
            //    Console.WriteLine(ex.Message);
            //}

            return itemReceived;
        }

        private Boolean Delete<TResponse>(String address) where TResponse : ResponseBase<TModel>, new()
        {
            return Request<TResponse>(address, "DELETE").IsSuccessful;
        }

        //private async Task<IEnumerable<TModel>> RequestAsync<TResponse>(String address, String method, String data = "") where TResponse : ResponseBase<TModel>, new()
        //{
        //    IEnumerable<TModel> listToReturn = default(IEnumerable<TModel>);
        //    String json = "";
        //    using (HttpClient client = new HttpClient())
        //    {
        //        client.DefaultRequestHeaders.Add("Authorization", Authentication.AutherizationHeader);
        //        using (HttpResponseMessage response = await client.GetAsync(address))
        //        {
        //            using (HttpContent content = response.Content)
        //            {
        //                Console.WriteLine("starting for json");
        //                json = await content.ReadAsStringAsync();
        //                Console.WriteLine("after for json");
        //                Console.WriteLine(json == "");
        //                // ... Display the result.
        //                if (json != null)
        //                {
        //                    Console.WriteLine("in for json");
        //                    if (!json.StartsWith("[") && !json.EndsWith("]"))
        //                    {
        //                        json = String.Format("[{0}]", json);
        //                    }

        //                    await Task.Factory.StartNew(() => { listToReturn = JsonConvert.DeserializeObject<IEnumerable<TModel>>(json); });
        //                    //listToReturn = await JsonConvert.DeserializeObjectAsync<IEnumerable<TModel>>(json);
        //                }
        //                Console.WriteLine("finished for json");
        //            }
        //        }
        //    }

        //    return listToReturn;
        //}

        private TResponse Request<TResponse>(String address, String method, String data = "") where TResponse : ResponseBase<TModel>, new()
        {
            TResponse response = new TResponse();
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(address);
            request.ContentType = "application/json";
            request.Method = method;
            request.Headers.Add("Authorization", Authentication.AutherizationHeader);
            request.Credentials = CredentialCache.DefaultCredentials;
            ServicePointManager.ServerCertificateValidationCallback = ((ssender, certificate, chain, sslPolicyErrors) => true);

            if (!String.IsNullOrEmpty(data))
            {
                using (StreamWriter writer = new StreamWriter(request.GetRequestStream()))
                {
                    writer.Write(data);
                }
            }

            //try
            //{
                response.request = request;
            //}
            //catch (WebException ex)
            //{
            //    Console.WriteLine(ex.Message);
            //}

            return response;
        }

        private IEnumerable<TModel> RequestAsync<TResponse>(String address, String method, String data = "") where TResponse : ResponseBase<TModel>, new()
        {
            return MakeRequestAsync<TResponse>(address, method, data).Result.ConvertTo<TModel>();
        }

        private async Task<String> MakeRequestAsync<TResponse>(String address, String method, String data = "")
        {
            using (HttpClient client = new HttpClient())
            {
                client.DefaultRequestHeaders.Add("Authorization", Authentication.AutherizationHeader);
                using (HttpResponseMessage response = await client.PostAsync(address, new StringContent(data)))
                {
                    //TResponse Tresponse = new TResponse();
                    // Tresponse.items = 
                    return await response.Content.ReadAsStringAsync();


                    //.ConvertTo<TModel>()
                    //using (HttpContent content = response.Content)
                    //{
                    //    string result = await content.ReadAsStringAsync();

                    //    // ... Display the result.
                    //    if (result != null && result.Length >= 50)
                    //    {
                    //        Console.WriteLine(result);
                    //    }
                    //}
                }
            }
        }


    }
}