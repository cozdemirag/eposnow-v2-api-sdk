using EposNowAPI.Models;
using System;
using System.Collections.Generic;
using EposNowAPI.Architecture.Interfaces;

namespace EposNowAPI.Core.Responses
{
    public class StockTransferReasonsResponse : EposNowResponse<StockTransferReason>, IEndPoint<StockTransferReason>
    {
        public StockTransferReasonsResponse() : base() { }

        public StockTransferReasonsResponse(Authenticator auth)
            : base(auth)
        {

        }

        public new StockTransferReason Create(StockTransferReason itemToCreate)
        {
            return base.Create<StockTransferReasonsResponse>(itemToCreate);
        }

        public new StockTransferReason Update(StockTransferReason itemToUpdate)
        {
            return base.Update<StockTransferReasonsResponse>(itemToUpdate);
        }

        public bool Delete(StockTransferReason item)
        {
            return base.Delete<StockTransferReasonsResponse>(item);
        }

        public new IEnumerable<StockTransferReason> Get()
        {
            return base.Get<StockTransferReasonsResponse>();
        }

        public new IEnumerable<StockTransferReason> Get(Int32 id)
        {
            return base.Get<StockTransferReasonsResponse>(id);
        }

        public IEnumerable<StockTransferReason> Get(Int32 page, String where, String order)
        {
            return base.Get<StockTransferReasonsResponse>(page, where, order);
        }

        public IEnumerable<StockTransferReason> Get<TKey1, TKey2>(System.Linq.Expressions.Expression<Func<StockTransferReason, TKey1>> whereClause = null, System.Linq.Expressions.Expression<Func<StockTransferReason, TKey2>> orderByClause = null)
        {
            return base.Get<StockTransferReasonsResponse, TKey1, TKey2>(whereClause, orderByClause);
        }
    }
}
