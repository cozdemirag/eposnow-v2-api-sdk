﻿using System;
using EposNowAPI.Architecture.Attributes;

namespace EposNowAPI.Models
{
    [EposModel("CustomerType", "CustomerTypes")]
    public class CustomerType
    {
        [EposID("CustomerTypeID")]
        public Int32 CustomerTypeID { get; set; }
        public String Name { get; set; }
        public Single Discount { get; set; }
        public String Description { get; set; }
        public Int32 DefaultExpiryLength { get; set; }
        public Single DefaultMaxCredit { get; set; }
    }
}
