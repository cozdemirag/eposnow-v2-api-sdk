﻿using System;

namespace ApiData.Attributes
{
    [AttributeUsage(AttributeTargets.Property | AttributeTargets.Field, AllowMultiple = false)]
    public sealed class EposID : Attribute
    {
        public String Name { get; internal set; }

        public EposID(string name)
        {
            Name = name;
        }
    }
}
