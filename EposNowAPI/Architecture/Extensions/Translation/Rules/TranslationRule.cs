﻿using EposNowAPI.Architecture.Attributes;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Reflection;

namespace EposNow.NET.Contracts.Expressions.Translation.Rules
{
    public abstract class TranslationRule : ITranslationRule
    {
        public abstract bool IsMatch(Expression left, Expression origin = null, Expression right = null);
        public abstract Object Translate(Expression left, Expression origin = null, Expression right = null);

        protected Object EvaluateMemberValue(MemberExpression member)
        {
            switch (member.Type.Name)
            {
                case "DateTime":
                    return String.Format("{0:dd/MM/yyyy HH:mm:ss}", (Expression.Lambda<Func<DateTime>>(Expression.Convert(member, typeof(DateTime)))).Compile()());//.ToString();//.ToString("MM/dd/yyyy HH:mm:ss");
                default:
                    return (Expression.Lambda<Func<object>>(Expression.Convert(member, typeof(object)))).Compile()();
            }
        }

        protected T EvaluateMemberValue<T>(MemberExpression member)
        {
            return (Expression.Lambda<Func<T>>(Expression.Convert(member, typeof(T)))).Compile()();
        }

        protected object Evaluate(MethodCallExpression methodExpression)
        {
            var methodCallAsObject = Expression.Convert(methodExpression, typeof(object));
            var lambdaWrappedMethod = Expression.Lambda<Func<object>>(methodCallAsObject);
            var compiledLambda = lambdaWrappedMethod.Compile();

            return compiledLambda();
        }

        protected String TranslateNodeType(ExpressionType nodeType)
        {
            switch (nodeType)
            {
                case ExpressionType.GreaterThan:
                    return TranslatorConstants.BinaryOperators.GreaterThan;
                case ExpressionType.GreaterThanOrEqual:
                    return TranslatorConstants.BinaryOperators.GreaterThanOrEqual;
                case ExpressionType.LessThan:
                    return TranslatorConstants.BinaryOperators.LessThan;
                case ExpressionType.LessThanOrEqual:
                    return TranslatorConstants.BinaryOperators.LessThanOrEqual;
                case ExpressionType.Not:
                case ExpressionType.NotEqual:
                    return TranslatorConstants.BinaryOperators.NotEqual;
                case ExpressionType.Equal:
                default:
                    return TranslatorConstants.BinaryOperators.Equal;
            }
        }

        protected String TranslateIntoEposQuery(String propertyName, String operatorType, Object value)
        {
            if (value == null && (operatorType != TranslatorConstants.BinaryOperators.Equal || operatorType != TranslatorConstants.BinaryOperators.NotEqual))
            {
                return String.Format(TranslatorConstants.NullSearchQueryFormat, propertyName, (operatorType == TranslatorConstants.BinaryOperators.Equal ? TranslatorConstants.SearchOperators.IsNull : TranslatorConstants.SearchOperators.IsNotNull));
            }
            else
            {
                if (operatorType != TranslatorConstants.BinaryOperators.Equal)
                {
                    return String.Format(TranslatorConstants.SearchQueryFormat, propertyName, operatorType, value);
                }
                else
                {
                    return String.Format(TranslatorConstants.BinaryQueryFormat, propertyName, operatorType, value);
                }
            }
        }

        protected Object GetEnumValueOf(ConstantExpression enumConstant)
        {
            return ((EnumValue)(enumConstant.Type.GetField(enumConstant.Value.ToString()).GetCustomAttribute(typeof(EnumValue)))).Value;
        }
    }

    internal sealed class TranslatorConstants
    {
        internal static class SearchOperators
        {
            internal static String StartsWith = "StartsWith";
            internal static String EndsWith = "EndsWith";
            internal static String Contains = "Contains";
            internal static String IsNull = "IsNull";
            internal static String IsNotNull = "IsNotNull";
        }

        internal static class BinaryOperators
        {
            internal static String Equal = "=";
            internal static String NotEqual = "<>";
            internal static String GreaterThan = ">";
            internal static String GreaterThanOrEqual = ">=";
            internal static String LessThan = "<";
            internal static String LessThanOrEqual = "<=";
        }

        internal static class OrderByTypes
        {
            public static String OrderByAscending = "OrderBy";
            public static String OrderByDescending = "OrderByDesc";
        }

        internal static String SearchQueryFormat = "search=({0}|{1}|{2})";
        internal static String NullSearchQueryFormat = "search=({0}|{1}|)";
        internal static String BinaryQueryFormat = "{0}{1}{2}";
        internal static String ParameterSeparator = "&";
        internal static List<String> AllowedMethodNames = new List<String>() { SearchOperators.StartsWith, SearchOperators.EndsWith, SearchOperators.Contains };
    }
}
