﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EposNowAPI.Architecture.Interfaces
{
    public interface IUpdateEndPoint<TModel>
    {
        TModel Update(TModel item);
    }
}
