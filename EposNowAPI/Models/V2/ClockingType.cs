﻿using System;
using EposNowAPI.Architecture.Attributes;

namespace EposNowAPI.Models
{
    [EposModel("ClockingType", "ClockingTypes")]
    public class ClockingType
    {
        [EposID("ClockingTypeID")]
        public Int32 ClockingTypeID { get; set; }
        public String Name { get; set; }
        public Decimal PayrateMultiplier { get; set; }
    }
}
