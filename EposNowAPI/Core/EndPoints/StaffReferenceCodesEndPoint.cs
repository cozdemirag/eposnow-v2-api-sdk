using EposNowAPI.Architecture.BaseClasses;
using EposNowAPI.Architecture.Extensions;
using EposNowAPI.Architecture.Interfaces;
using EposNowAPI.Core.Responses;
using EposNowAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace EposNowAPI.Core.EndPoints
{
    public sealed class StaffReferenceCodesEndPoint : EndPointBase<StaffReferenceCodesEndPoint, StaffReferenceCode> 
    {
        private StaffReferenceCodesResponse Response { get; set; }

        public StaffReferenceCodesEndPoint(Authenticator auth) : base()
        {
            Response = new StaffReferenceCodesResponse(auth);
        }


        public override StaffReferenceCode Find(Int32 id)
        {
            return Response.Find(id);
        }

        public override IEnumerable<StaffReferenceCode> Find()
        {
            IEnumerable<StaffReferenceCode> listToReturn = Response.Find(QueryParameters);
            QueryParameters.Reset();
            return listToReturn;
        }

        public override IEnumerable<StaffReferenceCode> FindAll()
        {
			QueryParameters.GetAll = true;
            QueryParameters.PageNo = 1;
            IEnumerable<StaffReferenceCode> listToReturn = Response.FindAll(QueryParameters);
            QueryParameters.Reset();
            return listToReturn;
        }

        public override StaffReferenceCodesEndPoint Where(Expression<Func<StaffReferenceCode, Boolean>> expression)
        {
            QueryParameters.Where.AddRange(expression.ToEposQueries());
            return this;
        }

        public override StaffReferenceCodesEndPoint And(Expression<Func<StaffReferenceCode, Boolean>> expression)
        {
            return Where(expression);
        }

        public override StaffReferenceCodesEndPoint OrderBy<T>(Expression<Func<StaffReferenceCode, T>> expression)
        {
            QueryParameters.Order = expression.ToEposOrderByQuery();
            return this;
        }

        public override StaffReferenceCodesEndPoint OrderByDesc<T>(Expression<Func<StaffReferenceCode, T>> expression)
        {
            QueryParameters.Order = expression.ToEposOrderByQuery(true);
            return this;
        }

        public override StaffReferenceCodesEndPoint Page(Int32 pageNo)
        {
            QueryParameters.PageNo = pageNo;
            return this;
        }
    }
}
