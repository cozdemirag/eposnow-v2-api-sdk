using EposNowAPI.Models;
using System;
using System.Collections.Generic;
using EposNowAPI.Architecture.Interfaces;

namespace EposNowAPI.Core.Responses
{
    public class CategorysResponse : EposNowResponse<Category>, IEndPoint<Category>
    {
        public CategorysResponse() : base() { }

        public CategorysResponse(Authenticator auth)
            : base(auth)
        {

        }

        public new Category Create(Category itemToCreate)
        {
            return base.Create<CategorysResponse>(itemToCreate);
        }

        public new Category Update(Category itemToUpdate)
        {
            return base.Update<CategorysResponse>(itemToUpdate);
        }

        public bool Delete(Category item)
        {
            return base.Delete<CategorysResponse>(item);
        }

        public new IEnumerable<Category> Get()
        {
            return base.Get<CategorysResponse>();
        }

        public new IEnumerable<Category> Get(Int32 id)
        {
            return base.Get<CategorysResponse>(id);
        }

        public IEnumerable<Category> Get(Int32 page, String where, String order)
        {
            return base.Get<CategorysResponse>(page, where, order);
        }

        public IEnumerable<Category> Get<TKey1, TKey2>(System.Linq.Expressions.Expression<Func<Category, TKey1>> whereClause = null, System.Linq.Expressions.Expression<Func<Category, TKey2>> orderByClause = null)
        {
            return base.Get<CategorysResponse, TKey1, TKey2>(whereClause, orderByClause);
        }
    }
}
