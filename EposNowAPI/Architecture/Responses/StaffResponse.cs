using EposNowAPI.Architecture.BaseClasses;
using EposNowAPI.Core.Tools.QueryTools;
using EposNowAPI.Models;
using System;
using System.Collections.Generic;

namespace EposNowAPI.Core.Responses
{
    public sealed class StaffResponse : ResponseBase<Staff>
    {
        public StaffResponse() : base() { }

        public StaffResponse(Authenticator auth)
            : base(auth)
        {
        }

        public Staff Create(Staff itemToCreate)
        {
            return base.Create<StaffResponse>(itemToCreate);
        }

        public Staff Update(Staff itemToUpdate)
        {
            return base.Update<StaffResponse>(itemToUpdate);
        }

        public Boolean Delete(Staff itemToDelete)
        {
            return base.Delete<StaffResponse>(itemToDelete);
        }

        public Boolean Delete(Int32 id)
        {
            return base.Delete<StaffResponse>(id);
        }

        public Staff Find(Int32 id)
        {
            return base.Find<StaffResponse>(id);
        }

        public IEnumerable<Staff> Find(QueryParameters<Staff> parameters)
        {
            return base.Find<StaffResponse>(parameters);
        }

        public IEnumerable<Staff> FindAll(QueryParameters<Staff> parameters)
        {
            return base.Find<StaffResponse>(parameters);
        }
    }
}
