﻿using ApiData.Attributes;
using ApiData.BaseClasses;
using System;

namespace ApiData.Models
{
    [EposModel("TransactionDetails", "TransactionDetails")]
    public class TransactionDetail : ModelBase
    {
        [EposID("TransactionDetailsID")]
        public Int32 TransactionDetailsID { get; set; }
        public Int32 TransactionID { get; set; }
        public String Name { get; set; }
        public String Details { get; set; }
        public Int32 Order { get; set; }
        public Boolean Printed { get; set; }
    }
}
