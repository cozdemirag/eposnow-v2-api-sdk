using EposNowAPI.Architecture.BaseClasses;
using EposNowAPI.Core.Tools.QueryTools;
using EposNowAPI.Models;
using System;
using System.Collections.Generic;

namespace EposNowAPI.Core.Responses
{
    public sealed class LocationAreasResponse : ResponseBase<LocationArea>
    {
        public LocationAreasResponse() : base() { }

        public LocationAreasResponse(Authenticator auth)
            : base(auth)
        {
        }

        public LocationArea Create(LocationArea itemToCreate)
        {
            return base.Create<LocationAreasResponse>(itemToCreate);
        }

        public LocationArea Update(LocationArea itemToUpdate)
        {
            return base.Update<LocationAreasResponse>(itemToUpdate);
        }

        public Boolean Delete(LocationArea itemToDelete)
        {
            return base.Delete<LocationAreasResponse>(itemToDelete);
        }

        public Boolean Delete(Int32 id)
        {
            return base.Delete<LocationAreasResponse>(id);
        }

        public LocationArea Find(Int32 id)
        {
            return base.Find<LocationAreasResponse>(id);
        }

        public IEnumerable<LocationArea> Find(QueryParameters<LocationArea> parameters)
        {
            return base.Find<LocationAreasResponse>(parameters);
        }

        public IEnumerable<LocationArea> FindAll(QueryParameters<LocationArea> parameters)
        {
            return base.Find<LocationAreasResponse>(parameters);
        }
    }
}
