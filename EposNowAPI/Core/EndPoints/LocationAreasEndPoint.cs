using EposNowAPI.Architecture.BaseClasses;
using EposNowAPI.Architecture.Extensions;
using EposNowAPI.Architecture.Interfaces;
using EposNowAPI.Core.Responses;
using EposNowAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace EposNowAPI.Core.EndPoints
{
    public sealed class LocationAreasEndPoint : EndPointBase<LocationAreasEndPoint, LocationArea> 
	, IUpdateEndPoint<LocationArea>
	, IDeleteEndPoint<LocationArea>
	, ICreateEndPoint<LocationArea>
    {
        private LocationAreasResponse Response { get; set; }

        public LocationAreasEndPoint(Authenticator auth) : base()
        {
            Response = new LocationAreasResponse(auth);
        }

        public LocationArea Create(LocationArea item)
        {
            return Response.Create(item);
        }
		public LocationArea Update(LocationArea item)
        {
            return Response.Update(item);
        }
		public bool Delete(int id)
        {
            return Response.Delete(id);
        }

		public bool Delete(LocationArea item)
        {
            return Response.Delete(item);
        }
		
        public override LocationArea Find(Int32 id)
        {
            return Response.Find(id);
        }

        public override IEnumerable<LocationArea> Find()
        {
            IEnumerable<LocationArea> listToReturn = Response.Find(QueryParameters);
            QueryParameters.Reset();
            return listToReturn;
        }

        public override IEnumerable<LocationArea> FindAll()
        {
			QueryParameters.GetAll = true;
            QueryParameters.PageNo = 1;
            IEnumerable<LocationArea> listToReturn = Response.FindAll(QueryParameters);
            QueryParameters.Reset();
            return listToReturn;
        }

        public override LocationAreasEndPoint Where(Expression<Func<LocationArea, Boolean>> expression)
        {
            QueryParameters.Where.AddRange(expression.ToEposQueries());
            return this;
        }

        public override LocationAreasEndPoint And(Expression<Func<LocationArea, Boolean>> expression)
        {
            return Where(expression);
        }

        public override LocationAreasEndPoint OrderBy<T>(Expression<Func<LocationArea, T>> expression)
        {
            QueryParameters.Order = expression.ToEposOrderByQuery();
            return this;
        }

        public override LocationAreasEndPoint OrderByDesc<T>(Expression<Func<LocationArea, T>> expression)
        {
            QueryParameters.Order = expression.ToEposOrderByQuery(true);
            return this;
        }

        public override LocationAreasEndPoint Page(Int32 pageNo)
        {
            QueryParameters.PageNo = pageNo;
            return this;
        }
    }
}
