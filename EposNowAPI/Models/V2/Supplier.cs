﻿using EposNowAPI.Architecture.Attributes;
using EposNowAPI.Architecture.BaseClasses;
using System;

namespace EposNowAPI.Models
{
    [EposModel("Supplier", "Suppliers")]
    public class Supplier : ModelBase
    {
        [EposID("SupplierID")]
        public Int32 SupplierID { get; set; }
        public String SupplierName { get; set; }
        public String SupplierDescription { get; set; }
        public String SupplierType { get; set; }
        public String AddressLine1 { get; set; }
        public String AddressLine2 { get; set; }
        public String Town { get; set; }
        public String PostCode { get; set; }
        public String ContactNumber { get; set; }
        public String ContactNumber2 { get; set; }
        public String EmailAddress { get; set; }
    }
}
