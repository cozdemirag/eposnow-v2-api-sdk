using EposNowAPI.Models;
using System;
using System.Collections.Generic;
using EposNowAPI.Architecture.Interfaces;

namespace EposNowAPI.Core.Responses
{
    public class TransactionsResponse : EposNowResponse<Transaction>, IEndPoint<Transaction>
    {
        public TransactionsResponse() : base() { }

        public TransactionsResponse(Authenticator auth)
            : base(auth)
        {

        }

        public new Transaction Create(Transaction itemToCreate)
        {
            return base.Create<TransactionsResponse>(itemToCreate);
        }

        public new Transaction Update(Transaction itemToUpdate)
        {
            return base.Update<TransactionsResponse>(itemToUpdate);
        }

        public bool Delete(Transaction item)
        {
            return base.Delete<TransactionsResponse>(item);
        }

        public new IEnumerable<Transaction> Get()
        {
            return base.Get<TransactionsResponse>();
        }

        public new IEnumerable<Transaction> Get(Int32 id)
        {
            return base.Get<TransactionsResponse>(id);
        }

        public IEnumerable<Transaction> Get(Int32 page, String where, String order)
        {
            return base.Get<TransactionsResponse>(page, where, order);
        }

        public IEnumerable<Transaction> Get<TKey1, TKey2>(System.Linq.Expressions.Expression<Func<Transaction, TKey1>> whereClause = null, System.Linq.Expressions.Expression<Func<Transaction, TKey2>> orderByClause = null)
        {
            return base.Get<TransactionsResponse, TKey1, TKey2>(whereClause, orderByClause);
        }
    }
}
