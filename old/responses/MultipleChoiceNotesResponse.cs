using EposNowAPI.Models;
using System;
using System.Collections.Generic;
using EposNowAPI.Architecture.Interfaces;

namespace EposNowAPI.Core.Responses
{
    public class MultipleChoiceNotesResponse : EposNowResponse<MultipleChoiceNote>, IEndPoint<MultipleChoiceNote>
    {
        public MultipleChoiceNotesResponse() : base() { }

        public MultipleChoiceNotesResponse(Authenticator auth)
            : base(auth)
        {

        }

        public new MultipleChoiceNote Create(MultipleChoiceNote itemToCreate)
        {
            return base.Create<MultipleChoiceNotesResponse>(itemToCreate);
        }

        public new MultipleChoiceNote Update(MultipleChoiceNote itemToUpdate)
        {
            return base.Update<MultipleChoiceNotesResponse>(itemToUpdate);
        }

        public bool Delete(MultipleChoiceNote item)
        {
            return base.Delete<MultipleChoiceNotesResponse>(item);
        }

        public new IEnumerable<MultipleChoiceNote> Get()
        {
            return base.Get<MultipleChoiceNotesResponse>();
        }

        public new IEnumerable<MultipleChoiceNote> Get(Int32 id)
        {
            return base.Get<MultipleChoiceNotesResponse>(id);
        }

        public IEnumerable<MultipleChoiceNote> Get(Int32 page, String where, String order)
        {
            return base.Get<MultipleChoiceNotesResponse>(page, where, order);
        }

        public IEnumerable<MultipleChoiceNote> Get<TKey1, TKey2>(System.Linq.Expressions.Expression<Func<MultipleChoiceNote, TKey1>> whereClause = null, System.Linq.Expressions.Expression<Func<MultipleChoiceNote, TKey2>> orderByClause = null)
        {
            return base.Get<MultipleChoiceNotesResponse, TKey1, TKey2>(whereClause, orderByClause);
        }
    }
}
