using EposNowAPI.Architecture.BaseClasses;
using EposNowAPI.Architecture.Extensions;
using EposNowAPI.Architecture.Interfaces;
using EposNowAPI.Core.Responses;
using EposNowAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace EposNowAPI.Core.EndPoints
{
    public sealed class TenderTypesEndPoint : EndPointBase<TenderTypesEndPoint, TenderType> 
	, IUpdateEndPoint<TenderType>
	, IDeleteEndPoint<TenderType>
	, ICreateEndPoint<TenderType>
    {
        private TenderTypesResponse Response { get; set; }

        public TenderTypesEndPoint(Authenticator auth) : base()
        {
            Response = new TenderTypesResponse(auth);
        }

        public TenderType Create(TenderType item)
        {
            return Response.Create(item);
        }
		public TenderType Update(TenderType item)
        {
            return Response.Update(item);
        }
		public bool Delete(int id)
        {
            return Response.Delete(id);
        }

		public bool Delete(TenderType item)
        {
            return Response.Delete(item);
        }
		
        public override TenderType Find(Int32 id)
        {
            return Response.Find(id);
        }

        public override IEnumerable<TenderType> Find()
        {
            IEnumerable<TenderType> listToReturn = Response.Find(QueryParameters);
            QueryParameters.Reset();
            return listToReturn;
        }

        public override IEnumerable<TenderType> FindAll()
        {
			QueryParameters.GetAll = true;
            QueryParameters.PageNo = 1;
            IEnumerable<TenderType> listToReturn = Response.FindAll(QueryParameters);
            QueryParameters.Reset();
            return listToReturn;
        }

        public override TenderTypesEndPoint Where(Expression<Func<TenderType, Boolean>> expression)
        {
            QueryParameters.Where.AddRange(expression.ToEposQueries());
            return this;
        }

        public override TenderTypesEndPoint And(Expression<Func<TenderType, Boolean>> expression)
        {
            return Where(expression);
        }

        public override TenderTypesEndPoint OrderBy<T>(Expression<Func<TenderType, T>> expression)
        {
            QueryParameters.Order = expression.ToEposOrderByQuery();
            return this;
        }

        public override TenderTypesEndPoint OrderByDesc<T>(Expression<Func<TenderType, T>> expression)
        {
            QueryParameters.Order = expression.ToEposOrderByQuery(true);
            return this;
        }

        public override TenderTypesEndPoint Page(Int32 pageNo)
        {
            QueryParameters.PageNo = pageNo;
            return this;
        }
    }
}
