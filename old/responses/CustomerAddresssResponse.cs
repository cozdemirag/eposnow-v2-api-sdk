using EposNowAPI.Models;
using System;
using System.Collections.Generic;
using EposNowAPI.Architecture.Interfaces;

namespace EposNowAPI.Core.Responses
{
    public class CustomerAddresssResponse : EposNowResponse<CustomerAddress>, IEndPoint<CustomerAddress>
    {
        public CustomerAddresssResponse() : base() { }

        public CustomerAddresssResponse(Authenticator auth)
            : base(auth)
        {

        }

        public new CustomerAddress Create(CustomerAddress itemToCreate)
        {
            return base.Create<CustomerAddresssResponse>(itemToCreate);
        }

        public new CustomerAddress Update(CustomerAddress itemToUpdate)
        {
            return base.Update<CustomerAddresssResponse>(itemToUpdate);
        }

        public bool Delete(CustomerAddress item)
        {
            return base.Delete<CustomerAddresssResponse>(item);
        }

        public new IEnumerable<CustomerAddress> Get()
        {
            return base.Get<CustomerAddresssResponse>();
        }

        public new IEnumerable<CustomerAddress> Get(Int32 id)
        {
            return base.Get<CustomerAddresssResponse>(id);
        }

        public IEnumerable<CustomerAddress> Get(Int32 page, String where, String order)
        {
            return base.Get<CustomerAddresssResponse>(page, where, order);
        }

        public IEnumerable<CustomerAddress> Get<TKey1, TKey2>(System.Linq.Expressions.Expression<Func<CustomerAddress, TKey1>> whereClause = null, System.Linq.Expressions.Expression<Func<CustomerAddress, TKey2>> orderByClause = null)
        {
            return base.Get<CustomerAddresssResponse, TKey1, TKey2>(whereClause, orderByClause);
        }
    }
}
