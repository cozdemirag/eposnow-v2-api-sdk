﻿using AutoPoco;
using AutoPoco.Engine;
using EposNowAPI;
using EposNowAPI.Core;
using EposNowAPI.Models;
using EposNowAPI.Models.Constants;
using System;
using System.Collections.Generic;
using System.Linq;

namespace LibraryBuilder
{

    public class RandomEnumDataSource<TEnum> : IDatasource
    {
        private static readonly Random _random = new Random();
        private List<TEnum> _values;

        public RandomEnumDataSource()
        {
            _values = Enum.GetValues(typeof(TEnum)).Cast<TEnum>().ToList();
        }

        public object Next(IGenerationContext context)
        {
            return _values.ElementAt(_random.Next(_values.Count()));
        }
    }


    public class MyClass{

        public int GetNum()
    {
        return 5;
    }
    }


    class Program
    {
        static String IDString = null;


        static void Main(string[] args)
        {
            //// Perform factory set up (once for entire test run)
            //IGenerationSessionFactory factory = AutoPocoContainer.Configure(x =>
            //{
            //    x.Conventions(c =>
            //    {
            //        c.UseDefaultConventions();
            //    });
            //    x.AddFromAssemblyContainingType<Product>();
            //});

            //// Generate one of these per test (factory will be a static variable most likely)
            //IGenerationSession session = factory.CreateSession();

            //// Get a single user
            //var p = session.Single<Product>().Get();

            //// Get a collection of users
            //var products = session.List<Product>(100)
            //                   .Random(20).Impose(it => it.Description, "Test Product")
            //                   .Next(20).Impose(it => it.CostPrice, 5)
            //                   .Next(60).Impose(it => it.CategoryID, 1)
            //                   .Invoke(it => it.ButtonColourID.GetValueOrDefault())
            //                   .All()
            //                   .Get()
            //                   .ToList();


            //var products2 = session.List<Product>(10).Random(10).All().Source(it => it.ProductType, new RandomEnumDataSource<ProductType>()).Get().ToList();
            //var test1 = session.List<Product>(10).Random(10).All().Source(it => it.ProductType, new RandomEnumDataSource<ProductType>()).Get().ToList();



            //var t = products.Count(it => it.Description == "Test Product");







































            Authenticator auth = new Authenticator()
            {
                BaseUrl = "https://api.eposnowhq.com/api/v2/",
                AuthenticationToken = "V1ZPTzA0TEhKVVpFN0xVTFVGSThWVE5CSVJEMlFUWUY6WEI1V1FDS1c4TDhSNE5HQUU2TFU4MUdBMlo3TzQ4Qkw="
            };



            var num1 = new MyClass();
            IDString = Console.ReadLine();
            while (!String.IsNullOrWhiteSpace(IDString))
            {
                using (EposNowApi api = new EposNowApi(auth))
                {

                    //var allTransactions = api.Transactions.FindAll().ToList();
                    //var deletedMag = api.Transactions.Where(it => it.PaymentStatus == PaymentStatus.DeletedMagento).FindAll().ToList();
                    //var deletedTil = api.Transactions.Where(it => it.PaymentStatus == PaymentStatus.DeletedTill).FindAll().ToList();
                    //var deletedAPI = api.Transactions.Where(it => it.PaymentStatus == PaymentStatus.DeletedAPI).FindAll().ToList();
                    //var deletedAdmin = api.Transactions.Where(it => it.PaymentStatus == PaymentStatus.DeletedAdmin).FindAll().ToList();
                    //var deletedPending = api.Transactions.Where(it => it.PaymentStatus == PaymentStatus.DeletedPending).FindAll().ToList();
                    //var transDetails = api.TransactionDetails.FindAll().ToList();
                    //var customerRefCodes = api.CustomerReferenceCodes.Where(it => it.AppID != 59).FindAll().ToList();
                    //var products = api.Products.Where(it => it.ProductType == ProductType.Standard).FindAll().ToList();
                    //api.Products.Where(it => it.TaxRate.Percentage > 20).FindAll().ToList();
                    //api.Product2.Where(it => it.ProductID == 0);
                    //var products2 =  api.Product2.Where(it => it.ProductID != 2).OrderBy(it => it.CostPrice).ToList();
                    //var products = api.Products.Where(it => it.ProductID == num1.GetNum()).FindAll().ToList();
                    //var productsOR = api.Products.Where(it => it.ProductID < 0 || it.ProductID == 2);
                    var products21 = api.Products.Where(it => (it.ProductID < 0 && it.Name.Contains("sadas")) || it.ProductID == 232);
                }

                IDString = Console.ReadLine();
            }
        }


        static int GetProductID(int num)
        {
            return 100 * num;
        }

        









        static void API_START()
        {
            Authenticator auth = new Authenticator()
            {
                BaseUrl = "https://api.eposnowhq.com/api",
                AuthenticationToken = "V1ZPTzA0TEhKVVpFN0xVTFVGSThWVE5CSVJEMlFUWUY6WEI1V1FDS1c4TDhSNE5HQUU2TFU4MUdBMlo3TzQ4Qkw="
            };

            IDString = Console.ReadLine();
            while (!String.IsNullOrWhiteSpace(IDString))
            {
                using (EposNowApi api = new EposNowApi(auth))
                {
                    var allTransactions = api.Transactions.FindAll().ToList();
                    var deletedMag = api.Transactions.Where(it => it.PaymentStatus == PaymentStatus.DeletedMagento).FindAll().ToList();
                    var deletedTil = api.Transactions.Where(it => it.PaymentStatus == PaymentStatus.DeletedTill).FindAll().ToList();
                    var deletedAPI = api.Transactions.Where(it => it.PaymentStatus == PaymentStatus.DeletedAPI).FindAll().ToList();
                    var deletedAdmin = api.Transactions.Where(it => it.PaymentStatus == PaymentStatus.DeletedAdmin).FindAll().ToList();
                    var deletedPending = api.Transactions.Where(it => it.PaymentStatus == PaymentStatus.DeletedPending).FindAll().ToList();

                    var products = api.Products.Where(it => it.ProductType == ProductType.Standard).FindAll().ToList();

                }

                IDString = Console.ReadLine();
            }
        }

    }
}
