﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EposNowNET.Contracts.Entities
{
    public interface IEposToken
    {
        String User { get; set; }
        String AuthToken { get; set; }
    }
}
