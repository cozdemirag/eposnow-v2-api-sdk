﻿using ApiData.Attributes;
using System;

namespace ApiData.Models
{
    [EposModel("CustomerRating", "CustomerRatings")]
    public class CustomerRating
    {
        [EposID("CustomerRatingID")]
        public Int32 CustomerRatingID { get; set; }
        public Int32 CustomerID { get; set; }
        public Int32 TransactionID { get; set; }
        public DateTime DateTime { get; set; }
        public Int32 Ratings { get; set; }
        public String Comment { get; set; }
        public String Source { get; set; }
    }
}
