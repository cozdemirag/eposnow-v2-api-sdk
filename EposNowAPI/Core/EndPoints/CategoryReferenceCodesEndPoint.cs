using EposNowAPI.Architecture.BaseClasses;
using EposNowAPI.Architecture.Extensions;
using EposNowAPI.Architecture.Interfaces;
using EposNowAPI.Core.Responses;
using EposNowAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace EposNowAPI.Core.EndPoints
{
    public sealed class CategoryReferenceCodesEndPoint : EndPointBase<CategoryReferenceCodesEndPoint, CategoryReferenceCode> 
    {
        private CategoryReferenceCodesResponse Response { get; set; }

        public CategoryReferenceCodesEndPoint(Authenticator auth) : base()
        {
            Response = new CategoryReferenceCodesResponse(auth);
        }


        public override CategoryReferenceCode Find(Int32 id)
        {
            return Response.Find(id);
        }

        public override IEnumerable<CategoryReferenceCode> Find()
        {
            IEnumerable<CategoryReferenceCode> listToReturn = Response.Find(QueryParameters);
            QueryParameters.Reset();
            return listToReturn;
        }

        public override IEnumerable<CategoryReferenceCode> FindAll()
        {
			QueryParameters.GetAll = true;
            QueryParameters.PageNo = 1;
            IEnumerable<CategoryReferenceCode> listToReturn = Response.FindAll(QueryParameters);
            QueryParameters.Reset();
            return listToReturn;
        }

        public override CategoryReferenceCodesEndPoint Where(Expression<Func<CategoryReferenceCode, Boolean>> expression)
        {
            QueryParameters.Where.AddRange(expression.ToEposQueries());
            return this;
        }

        public override CategoryReferenceCodesEndPoint And(Expression<Func<CategoryReferenceCode, Boolean>> expression)
        {
            return Where(expression);
        }

        public override CategoryReferenceCodesEndPoint OrderBy<T>(Expression<Func<CategoryReferenceCode, T>> expression)
        {
            QueryParameters.Order = expression.ToEposOrderByQuery();
            return this;
        }

        public override CategoryReferenceCodesEndPoint OrderByDesc<T>(Expression<Func<CategoryReferenceCode, T>> expression)
        {
            QueryParameters.Order = expression.ToEposOrderByQuery(true);
            return this;
        }

        public override CategoryReferenceCodesEndPoint Page(Int32 pageNo)
        {
            QueryParameters.PageNo = pageNo;
            return this;
        }
    }
}
