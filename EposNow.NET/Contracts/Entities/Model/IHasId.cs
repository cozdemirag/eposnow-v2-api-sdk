﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EposNowNET.Contracts.Entities
{
    internal interface IHasId
    {
        Int32 ID { get; set; }
    }
}
