﻿using System;
using ApiData.Attributes;
using ApiData.BaseClasses;

namespace ApiData.Models
{
    [EposModel("Tender", "Tenders", IsCreatable = true, IsDeletable = true, IsUpdatable = true)]
    public class Tender : ModelBase
    {
        [EposID("TenderID")]
        public Int32 TenderID { get; set; }
        public Int32 TransactionID { get; set; }
        public Int32 TypeID { get; set; }
        public Decimal Amount { get; set; }
        public Decimal Change { get; set; }
    }
}
