﻿using EposNowAPI.Architecture.Attributes;
using EposNowAPI.Architecture.BaseClasses;
using System;

namespace EposNowAPI.Models.V2
{
    [EposModel("MiscProduct", "MiscProducts")]
    public class MiscProduct : ModelBase
    {
        [EposID("MiscProductID")]
        public Int32 MiscProductID { get; set; }
        public String Name { get; set; }
        public Int32? TaxRateID { get; set; }
    }
}
