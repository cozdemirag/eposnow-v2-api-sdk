using EposNowAPI.Architecture.BaseClasses;
using EposNowAPI.Core.Tools.QueryTools;
using EposNowAPI.Models;
using System;
using System.Collections.Generic;

namespace EposNowAPI.Core.Responses
{
    public sealed class SupplierReferenceCodesResponse : ResponseBase<SupplierReferenceCode>
    {
        public SupplierReferenceCodesResponse() : base() { }

        public SupplierReferenceCodesResponse(Authenticator auth)
            : base(auth)
        {
        }

        public SupplierReferenceCode Create(SupplierReferenceCode itemToCreate)
        {
            return base.Create<SupplierReferenceCodesResponse>(itemToCreate);
        }

        public SupplierReferenceCode Update(SupplierReferenceCode itemToUpdate)
        {
            return base.Update<SupplierReferenceCodesResponse>(itemToUpdate);
        }

        public Boolean Delete(SupplierReferenceCode itemToDelete)
        {
            return base.Delete<SupplierReferenceCodesResponse>(itemToDelete);
        }

        public Boolean Delete(Int32 id)
        {
            return base.Delete<SupplierReferenceCodesResponse>(id);
        }

        public SupplierReferenceCode Find(Int32 id)
        {
            return base.Find<SupplierReferenceCodesResponse>(id);
        }

        public IEnumerable<SupplierReferenceCode> Find(QueryParameters<SupplierReferenceCode> parameters)
        {
            return base.Find<SupplierReferenceCodesResponse>(parameters);
        }

        public IEnumerable<SupplierReferenceCode> FindAll(QueryParameters<SupplierReferenceCode> parameters)
        {
            return base.Find<SupplierReferenceCodesResponse>(parameters);
        }
    }
}
