using EposNowAPI.Models;
using System;
using System.Collections.Generic;
using EposNowAPI.Architecture.Interfaces;

namespace EposNowAPI.Core.Responses
{
    public class MultipleChoiceProductsResponse : EposNowResponse<MultipleChoiceProduct>, IEndPoint<MultipleChoiceProduct>
    {
        public MultipleChoiceProductsResponse() : base() { }

        public MultipleChoiceProductsResponse(Authenticator auth)
            : base(auth)
        {

        }

        public new MultipleChoiceProduct Create(MultipleChoiceProduct itemToCreate)
        {
            return base.Create<MultipleChoiceProductsResponse>(itemToCreate);
        }

        public new MultipleChoiceProduct Update(MultipleChoiceProduct itemToUpdate)
        {
            return base.Update<MultipleChoiceProductsResponse>(itemToUpdate);
        }

        public bool Delete(MultipleChoiceProduct item)
        {
            return base.Delete<MultipleChoiceProductsResponse>(item);
        }

        public new IEnumerable<MultipleChoiceProduct> Get()
        {
            return base.Get<MultipleChoiceProductsResponse>();
        }

        public new IEnumerable<MultipleChoiceProduct> Get(Int32 id)
        {
            return base.Get<MultipleChoiceProductsResponse>(id);
        }

        public IEnumerable<MultipleChoiceProduct> Get(Int32 page, String where, String order)
        {
            return base.Get<MultipleChoiceProductsResponse>(page, where, order);
        }

        public IEnumerable<MultipleChoiceProduct> Get<TKey1, TKey2>(System.Linq.Expressions.Expression<Func<MultipleChoiceProduct, TKey1>> whereClause = null, System.Linq.Expressions.Expression<Func<MultipleChoiceProduct, TKey2>> orderByClause = null)
        {
            return base.Get<MultipleChoiceProductsResponse, TKey1, TKey2>(whereClause, orderByClause);
        }
    }
}
