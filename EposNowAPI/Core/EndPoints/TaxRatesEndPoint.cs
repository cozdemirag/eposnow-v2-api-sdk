using EposNowAPI.Architecture.BaseClasses;
using EposNowAPI.Architecture.Extensions;
using EposNowAPI.Architecture.Interfaces;
using EposNowAPI.Core.Responses;
using EposNowAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace EposNowAPI.Core.EndPoints
{
    public sealed class TaxRatesEndPoint : EndPointBase<TaxRatesEndPoint, TaxRate> 
    {
        private TaxRatesResponse Response { get; set; }

        public TaxRatesEndPoint(Authenticator auth) : base()
        {
            Response = new TaxRatesResponse(auth);
        }


        public override TaxRate Find(Int32 id)
        {
            return Response.Find(id);
        }

        public override IEnumerable<TaxRate> Find()
        {
            IEnumerable<TaxRate> listToReturn = Response.Find(QueryParameters);
            QueryParameters.Reset();
            return listToReturn;
        }

        public override IEnumerable<TaxRate> FindAll()
        {
			QueryParameters.GetAll = true;
            QueryParameters.PageNo = 1;
            IEnumerable<TaxRate> listToReturn = Response.FindAll(QueryParameters);
            QueryParameters.Reset();
            return listToReturn;
        }

        public override TaxRatesEndPoint Where(Expression<Func<TaxRate, Boolean>> expression)
        {
            QueryParameters.Where.AddRange(expression.ToEposQueries());
            return this;
        }

        public override TaxRatesEndPoint And(Expression<Func<TaxRate, Boolean>> expression)
        {
            return Where(expression);
        }

        public override TaxRatesEndPoint OrderBy<T>(Expression<Func<TaxRate, T>> expression)
        {
            QueryParameters.Order = expression.ToEposOrderByQuery();
            return this;
        }

        public override TaxRatesEndPoint OrderByDesc<T>(Expression<Func<TaxRate, T>> expression)
        {
            QueryParameters.Order = expression.ToEposOrderByQuery(true);
            return this;
        }

        public override TaxRatesEndPoint Page(Int32 pageNo)
        {
            QueryParameters.PageNo = pageNo;
            return this;
        }
    }
}
