﻿using ApiData.Attributes;

namespace ApiData.Models.Constants
{
    public enum PromotionType
    {
        [EnumValue(1)]
        XForY,
        [EnumValue(2)]
        XForCurrency,
        [EnumValue(11)]
        PercentagePromotion,
        [EnumValue(21)]
        SpendOverGetPercentage,
        [EnumValue(22)]
        SpendOverGetCurrency,
        [EnumValue(51)]
        BonusPoints,
        [EnumValue(52)]
        MultiplePoints,
    }
}
