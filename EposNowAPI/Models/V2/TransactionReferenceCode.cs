﻿using EposNowAPI.Architecture.Attributes;
using EposNowAPI.Architecture.BaseClasses;
using System;

namespace EposNowAPI.Models
{
    [EposModel("TransactionReferenceCode", "TransactionReferenceCodes")]
    public class TransactionReferenceCode : ModelBase
    {
        [EposID("TransactionReferenceCodeID")]
        public Int32 TransactionReferenceCodeID { get; set; }
        public Int32 TransactionID { get; set; }
        public Int32 AppID { get; set; }
        public String ReferenceCode { get; set; }
    }
}
