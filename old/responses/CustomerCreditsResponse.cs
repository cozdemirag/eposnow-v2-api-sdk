using EposNowAPI.Models;
using System;
using System.Collections.Generic;
using EposNowAPI.Architecture.Interfaces;

namespace EposNowAPI.Core.Responses
{
    public class CustomerCreditsResponse : EposNowResponse<CustomerCredit>, IEndPoint<CustomerCredit>
    {
        public CustomerCreditsResponse() : base() { }

        public CustomerCreditsResponse(Authenticator auth)
            : base(auth)
        {

        }

        public new CustomerCredit Create(CustomerCredit itemToCreate)
        {
            return base.Create<CustomerCreditsResponse>(itemToCreate);
        }

        public new CustomerCredit Update(CustomerCredit itemToUpdate)
        {
            return base.Update<CustomerCreditsResponse>(itemToUpdate);
        }

        public bool Delete(CustomerCredit item)
        {
            return base.Delete<CustomerCreditsResponse>(item);
        }

        public new IEnumerable<CustomerCredit> Get()
        {
            return base.Get<CustomerCreditsResponse>();
        }

        public new IEnumerable<CustomerCredit> Get(Int32 id)
        {
            return base.Get<CustomerCreditsResponse>(id);
        }

        public IEnumerable<CustomerCredit> Get(Int32 page, String where, String order)
        {
            return base.Get<CustomerCreditsResponse>(page, where, order);
        }

        public IEnumerable<CustomerCredit> Get<TKey1, TKey2>(System.Linq.Expressions.Expression<Func<CustomerCredit, TKey1>> whereClause = null, System.Linq.Expressions.Expression<Func<CustomerCredit, TKey2>> orderByClause = null)
        {
            return base.Get<CustomerCreditsResponse, TKey1, TKey2>(whereClause, orderByClause);
        }
    }
}
