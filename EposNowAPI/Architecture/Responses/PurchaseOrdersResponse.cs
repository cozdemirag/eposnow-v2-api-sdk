using EposNowAPI.Architecture.BaseClasses;
using EposNowAPI.Core.Tools.QueryTools;
using EposNowAPI.Models;
using System;
using System.Collections.Generic;

namespace EposNowAPI.Core.Responses
{
    public sealed class PurchaseOrdersResponse : ResponseBase<PurchaseOrder>
    {
        public PurchaseOrdersResponse() : base() { }

        public PurchaseOrdersResponse(Authenticator auth)
            : base(auth)
        {
        }

        public PurchaseOrder Create(PurchaseOrder itemToCreate)
        {
            return base.Create<PurchaseOrdersResponse>(itemToCreate);
        }

        public PurchaseOrder Update(PurchaseOrder itemToUpdate)
        {
            return base.Update<PurchaseOrdersResponse>(itemToUpdate);
        }

        public Boolean Delete(PurchaseOrder itemToDelete)
        {
            return base.Delete<PurchaseOrdersResponse>(itemToDelete);
        }

        public Boolean Delete(Int32 id)
        {
            return base.Delete<PurchaseOrdersResponse>(id);
        }

        public PurchaseOrder Find(Int32 id)
        {
            return base.Find<PurchaseOrdersResponse>(id);
        }

        public IEnumerable<PurchaseOrder> Find(QueryParameters<PurchaseOrder> parameters)
        {
            return base.Find<PurchaseOrdersResponse>(parameters);
        }

        public IEnumerable<PurchaseOrder> FindAll(QueryParameters<PurchaseOrder> parameters)
        {
            return base.Find<PurchaseOrdersResponse>(parameters);
        }
    }
}
