﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EposNowAPI.Architecture.Attributes;

namespace EposNowAPI.Models
{
    [EposModel("BookingItem", "BookingItems")]
    public class BookingItem
    {
        [EposID("BookingItemID")]
        public Int32 BookingItemID { get; set; }
        public Int32 BookingItemTypeID { get; set; }
        public String Description { get; set; }
        public Int32 LocationID { get; set; }
        public Int32 MaxOccupancy { get; set; }
        public String Name { get; set; }
        public String Notes { get; set; }
    }
}
