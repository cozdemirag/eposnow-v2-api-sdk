﻿using System;

namespace ApiData.Attributes
{
    [AttributeUsage(AttributeTargets.Class , AllowMultiple = false)]
    public sealed class EposModel : Attribute
    {
        public String PluralName { get; internal set; }
        public String EndPointName { get; internal set; }
        public Boolean IsCreatable { get; set; }
        public Boolean IsUpdatable { get; set; }
        public Boolean IsDeletable { get; set; }
        
        public EposModel(String endpoint, String plural = "")
        {
            PluralName = plural;
            EndPointName = endpoint;
        }
    }
}
