﻿using System;
using EposNowAPI.Architecture.Attributes;
using EposNowAPI.Architecture.BaseClasses;

namespace EposNowAPI.Models
{
    [EposModel("TaxRate", "TaxRates")]
    public class TaxRate : ModelBase
    {
        [EposID("TaxRateID")]
        public Int32 TaxRateID { get; set; }
        public Decimal Percentage { get; set; }
        public String Name { get; set; }
        public String Description { get; set; }
        public String TaxCode { get; set; }
    }
}
