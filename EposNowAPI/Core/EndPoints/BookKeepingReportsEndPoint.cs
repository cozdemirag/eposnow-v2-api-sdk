using EposNowAPI.Architecture.BaseClasses;
using EposNowAPI.Architecture.Extensions;
using EposNowAPI.Architecture.Interfaces;
using EposNowAPI.Core.Responses;
using EposNowAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace EposNowAPI.Core.EndPoints
{
    public sealed class BookKeepingReportsEndPoint : EndPointBase<BookKeepingReportsEndPoint, BookKeepingReport> 
    {
        private BookKeepingReportsResponse Response { get; set; }

        public BookKeepingReportsEndPoint(Authenticator auth) : base()
        {
            Response = new BookKeepingReportsResponse(auth);
        }


        public override BookKeepingReport Find(Int32 id)
        {
            return Response.Find(id);
        }

        public override IEnumerable<BookKeepingReport> Find()
        {
            IEnumerable<BookKeepingReport> listToReturn = Response.Find(QueryParameters);
            QueryParameters.Reset();
            return listToReturn;
        }

        public override IEnumerable<BookKeepingReport> FindAll()
        {
			QueryParameters.GetAll = true;
            QueryParameters.PageNo = 1;
            IEnumerable<BookKeepingReport> listToReturn = Response.FindAll(QueryParameters);
            QueryParameters.Reset();
            return listToReturn;
        }

        public override BookKeepingReportsEndPoint Where(Expression<Func<BookKeepingReport, Boolean>> expression)
        {
            QueryParameters.Where.AddRange(expression.ToEposQueries());
            return this;
        }

        public override BookKeepingReportsEndPoint And(Expression<Func<BookKeepingReport, Boolean>> expression)
        {
            return Where(expression);
        }

        public override BookKeepingReportsEndPoint OrderBy<T>(Expression<Func<BookKeepingReport, T>> expression)
        {
            QueryParameters.Order = expression.ToEposOrderByQuery();
            return this;
        }

        public override BookKeepingReportsEndPoint OrderByDesc<T>(Expression<Func<BookKeepingReport, T>> expression)
        {
            QueryParameters.Order = expression.ToEposOrderByQuery(true);
            return this;
        }

        public override BookKeepingReportsEndPoint Page(Int32 pageNo)
        {
            QueryParameters.PageNo = pageNo;
            return this;
        }
    }
}
