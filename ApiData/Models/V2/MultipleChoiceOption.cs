﻿using System;
using ApiData.Attributes;

namespace ApiData.Models
{
    [EposModel("MultipleChoiceOption", "MultipleChoiceOptions")]
    public class MultipleChoiceOption
    {
        [EposID("MultipleChoiceOptionID")]
        public Int32 MultipleChoiceOptionID { get; set; }
        public String Message { get; set; }
        public Int32 NoteID { get; set; }
    }
}
