﻿using EposNowAPI.Architecture.Attributes;

namespace EposNowAPI.Models.Constants
{
    public enum ProductType
    {
        [EnumValue(0)]
        Standard = 0,
        [EnumValue(2)]
        Measured = 2,
        [EnumValue(3)]
        Weighted = 3,
    }
}
