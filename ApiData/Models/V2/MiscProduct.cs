﻿using ApiData.Attributes;
using System;

namespace ApiData.Models.V2
{
    [EposModel("MiscProduct", "MiscProducts")]
    public class MiscProduct
    {
        [EposID("MiscProductID")]
        public Int32 MiscProductID { get; set; }
        public String Name { get; set; }
        public Int32? TaxRateID { get; set; }
    }
}
