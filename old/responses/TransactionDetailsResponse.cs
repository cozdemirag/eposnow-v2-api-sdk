using EposNowAPI.Models;
using System;
using System.Collections.Generic;
using EposNowAPI.Architecture.Interfaces;

namespace EposNowAPI.Core.Responses
{
    public class TransactionDetailsResponse : EposNowResponse<TransactionDetail>, IEndPoint<TransactionDetail>
    {
        public TransactionDetailsResponse() : base() { }

        public TransactionDetailsResponse(Authenticator auth)
            : base(auth)
        {

        }

        public new TransactionDetail Create(TransactionDetail itemToCreate)
        {
            return base.Create<TransactionDetailsResponse>(itemToCreate);
        }

        public new TransactionDetail Update(TransactionDetail itemToUpdate)
        {
            return base.Update<TransactionDetailsResponse>(itemToUpdate);
        }

        public bool Delete(TransactionDetail item)
        {
            return base.Delete<TransactionDetailsResponse>(item);
        }

        public new IEnumerable<TransactionDetail> Get()
        {
            return base.Get<TransactionDetailsResponse>();
        }

        public new IEnumerable<TransactionDetail> Get(Int32 id)
        {
            return base.Get<TransactionDetailsResponse>(id);
        }

        public IEnumerable<TransactionDetail> Get(Int32 page, String where, String order)
        {
            return base.Get<TransactionDetailsResponse>(page, where, order);
        }

        public IEnumerable<TransactionDetail> Get<TKey1, TKey2>(System.Linq.Expressions.Expression<Func<TransactionDetail, TKey1>> whereClause = null, System.Linq.Expressions.Expression<Func<TransactionDetail, TKey2>> orderByClause = null)
        {
            return base.Get<TransactionDetailsResponse, TKey1, TKey2>(whereClause, orderByClause);
        }
    }
}
