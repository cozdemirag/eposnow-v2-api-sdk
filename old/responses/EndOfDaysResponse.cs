using EposNowAPI.Models;
using System;
using System.Collections.Generic;
using EposNowAPI.Architecture.Interfaces;

namespace EposNowAPI.Core.Responses
{
    public class EndOfDaysResponse : EposNowResponse<EndOfDay>, IEndPoint<EndOfDay>
    {
        public EndOfDaysResponse() : base() { }

        public EndOfDaysResponse(Authenticator auth)
            : base(auth)
        {

        }

        public new EndOfDay Create(EndOfDay itemToCreate)
        {
            return base.Create<EndOfDaysResponse>(itemToCreate);
        }

        public new EndOfDay Update(EndOfDay itemToUpdate)
        {
            return base.Update<EndOfDaysResponse>(itemToUpdate);
        }

        public bool Delete(EndOfDay item)
        {
            return base.Delete<EndOfDaysResponse>(item);
        }

        public new IEnumerable<EndOfDay> Get()
        {
            return base.Get<EndOfDaysResponse>();
        }

        public new IEnumerable<EndOfDay> Get(Int32 id)
        {
            return base.Get<EndOfDaysResponse>(id);
        }

        public IEnumerable<EndOfDay> Get(Int32 page, String where, String order)
        {
            return base.Get<EndOfDaysResponse>(page, where, order);
        }

        public IEnumerable<EndOfDay> Get<TKey1, TKey2>(System.Linq.Expressions.Expression<Func<EndOfDay, TKey1>> whereClause = null, System.Linq.Expressions.Expression<Func<EndOfDay, TKey2>> orderByClause = null)
        {
            return base.Get<EndOfDaysResponse, TKey1, TKey2>(whereClause, orderByClause);
        }
    }
}
