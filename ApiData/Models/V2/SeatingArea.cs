﻿using System;
using ApiData.Attributes;

namespace ApiData.Models
{
    [EposModel("SeatingArea", "SeatingAreas")]
    public class SeatingArea
    {
        [EposID("SeatingAreaID")]
        public Int32 SeatingAreaID { get; set; }
        public String Name { get; set; }
        public String Description { get; set; }
        public Int32 LocationID { get; set; }
    }
}
