﻿using EposNowAPI.Architecture.Attributes;
using EposNowAPI.Core.Tools.QueryTools.Translator;
using EposNowAPI.Models;
using EposNowAPI.Models.Constants;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;

namespace EposNowAPI.Architecture.Extensions
{
    public static class ExpressionExtensions
    {
        public static List<String> ToEposQueries<T>(this Expression<Func<T, Boolean>> exp)
        {
            return new ExpressionTreeTranslator().GetQueryStrings(exp);
        }

        public static List<String> ToEposQueries(this Expression exp)
        {
            return new ExpressionTreeTranslator().GetQueryStrings(exp);
        }

        public static String ToEposOrderByQuery<TModel, TPropertyType>(this Expression<Func<TModel, TPropertyType>> exp, Boolean orderByDescending = false)
        {
            return new ExpressionTreeTranslator().GetOrderByQueryString(exp, orderByDescending);
        }

        public static Object GetEnumValue<TEnum>(this TEnum productType)
        {
            return ((EnumValue)(productType.GetType().GetField(productType.ToString()).GetCustomAttribute(typeof(EnumValue)))).Value;
        }

       

        public static Byte GetEnumTypedValue(this ProductType productType)
        {
            Object val = ((EnumValue)(productType.GetType().GetField(productType.ToString()).GetCustomAttribute(typeof(EnumValue)))).Value;
            return Convert.ToByte(val);
        }
    }
}
