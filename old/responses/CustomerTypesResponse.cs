using EposNowAPI.Models;
using System;
using System.Collections.Generic;
using EposNowAPI.Architecture.Interfaces;

namespace EposNowAPI.Core.Responses
{
    public class CustomerTypesResponse : EposNowResponse<CustomerType>, IEndPoint<CustomerType>
    {
        public CustomerTypesResponse() : base() { }

        public CustomerTypesResponse(Authenticator auth)
            : base(auth)
        {

        }

        public new CustomerType Create(CustomerType itemToCreate)
        {
            return base.Create<CustomerTypesResponse>(itemToCreate);
        }

        public new CustomerType Update(CustomerType itemToUpdate)
        {
            return base.Update<CustomerTypesResponse>(itemToUpdate);
        }

        public bool Delete(CustomerType item)
        {
            return base.Delete<CustomerTypesResponse>(item);
        }

        public new IEnumerable<CustomerType> Get()
        {
            return base.Get<CustomerTypesResponse>();
        }

        public new IEnumerable<CustomerType> Get(Int32 id)
        {
            return base.Get<CustomerTypesResponse>(id);
        }

        public IEnumerable<CustomerType> Get(Int32 page, String where, String order)
        {
            return base.Get<CustomerTypesResponse>(page, where, order);
        }

        public IEnumerable<CustomerType> Get<TKey1, TKey2>(System.Linq.Expressions.Expression<Func<CustomerType, TKey1>> whereClause = null, System.Linq.Expressions.Expression<Func<CustomerType, TKey2>> orderByClause = null)
        {
            return base.Get<CustomerTypesResponse, TKey1, TKey2>(whereClause, orderByClause);
        }
    }
}
