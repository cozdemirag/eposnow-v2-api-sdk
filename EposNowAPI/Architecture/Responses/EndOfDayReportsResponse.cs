using EposNowAPI.Architecture.BaseClasses;
using EposNowAPI.Core.Tools.QueryTools;
using EposNowAPI.Models;
using System;
using System.Collections.Generic;

namespace EposNowAPI.Core.Responses
{
    public sealed class EndOfDayReportsResponse : ResponseBase<EndOfDayReport>
    {
        public EndOfDayReportsResponse() : base() { }

        public EndOfDayReportsResponse(Authenticator auth)
            : base(auth)
        {
        }

        public EndOfDayReport Create(EndOfDayReport itemToCreate)
        {
            return base.Create<EndOfDayReportsResponse>(itemToCreate);
        }

        public EndOfDayReport Update(EndOfDayReport itemToUpdate)
        {
            return base.Update<EndOfDayReportsResponse>(itemToUpdate);
        }

        public Boolean Delete(EndOfDayReport itemToDelete)
        {
            return base.Delete<EndOfDayReportsResponse>(itemToDelete);
        }

        public Boolean Delete(Int32 id)
        {
            return base.Delete<EndOfDayReportsResponse>(id);
        }

        public EndOfDayReport Find(Int32 id)
        {
            return base.Find<EndOfDayReportsResponse>(id);
        }

        public IEnumerable<EndOfDayReport> Find(QueryParameters<EndOfDayReport> parameters)
        {
            return base.Find<EndOfDayReportsResponse>(parameters);
        }

        public IEnumerable<EndOfDayReport> FindAll(QueryParameters<EndOfDayReport> parameters)
        {
            return base.Find<EndOfDayReportsResponse>(parameters);
        }
    }
}
