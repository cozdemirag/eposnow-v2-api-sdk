using EposNowAPI.Models;
using System;
using System.Collections.Generic;
using EposNowAPI.Architecture.Interfaces;

namespace EposNowAPI.Core.Responses
{
    public class ClockingsResponse : EposNowResponse<Clocking>, IEndPoint<Clocking>
    {
        public ClockingsResponse() : base() { }

        public ClockingsResponse(Authenticator auth)
            : base(auth)
        {

        }

        public new Clocking Create(Clocking itemToCreate)
        {
            return base.Create<ClockingsResponse>(itemToCreate);
        }

        public new Clocking Update(Clocking itemToUpdate)
        {
            return base.Update<ClockingsResponse>(itemToUpdate);
        }

        public bool Delete(Clocking item)
        {
            return base.Delete<ClockingsResponse>(item);
        }

        public new IEnumerable<Clocking> Get()
        {
            return base.Get<ClockingsResponse>();
        }

        public new IEnumerable<Clocking> Get(Int32 id)
        {
            return base.Get<ClockingsResponse>(id);
        }

        public IEnumerable<Clocking> Get(Int32 page, String where, String order)
        {
            return base.Get<ClockingsResponse>(page, where, order);
        }

        public IEnumerable<Clocking> Get<TKey1, TKey2>(System.Linq.Expressions.Expression<Func<Clocking, TKey1>> whereClause = null, System.Linq.Expressions.Expression<Func<Clocking, TKey2>> orderByClause = null)
        {
            return base.Get<ClockingsResponse, TKey1, TKey2>(whereClause, orderByClause);
        }
    }
}
