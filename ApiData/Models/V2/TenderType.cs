﻿using System;
using ApiData.Attributes;
using ApiData.BaseClasses;

namespace ApiData.Models
{
    [EposModel("TenderType", "TenderTypes", IsCreatable = true, IsDeletable = true, IsUpdatable = true)]
    public class TenderType : ModelBase
    {
        [EposID("TenderTypeID")]
        public Int32 TenderTypeID { get; set; }
        public Int32 TypeID { get; set; }
        public String Name { get; set; }
        public String Description { get; set; }
    }
}
