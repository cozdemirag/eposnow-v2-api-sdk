using EposNowAPI.Architecture.BaseClasses;
using EposNowAPI.Core.Tools.QueryTools;
using EposNowAPI.Models;
using System;
using System.Collections.Generic;

namespace EposNowAPI.Core.Responses
{
    public sealed class CategoryReferenceCodesResponse : ResponseBase<CategoryReferenceCode>
    {
        public CategoryReferenceCodesResponse() : base() { }

        public CategoryReferenceCodesResponse(Authenticator auth)
            : base(auth)
        {
        }

        public CategoryReferenceCode Create(CategoryReferenceCode itemToCreate)
        {
            return base.Create<CategoryReferenceCodesResponse>(itemToCreate);
        }

        public CategoryReferenceCode Update(CategoryReferenceCode itemToUpdate)
        {
            return base.Update<CategoryReferenceCodesResponse>(itemToUpdate);
        }

        public Boolean Delete(CategoryReferenceCode itemToDelete)
        {
            return base.Delete<CategoryReferenceCodesResponse>(itemToDelete);
        }

        public Boolean Delete(Int32 id)
        {
            return base.Delete<CategoryReferenceCodesResponse>(id);
        }

        public CategoryReferenceCode Find(Int32 id)
        {
            return base.Find<CategoryReferenceCodesResponse>(id);
        }

        public IEnumerable<CategoryReferenceCode> Find(QueryParameters<CategoryReferenceCode> parameters)
        {
            return base.Find<CategoryReferenceCodesResponse>(parameters);
        }

        public IEnumerable<CategoryReferenceCode> FindAll(QueryParameters<CategoryReferenceCode> parameters)
        {
            return base.Find<CategoryReferenceCodesResponse>(parameters);
        }
    }
}
