using EposNowAPI.Models;
using System;
using System.Collections.Generic;
using EposNowAPI.Architecture.Interfaces;

namespace EposNowAPI.Core.Responses
{
    public class CustomerPointsResponse : EposNowResponse<CustomerPoint>, IEndPoint<CustomerPoint>
    {
        public CustomerPointsResponse() : base() { }

        public CustomerPointsResponse(Authenticator auth)
            : base(auth)
        {

        }

        public new CustomerPoint Create(CustomerPoint itemToCreate)
        {
            return base.Create<CustomerPointsResponse>(itemToCreate);
        }

        public new CustomerPoint Update(CustomerPoint itemToUpdate)
        {
            return base.Update<CustomerPointsResponse>(itemToUpdate);
        }

        public bool Delete(CustomerPoint item)
        {
            return base.Delete<CustomerPointsResponse>(item);
        }

        public new IEnumerable<CustomerPoint> Get()
        {
            return base.Get<CustomerPointsResponse>();
        }

        public new IEnumerable<CustomerPoint> Get(Int32 id)
        {
            return base.Get<CustomerPointsResponse>(id);
        }

        public IEnumerable<CustomerPoint> Get(Int32 page, String where, String order)
        {
            return base.Get<CustomerPointsResponse>(page, where, order);
        }

        public IEnumerable<CustomerPoint> Get<TKey1, TKey2>(System.Linq.Expressions.Expression<Func<CustomerPoint, TKey1>> whereClause = null, System.Linq.Expressions.Expression<Func<CustomerPoint, TKey2>> orderByClause = null)
        {
            return base.Get<CustomerPointsResponse, TKey1, TKey2>(whereClause, orderByClause);
        }
    }
}
