﻿using System;
using EposNowAPI.Architecture.Attributes;
using EposNowAPI.Architecture.BaseClasses;

namespace EposNowAPI.Models
{
    [EposModel("Category","Categories")]
    public class Category : ModelBase
    {
        [EposID("CategoryID")]
        public Int32 CategoryID { get; set; }
        public String Name { get; set; }
        public String Description { get; set; }
        public Int32? ParentID { get; set; }
        public Boolean ShowOnTill { get; set; }
        public Boolean Wet { get; set; }
        public Int32? ButtonColourID { get; set; }
        public Int32? SortPosition { get; set; }
        public Int32? ReportingCategoryID { get; set; }
        public String NominalCode { get; set; }
    }
}
