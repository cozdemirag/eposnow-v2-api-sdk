using EposNowAPI.Architecture.BaseClasses;
using EposNowAPI.Architecture.Extensions;
using EposNowAPI.Architecture.Interfaces;
using EposNowAPI.Core.Responses;
using EposNowAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace EposNowAPI.Core.EndPoints
{
    public sealed class DevicesEndPoint : EndPointBase<DevicesEndPoint, Device> 
    {
        private DevicesResponse Response { get; set; }

        public DevicesEndPoint(Authenticator auth) : base()
        {
            Response = new DevicesResponse(auth);
        }


        public override Device Find(Int32 id)
        {
            return Response.Find(id);
        }

        public override IEnumerable<Device> Find()
        {
            IEnumerable<Device> listToReturn = Response.Find(QueryParameters);
            QueryParameters.Reset();
            return listToReturn;
        }

        public override IEnumerable<Device> FindAll()
        {
			QueryParameters.GetAll = true;
            QueryParameters.PageNo = 1;
            IEnumerable<Device> listToReturn = Response.FindAll(QueryParameters);
            QueryParameters.Reset();
            return listToReturn;
        }

        public override DevicesEndPoint Where(Expression<Func<Device, Boolean>> expression)
        {
            QueryParameters.Where.AddRange(expression.ToEposQueries());
            return this;
        }

        public override DevicesEndPoint And(Expression<Func<Device, Boolean>> expression)
        {
            return Where(expression);
        }

        public override DevicesEndPoint OrderBy<T>(Expression<Func<Device, T>> expression)
        {
            QueryParameters.Order = expression.ToEposOrderByQuery();
            return this;
        }

        public override DevicesEndPoint OrderByDesc<T>(Expression<Func<Device, T>> expression)
        {
            QueryParameters.Order = expression.ToEposOrderByQuery(true);
            return this;
        }

        public override DevicesEndPoint Page(Int32 pageNo)
        {
            QueryParameters.PageNo = pageNo;
            return this;
        }
    }
}
