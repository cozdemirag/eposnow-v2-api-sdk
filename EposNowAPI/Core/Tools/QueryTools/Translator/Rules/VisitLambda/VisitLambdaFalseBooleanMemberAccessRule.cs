﻿using EposNow.NET.Contracts.Expressions.Translation.Rules;
using System;
using System.Linq.Expressions;

namespace EposNow.NET.Core.Rules.VisitLambda
{
    public class VisitLambdaFalseBooleanMemberAccessRule : TranslationRule
    {
        public override bool IsMatch(Expression left, Expression origin = null, Expression right = null)
        {
            return right == null && origin == null && left.NodeType == ExpressionType.Not && IsMatchUnary(left as UnaryExpression);
        }

        private bool IsMatchUnary(UnaryExpression unary)
        {
            return unary != null && unary.Operand.NodeType == ExpressionType.MemberAccess;
        }

        public override Object Translate(Expression left, Expression origin = null, Expression right = null)
        {
            MemberExpression leftMember = ((MemberExpression)((UnaryExpression)left).Operand);
            return TranslateIntoEposQuery(leftMember.Member.Name, TranslateNodeType(ExpressionType.Equal), false);
        }
    }
}
