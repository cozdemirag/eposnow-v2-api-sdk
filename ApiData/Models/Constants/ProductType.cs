﻿using ApiData.Attributes;

namespace ApiData.Models.Constants
{
    public enum ProductType
    {
        [EnumValue(0)]
        Standard,
        [EnumValue(2)]
        Measured,
        [EnumValue(3)]
        Weighted,
    }
}
