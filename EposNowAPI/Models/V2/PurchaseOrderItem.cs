﻿using System;
using EposNowAPI.Architecture.Attributes;
using EposNowAPI.Architecture.BaseClasses;

namespace EposNowAPI.Models
{
    [EposModel("PurchaseOrderItem", "PurchaseOrderItems")]
    public class PurchaseOrderItem : ModelBase
    {
        [EposID("PurchaseOrderItemID")]
        public Int32 PurchaseOrderItemID { get; set; }
        public Int32 PurchaseOrderID { get; set; }
        public Int32 ProductID { get; set; }
        public Int32 QtyOrdered { get; set; }
        public Int32? QtyReceived { get; set; }
        public Decimal? CostPrice { get; set; }
    }
}
