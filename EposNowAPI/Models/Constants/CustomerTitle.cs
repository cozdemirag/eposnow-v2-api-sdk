﻿using EposNowAPI.Architecture.Attributes;

namespace EposNowAPI.Models.Constants
{
    public enum CustomerTitle
    {
        [EnumValue(0)]
        None,
        [EnumValue(1)]
        Mr,
        [EnumValue(2)]
        Mrs,
        [EnumValue(3)]
        Ms,
        [EnumValue(4)]
        Miss,
        [EnumValue(5)]
        Dr,
        [EnumValue(6)]
        Sir,
        [EnumValue(7)]
        Master,
        [EnumValue(8)]
        Prof,
        [EnumValue(9)]
        Rev
    }
}
