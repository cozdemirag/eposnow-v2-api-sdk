using EposNowAPI.Models;
using System;
using System.Collections.Generic;

namespace EposNowAPI.Core.Responses
{
    public class CustomerPointssResponse : EposNowResponse<CustomerPoints>, IEndPoint<CustomerPoints>
    {
        public CustomerPointssResponse() : base() { }

        public CustomerPointssResponse(Authenticator auth)
            : base(auth)
        {

        }

        public new CustomerPoints Create(CustomerPoints itemToCreate)
        {
            return base.Create<CustomerPointssResponse>(itemToCreate);
        }

        public new CustomerPoints Update(CustomerPoints itemToUpdate)
        {
            return base.Update<CustomerPointssResponse>(itemToUpdate);
        }

        public bool Delete(CustomerPoints item)
        {
            return base.Delete<CustomerPointssResponse>(item);
        }

        public new IEnumerable<CustomerPoints> Get()
        {
            return base.Get<CustomerPointssResponse>();
        }

        public new IEnumerable<CustomerPoints> Get(Int32 id)
        {
            return base.Get<CustomerPointssResponse>(id);
        }

        public IEnumerable<CustomerPoints> Get(Int32 page, String where, String order)
        {
            return base.Get<CustomerPointssResponse>(page, where, order);
        }

        public IEnumerable<CustomerPoints> Get<TKey1, TKey2>(System.Linq.Expressions.Expression<Func<CustomerPoints, TKey1>> whereClause = null, System.Linq.Expressions.Expression<Func<CustomerPoints, TKey2>> orderByClause = null)
        {
            return base.Get<CustomerPointssResponse, TKey1, TKey2>(whereClause, orderByClause);
        }
    }
}
