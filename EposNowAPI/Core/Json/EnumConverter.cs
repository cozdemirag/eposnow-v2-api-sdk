﻿using EposNowAPI.Architecture.Attributes;
using EposNowAPI.Models.Constants;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace EposNowAPI.Core.Json
{

    public class EnumConverter : JsonConverter
    {
        private Type EnumType { get; set; }

        public EnumConverter()
        {
        }

        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {
            JToken t = JToken.FromObject(((EnumValue)(EnumType.GetField(value.ToString())).GetCustomAttribute(typeof(EnumValue))).Value);

            if (t.Type != JTokenType.Object)
            {
                t.WriteTo(writer);
            }
            //else
            //{
            //    JObject o = (JObject)t;
            //    o.WriteTo(writer);
            //}
        }

        public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
        {
            var receivedValue = serializer.Deserialize(reader);
            var foundEnumObj = objectType.GetFields().Where(it => it.FieldType == objectType && it.GetCustomAttributes<EnumValue>().Any(ca => ca.Value.ToString() == receivedValue.ToString())).FirstOrDefault();

            return Enum.Parse(objectType, foundEnumObj.Name);
        }

        //public override bool CanRead
        //{
        //    get { return false; }
        //}

        public override bool CanConvert(Type objectType)
        {
            EnumType = Core.Tools.Constants.EnumTypes.Where(it => it == objectType).FirstOrDefault();
            return EnumType != null;
        }
    }
}
