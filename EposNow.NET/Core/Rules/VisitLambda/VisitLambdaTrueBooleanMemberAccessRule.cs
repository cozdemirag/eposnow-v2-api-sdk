﻿using EposNow.NET.Contracts.Expressions.Translation.Rules;
using System;
using System.Linq.Expressions;

namespace EposNow.NET.Core.Rules.VisitLambda
{
    public class VisitLambdaTrueBooleanMemberAccessRule : TranslationRule
    {
        public override bool IsMatch(Expression left, Expression origin = null, Expression right = null)
        {
            return right == null && origin == null && left.NodeType == ExpressionType.MemberAccess;
        }

        public override Object Translate(Expression left, Expression origin = null, Expression right = null)
        {
            MemberExpression leftMember = (MemberExpression)left;
            return TranslateIntoEposQuery(leftMember.Member.Name, TranslateNodeType(ExpressionType.Equal), true);
        }
    }
}
