﻿using EposNowAPI.Architecture.Attributes;
using EposNowAPI.Architecture.BaseClasses;
using System;

namespace EposNowAPI.Models
{
    [EposModel("SupplierReferenceCode", "SupplierReferenceCodes")]
    public class SupplierReferenceCode : ModelBase
    {
        [EposID("SupplierReferenceCodeID")]
        public Int32 SupplierReferenceCodeID { get; set; }
        public Int32 SupplierID { get; set; }
        public Int32 AppID { get; set; }
        public String ReferenceCode { get; set; }
    }
}
