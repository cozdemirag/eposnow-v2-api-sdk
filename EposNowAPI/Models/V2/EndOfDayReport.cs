﻿using System;
using EposNowAPI.Architecture.Attributes;
using EposNowAPI.Architecture.BaseClasses;

namespace EposNowAPI.Models
{
    [EposModel("EndOfDay", "EndOfDayReports", "Reports")]
    public class EndOfDayReport : ModelBase
    {
        public Int32 LocationAreaID { get; set; }
        public Int32 LocationID { get; set; }
        public Int32 DeviceID { get; set; }
        public DateTime FromDate { get; set; }
        public DateTime ToDate { get; set; }
        public Int32 DeviceGroupID { get; set; }
        public Int32 StaffID { get; set; }
    }
}
