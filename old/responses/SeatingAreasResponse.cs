using EposNowAPI.Models;
using System;
using System.Collections.Generic;
using EposNowAPI.Architecture.Interfaces;

namespace EposNowAPI.Core.Responses
{
    public class SeatingAreasResponse : EposNowResponse<SeatingArea>, IEndPoint<SeatingArea>
    {
        public SeatingAreasResponse() : base() { }

        public SeatingAreasResponse(Authenticator auth)
            : base(auth)
        {

        }

        public new SeatingArea Create(SeatingArea itemToCreate)
        {
            return base.Create<SeatingAreasResponse>(itemToCreate);
        }

        public new SeatingArea Update(SeatingArea itemToUpdate)
        {
            return base.Update<SeatingAreasResponse>(itemToUpdate);
        }

        public bool Delete(SeatingArea item)
        {
            return base.Delete<SeatingAreasResponse>(item);
        }

        public new IEnumerable<SeatingArea> Get()
        {
            return base.Get<SeatingAreasResponse>();
        }

        public new IEnumerable<SeatingArea> Get(Int32 id)
        {
            return base.Get<SeatingAreasResponse>(id);
        }

        public IEnumerable<SeatingArea> Get(Int32 page, String where, String order)
        {
            return base.Get<SeatingAreasResponse>(page, where, order);
        }

        public IEnumerable<SeatingArea> Get<TKey1, TKey2>(System.Linq.Expressions.Expression<Func<SeatingArea, TKey1>> whereClause = null, System.Linq.Expressions.Expression<Func<SeatingArea, TKey2>> orderByClause = null)
        {
            return base.Get<SeatingAreasResponse, TKey1, TKey2>(whereClause, orderByClause);
        }
    }
}
