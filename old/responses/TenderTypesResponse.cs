using EposNowAPI.Models;
using System;
using System.Collections.Generic;
using EposNowAPI.Architecture.Interfaces;

namespace EposNowAPI.Core.Responses
{
    public class TenderTypesResponse : EposNowResponse<TenderType>, IEndPoint<TenderType>
    {
        public TenderTypesResponse() : base() { }

        public TenderTypesResponse(Authenticator auth)
            : base(auth)
        {

        }

        public new TenderType Create(TenderType itemToCreate)
        {
            return base.Create<TenderTypesResponse>(itemToCreate);
        }

        public new TenderType Update(TenderType itemToUpdate)
        {
            return base.Update<TenderTypesResponse>(itemToUpdate);
        }

        public bool Delete(TenderType item)
        {
            return base.Delete<TenderTypesResponse>(item);
        }

        public new IEnumerable<TenderType> Get()
        {
            return base.Get<TenderTypesResponse>();
        }

        public new IEnumerable<TenderType> Get(Int32 id)
        {
            return base.Get<TenderTypesResponse>(id);
        }

        public IEnumerable<TenderType> Get(Int32 page, String where, String order)
        {
            return base.Get<TenderTypesResponse>(page, where, order);
        }

        public IEnumerable<TenderType> Get<TKey1, TKey2>(System.Linq.Expressions.Expression<Func<TenderType, TKey1>> whereClause = null, System.Linq.Expressions.Expression<Func<TenderType, TKey2>> orderByClause = null)
        {
            return base.Get<TenderTypesResponse, TKey1, TKey2>(whereClause, orderByClause);
        }
    }
}
