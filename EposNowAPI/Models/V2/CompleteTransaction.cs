﻿using EposNowAPI.Architecture.Attributes;
using EposNowAPI.Models.Constants;
using System;
using System.Collections.Generic;

namespace EposNowAPI.Models
{
    [EposModel("CompleteTransaction", "CompleteTransactions")]
    public class CompleteTransaction
    {
        public Int32 CustomerID { get; set; }
        public Int32 StaffID { get; set; }
        public Int32 TableID { get; set; }
        public DateTime DateTime { get; set; }
        public EatOutType EatOut { get; set; }
        public List<TransactionItem> TransactionItems { get; set; }
        public List<Tender> Tenders { get; set; }
        public List<BaseItem> BaseItems { get; set; }
    }
}
