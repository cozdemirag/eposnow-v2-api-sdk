using EposNowAPI.Models;
using System;
using System.Collections.Generic;
using EposNowAPI.Architecture.Interfaces;

namespace EposNowAPI.Core.Responses
{
    public class CustomersResponse : EposNowResponse<Customer>, IEndPoint<Customer>
    {
        public CustomersResponse() : base() { }

        public CustomersResponse(Authenticator auth)
            : base(auth)
        {

        }

        public new Customer Create(Customer itemToCreate)
        {
            return base.Create<CustomersResponse>(itemToCreate);
        }

        public new Customer Update(Customer itemToUpdate)
        {
            return base.Update<CustomersResponse>(itemToUpdate);
        }

        public bool Delete(Customer item)
        {
            return base.Delete<CustomersResponse>(item);
        }

        public new IEnumerable<Customer> Get()
        {
            return base.Get<CustomersResponse>();
        }

        public new IEnumerable<Customer> Get(Int32 id)
        {
            return base.Get<CustomersResponse>(id);
        }

        public IEnumerable<Customer> Get(Int32 page, String where, String order)
        {
            return base.Get<CustomersResponse>(page, where, order);
        }

        public IEnumerable<Customer> Get<TKey1, TKey2>(System.Linq.Expressions.Expression<Func<Customer, TKey1>> whereClause = null, System.Linq.Expressions.Expression<Func<Customer, TKey2>> orderByClause = null)
        {
            return base.Get<CustomersResponse, TKey1, TKey2>(whereClause, orderByClause);
        }
    }
}
