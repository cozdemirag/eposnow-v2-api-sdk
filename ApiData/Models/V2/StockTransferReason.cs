﻿using System;
using ApiData.Attributes;

namespace ApiData.Models
{
    [EposModel("StockTransferReason", "StockTransferReasons")]
    public class StockTransferReason
    {
        [EposID("StockTransferReasonID")]
        public Int32 StockTransferReasonID { get; set; }
        public String Description { get; set; }
    }
}
