using EposNowAPI.Models;
using System;
using System.Collections.Generic;
using EposNowAPI.Architecture.Interfaces;

namespace EposNowAPI.Core.Responses
{
    public class PopupNotesResponse : EposNowResponse<PopupNote>, IEndPoint<PopupNote>
    {
        public PopupNotesResponse() : base() { }

        public PopupNotesResponse(Authenticator auth)
            : base(auth)
        {

        }

        public new PopupNote Create(PopupNote itemToCreate)
        {
            return base.Create<PopupNotesResponse>(itemToCreate);
        }

        public new PopupNote Update(PopupNote itemToUpdate)
        {
            return base.Update<PopupNotesResponse>(itemToUpdate);
        }

        public bool Delete(PopupNote item)
        {
            return base.Delete<PopupNotesResponse>(item);
        }

        public new IEnumerable<PopupNote> Get()
        {
            return base.Get<PopupNotesResponse>();
        }

        public new IEnumerable<PopupNote> Get(Int32 id)
        {
            return base.Get<PopupNotesResponse>(id);
        }

        public IEnumerable<PopupNote> Get(Int32 page, String where, String order)
        {
            return base.Get<PopupNotesResponse>(page, where, order);
        }

        public IEnumerable<PopupNote> Get<TKey1, TKey2>(System.Linq.Expressions.Expression<Func<PopupNote, TKey1>> whereClause = null, System.Linq.Expressions.Expression<Func<PopupNote, TKey2>> orderByClause = null)
        {
            return base.Get<PopupNotesResponse, TKey1, TKey2>(whereClause, orderByClause);
        }
    }
}
