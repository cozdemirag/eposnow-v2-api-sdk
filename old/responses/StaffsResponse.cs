using EposNowAPI.Models;
using System;
using System.Collections.Generic;
using EposNowAPI.Architecture.Interfaces;

namespace EposNowAPI.Core.Responses
{
    public class StaffsResponse : EposNowResponse<Staff>, IEndPoint<Staff>
    {
        public StaffsResponse() : base() { }

        public StaffsResponse(Authenticator auth)
            : base(auth)
        {

        }

        public new Staff Create(Staff itemToCreate)
        {
            return base.Create<StaffsResponse>(itemToCreate);
        }

        public new Staff Update(Staff itemToUpdate)
        {
            return base.Update<StaffsResponse>(itemToUpdate);
        }

        public bool Delete(Staff item)
        {
            return base.Delete<StaffsResponse>(item);
        }

        public new IEnumerable<Staff> Get()
        {
            return base.Get<StaffsResponse>();
        }

        public new IEnumerable<Staff> Get(Int32 id)
        {
            return base.Get<StaffsResponse>(id);
        }

        public IEnumerable<Staff> Get(Int32 page, String where, String order)
        {
            return base.Get<StaffsResponse>(page, where, order);
        }

        public IEnumerable<Staff> Get<TKey1, TKey2>(System.Linq.Expressions.Expression<Func<Staff, TKey1>> whereClause = null, System.Linq.Expressions.Expression<Func<Staff, TKey2>> orderByClause = null)
        {
            return base.Get<StaffsResponse, TKey1, TKey2>(whereClause, orderByClause);
        }
    }
}
