using EposNowAPI.Architecture.BaseClasses;
using EposNowAPI.Architecture.Extensions;
using EposNowAPI.Architecture.Interfaces;
using EposNowAPI.Core.Responses;
using EposNowAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace EposNowAPI.Core.EndPoints
{
    public sealed class SupplierReferenceCodesEndPoint : EndPointBase<SupplierReferenceCodesEndPoint, SupplierReferenceCode> 
    {
        private SupplierReferenceCodesResponse Response { get; set; }

        public SupplierReferenceCodesEndPoint(Authenticator auth) : base()
        {
            Response = new SupplierReferenceCodesResponse(auth);
        }


        public override SupplierReferenceCode Find(Int32 id)
        {
            return Response.Find(id);
        }

        public override IEnumerable<SupplierReferenceCode> Find()
        {
            IEnumerable<SupplierReferenceCode> listToReturn = Response.Find(QueryParameters);
            QueryParameters.Reset();
            return listToReturn;
        }

        public override IEnumerable<SupplierReferenceCode> FindAll()
        {
			QueryParameters.GetAll = true;
            QueryParameters.PageNo = 1;
            IEnumerable<SupplierReferenceCode> listToReturn = Response.FindAll(QueryParameters);
            QueryParameters.Reset();
            return listToReturn;
        }

        public override SupplierReferenceCodesEndPoint Where(Expression<Func<SupplierReferenceCode, Boolean>> expression)
        {
            QueryParameters.Where.AddRange(expression.ToEposQueries());
            return this;
        }

        public override SupplierReferenceCodesEndPoint And(Expression<Func<SupplierReferenceCode, Boolean>> expression)
        {
            return Where(expression);
        }

        public override SupplierReferenceCodesEndPoint OrderBy<T>(Expression<Func<SupplierReferenceCode, T>> expression)
        {
            QueryParameters.Order = expression.ToEposOrderByQuery();
            return this;
        }

        public override SupplierReferenceCodesEndPoint OrderByDesc<T>(Expression<Func<SupplierReferenceCode, T>> expression)
        {
            QueryParameters.Order = expression.ToEposOrderByQuery(true);
            return this;
        }

        public override SupplierReferenceCodesEndPoint Page(Int32 pageNo)
        {
            QueryParameters.PageNo = pageNo;
            return this;
        }
    }
}
