﻿using System;
using ApiData.Attributes;

namespace ApiData.Models
{
    [EposModel("StockTransfer", "StockTransfers")]
    public class StockTransfer
    {
        [EposID("StockTransferID")]
        public Int32 StockTransferID { get; set; }
        public Int32 TransNo { get; set; }
        public Int32 FromLocation { get; set; }
        public Int32 ToLocation { get; set; }
        public Int32 StaffSent { get; set; }
        public Int32 StaffReceived { get; set; }
        public DateTime DataSent { get; set; }
        public DateTime DateReceived { get; set; }
        public String Status { get; set; }
        public Int32 ReasonID { get; set; }
        public String Note { get; set; }
    }
}
