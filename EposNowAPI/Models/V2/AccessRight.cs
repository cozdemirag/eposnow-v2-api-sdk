﻿using EposNowAPI.Architecture.Attributes;
using System;

namespace EposNowAPI.Models
{
    [EposModel("AccessRight","AccessRights")]
    public class AccessRight
    {
        [EposID("AccessRightID")]
        public Int32 AccessRightID { get; set; }
        public String Name { get; set; }
        public String Description { get; set; }
        public Boolean Till { get; set; }
        public Boolean TillAdmin { get; set; }
        public Boolean BackOffice { get; set; }
        public Boolean Reports { get; set; }
        public Boolean Refunds { get; set; }
        public Boolean TablePlanning { get; set; }
        public Boolean BasketDiscount { get; set; }
        public Boolean ItemDiscount { get; set; }
        public Boolean StockSend { get; set; }
        public Boolean StockReceive { get; set; }
        public Boolean StockTake { get; set; }
        public Boolean Payouts { get; set; }
        public Boolean DisableServiceCharge { get; set; }
        public Boolean NoSale { get; set; }
        public Boolean PettyCash { get; set; }
        public Boolean FloatAdjust { get; set; }
        public Boolean AdminOptions { get; set; }
        public Boolean CloseTill { get; set; }
        public Boolean VoidLine { get; set; }
        public Boolean Clear { get; set; }
        public Boolean Hold { get; set; }
        public Boolean CustomerSelect { get; set; }
        public Boolean BlindEod { get; set; }
        public Decimal SetMaxCreditLimit { get; set; }
        public Decimal ItemDiscountLimit { get; set; }
        public Decimal ItemDiscountLimitPerc { get; set; }
        public Decimal BasketDiscountLimit { get; set; }
        public Decimal BasketDiscountLimitPerc { get; set; }
        public Decimal RefundLimit { get; set; }
    }
}
