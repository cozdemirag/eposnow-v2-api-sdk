using EposNowAPI.Architecture.BaseClasses;
using EposNowAPI.Core.Tools.QueryTools;
using EposNowAPI.Models;
using System;
using System.Collections.Generic;

namespace EposNowAPI.Core.Responses
{
    public sealed class EndOfDayReferenceCodesResponse : ResponseBase<EndOfDayReferenceCode>
    {
        public EndOfDayReferenceCodesResponse() : base() { }

        public EndOfDayReferenceCodesResponse(Authenticator auth)
            : base(auth)
        {
        }

        public EndOfDayReferenceCode Create(EndOfDayReferenceCode itemToCreate)
        {
            return base.Create<EndOfDayReferenceCodesResponse>(itemToCreate);
        }

        public EndOfDayReferenceCode Update(EndOfDayReferenceCode itemToUpdate)
        {
            return base.Update<EndOfDayReferenceCodesResponse>(itemToUpdate);
        }

        public Boolean Delete(EndOfDayReferenceCode itemToDelete)
        {
            return base.Delete<EndOfDayReferenceCodesResponse>(itemToDelete);
        }

        public Boolean Delete(Int32 id)
        {
            return base.Delete<EndOfDayReferenceCodesResponse>(id);
        }

        public EndOfDayReferenceCode Find(Int32 id)
        {
            return base.Find<EndOfDayReferenceCodesResponse>(id);
        }

        public IEnumerable<EndOfDayReferenceCode> Find(QueryParameters<EndOfDayReferenceCode> parameters)
        {
            return base.Find<EndOfDayReferenceCodesResponse>(parameters);
        }

        public IEnumerable<EndOfDayReferenceCode> FindAll(QueryParameters<EndOfDayReferenceCode> parameters)
        {
            return base.Find<EndOfDayReferenceCodesResponse>(parameters);
        }
    }
}
