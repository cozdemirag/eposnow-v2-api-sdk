﻿using ApiData.Attributes;

namespace ApiData.Models.Constants
{
    public enum BaseItemType
    {
        [EnumValue(1)]
        BasketDiscount,
        [EnumValue(2)]
        CustomerDiscount,
        [EnumValue(31)]
        PromotionItem,
        [EnumValue(51)]
        ServiceCharge,
        [EnumValue(61)]
        Gratuity,
        [EnumValue(101)]
        CustomerCreditItem,
        [EnumValue(121)]
        PayOuyItem,
        [EnumValue(201)]
        TenderCustomerCredit,
        [EnumValue(202)]
        TenderCustomerPoints
    }
}
