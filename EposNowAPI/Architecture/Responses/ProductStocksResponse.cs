using EposNowAPI.Architecture.BaseClasses;
using EposNowAPI.Core.Tools.QueryTools;
using EposNowAPI.Models;
using System;
using System.Collections.Generic;

namespace EposNowAPI.Core.Responses
{
    public sealed class ProductStocksResponse : ResponseBase<ProductStock>
    {
        public ProductStocksResponse() : base() { }

        public ProductStocksResponse(Authenticator auth)
            : base(auth)
        {
        }

        public ProductStock Create(ProductStock itemToCreate)
        {
            return base.Create<ProductStocksResponse>(itemToCreate);
        }

        public ProductStock Update(ProductStock itemToUpdate)
        {
            return base.Update<ProductStocksResponse>(itemToUpdate);
        }

        public Boolean Delete(ProductStock itemToDelete)
        {
            return base.Delete<ProductStocksResponse>(itemToDelete);
        }

        public Boolean Delete(Int32 id)
        {
            return base.Delete<ProductStocksResponse>(id);
        }

        public ProductStock Find(Int32 id)
        {
            return base.Find<ProductStocksResponse>(id);
        }

        public IEnumerable<ProductStock> Find(QueryParameters<ProductStock> parameters)
        {
            return base.Find<ProductStocksResponse>(parameters);
        }

        public IEnumerable<ProductStock> FindAll(QueryParameters<ProductStock> parameters)
        {
            return base.Find<ProductStocksResponse>(parameters);
        }
    }
}
