using EposNowAPI.Architecture.Interfaces;
using EposNowAPI.Models;
using System;
using System.Collections.Generic;

namespace EposNowAPI.Core.Responses
{
    public class BaseItemsResponse : EposNowResponse<BaseItem>, IEndPoint<BaseItem>
    {
        public BaseItemsResponse() : base() { }

        public BaseItemsResponse(Authenticator auth)
            : base(auth)
        {

        }

        public new BaseItem Create(BaseItem itemToCreate)
        {
            return base.Create<BaseItemsResponse>(itemToCreate);
        }

        public new BaseItem Update(BaseItem itemToUpdate)
        {
            return base.Update<BaseItemsResponse>(itemToUpdate);
        }

        public bool Delete(BaseItem item)
        {
            return base.Delete<BaseItemsResponse>(item);
        }

        public new IEnumerable<BaseItem> Get()
        {
            return base.Get<BaseItemsResponse>();
        }

        public new IEnumerable<BaseItem> Get(Int32 id)
        {
            return base.Get<BaseItemsResponse>(id);
        }

        public IEnumerable<BaseItem> Get(Int32 page, String where, String order)
        {
            return base.Get<BaseItemsResponse>(page, where, order);
        }

        public IEnumerable<BaseItem> Get<TKey1, TKey2>(System.Linq.Expressions.Expression<Func<BaseItem, TKey1>> whereClause = null, System.Linq.Expressions.Expression<Func<BaseItem, TKey2>> orderByClause = null)
        {
            return base.Get<BaseItemsResponse, TKey1, TKey2>(whereClause, orderByClause);
        }
    }
}
