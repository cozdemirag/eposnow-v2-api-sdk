﻿using ApiData.Attributes;

namespace ApiData.Models.Constants
{
    public enum PaymentStatus
    {
        [EnumValue("Hold")]
        Hold,
        [EnumValue("Ordered")]
        Ordered,
        [EnumValue("Complete")]
        Complete,
    }
}
