using EposNowAPI.Models;
using System;
using System.Collections.Generic;
using EposNowAPI.Architecture.Interfaces;

namespace EposNowAPI.Core.Responses
{
    public class StockTransferItemsResponse : EposNowResponse<StockTransferItem>, IEndPoint<StockTransferItem>
    {
        public StockTransferItemsResponse() : base() { }

        public StockTransferItemsResponse(Authenticator auth)
            : base(auth)
        {

        }

        public new StockTransferItem Create(StockTransferItem itemToCreate)
        {
            return base.Create<StockTransferItemsResponse>(itemToCreate);
        }

        public new StockTransferItem Update(StockTransferItem itemToUpdate)
        {
            return base.Update<StockTransferItemsResponse>(itemToUpdate);
        }

        public bool Delete(StockTransferItem item)
        {
            return base.Delete<StockTransferItemsResponse>(item);
        }

        public new IEnumerable<StockTransferItem> Get()
        {
            return base.Get<StockTransferItemsResponse>();
        }

        public new IEnumerable<StockTransferItem> Get(Int32 id)
        {
            return base.Get<StockTransferItemsResponse>(id);
        }

        public IEnumerable<StockTransferItem> Get(Int32 page, String where, String order)
        {
            return base.Get<StockTransferItemsResponse>(page, where, order);
        }

        public IEnumerable<StockTransferItem> Get<TKey1, TKey2>(System.Linq.Expressions.Expression<Func<StockTransferItem, TKey1>> whereClause = null, System.Linq.Expressions.Expression<Func<StockTransferItem, TKey2>> orderByClause = null)
        {
            return base.Get<StockTransferItemsResponse, TKey1, TKey2>(whereClause, orderByClause);
        }
    }
}
