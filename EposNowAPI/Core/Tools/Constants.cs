﻿using EposNowAPI.Models.Constants;
using System;
using System.Collections.Generic;

namespace EposNowAPI.Core.Tools
{
    public class Constants
    {
        public static Int32 API_MAX_RESULT_PER_REQUEST = 200;
        public static String DefaultEndpointVersion = "v2";
        public static String BaseUrlFormat = "{0}/{1}/";
        public static String UpdateQueryFormat = "{0}{1}/{2}";
        public static String GetSingleQueryFormat = "{0}{1}/{2}";
        public static String GetManyQueryFormat = "{0}{1}?{2}";
        public static String CreateQueryFormat = "{0}{1}";
        public static String DeleteQueryFormat = "{0}{1}/{2}";
        public static readonly IReadOnlyList<Type> EnumTypes = new List<Type>() { typeof(BaseItemType), typeof(ProductType), typeof(EatOutType), typeof(PurchaseOrderStatus), typeof(PaymentStatus) };
    }
}
