﻿using EposNowAPI.Core;
using EposNowAPI.Core.Tools.QueryTools;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace EposNowAPI.Architecture.BaseClasses
{
    public abstract class EndPointBase<TEndPoint, TModel> 
    {
        protected QueryParameters<TModel> QueryParameters { get; set; }

        internal EndPointBase()
        {
            QueryParameters = new QueryParameters<TModel>();
        }

        public abstract TModel Find(Int32 id);
        public abstract IEnumerable<TModel> Find();
        public abstract IEnumerable<TModel> FindAll();
        //public abstract TModel FindAllAsync();

        public abstract TEndPoint Where(Expression<Func<TModel, Boolean>> expression);
        public abstract TEndPoint And(Expression<Func<TModel, Boolean>> expression);
        public abstract TEndPoint OrderBy<T>(Expression<Func<TModel, T>> expression);
        public abstract TEndPoint OrderByDesc<T>(Expression<Func<TModel, T>> expression);
        public abstract TEndPoint Page(Int32 pageNo);

        public void ClearParameters()
        {
            QueryParameters.Reset();
        }
    }
}
