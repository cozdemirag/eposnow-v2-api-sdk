using EposNowAPI.Architecture.BaseClasses;
using EposNowAPI.Architecture.Extensions;
using EposNowAPI.Architecture.Interfaces;
using EposNowAPI.Core.Responses;
using EposNowAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace EposNowAPI.Core.EndPoints
{
    public sealed class ProductReferenceCodesEndPoint : EndPointBase<ProductReferenceCodesEndPoint, ProductReferenceCode> 
    {
        private ProductReferenceCodesResponse Response { get; set; }

        public ProductReferenceCodesEndPoint(Authenticator auth) : base()
        {
            Response = new ProductReferenceCodesResponse(auth);
        }


        public override ProductReferenceCode Find(Int32 id)
        {
            return Response.Find(id);
        }

        public override IEnumerable<ProductReferenceCode> Find()
        {
            IEnumerable<ProductReferenceCode> listToReturn = Response.Find(QueryParameters);
            QueryParameters.Reset();
            return listToReturn;
        }

        public override IEnumerable<ProductReferenceCode> FindAll()
        {
			QueryParameters.GetAll = true;
            QueryParameters.PageNo = 1;
            IEnumerable<ProductReferenceCode> listToReturn = Response.FindAll(QueryParameters);
            QueryParameters.Reset();
            return listToReturn;
        }

        public override ProductReferenceCodesEndPoint Where(Expression<Func<ProductReferenceCode, Boolean>> expression)
        {
            QueryParameters.Where.AddRange(expression.ToEposQueries());
            return this;
        }

        public override ProductReferenceCodesEndPoint And(Expression<Func<ProductReferenceCode, Boolean>> expression)
        {
            return Where(expression);
        }

        public override ProductReferenceCodesEndPoint OrderBy<T>(Expression<Func<ProductReferenceCode, T>> expression)
        {
            QueryParameters.Order = expression.ToEposOrderByQuery();
            return this;
        }

        public override ProductReferenceCodesEndPoint OrderByDesc<T>(Expression<Func<ProductReferenceCode, T>> expression)
        {
            QueryParameters.Order = expression.ToEposOrderByQuery(true);
            return this;
        }

        public override ProductReferenceCodesEndPoint Page(Int32 pageNo)
        {
            QueryParameters.PageNo = pageNo;
            return this;
        }
    }
}
