using EposNowAPI.Models;
using System;
using System.Collections.Generic;
using EposNowAPI.Architecture.Interfaces;

namespace EposNowAPI.Core.Responses
{
    public class ClockingTypesResponse : EposNowResponse<ClockingType>, IEndPoint<ClockingType>
    {
        public ClockingTypesResponse() : base() { }

        public ClockingTypesResponse(Authenticator auth)
            : base(auth)
        {

        }

        public new ClockingType Create(ClockingType itemToCreate)
        {
            return base.Create<ClockingTypesResponse>(itemToCreate);
        }

        public new ClockingType Update(ClockingType itemToUpdate)
        {
            return base.Update<ClockingTypesResponse>(itemToUpdate);
        }

        public bool Delete(ClockingType item)
        {
            return base.Delete<ClockingTypesResponse>(item);
        }

        public new IEnumerable<ClockingType> Get()
        {
            return base.Get<ClockingTypesResponse>();
        }

        public new IEnumerable<ClockingType> Get(Int32 id)
        {
            return base.Get<ClockingTypesResponse>(id);
        }

        public IEnumerable<ClockingType> Get(Int32 page, String where, String order)
        {
            return base.Get<ClockingTypesResponse>(page, where, order);
        }

        public IEnumerable<ClockingType> Get<TKey1, TKey2>(System.Linq.Expressions.Expression<Func<ClockingType, TKey1>> whereClause = null, System.Linq.Expressions.Expression<Func<ClockingType, TKey2>> orderByClause = null)
        {
            return base.Get<ClockingTypesResponse, TKey1, TKey2>(whereClause, orderByClause);
        }
    }
}
