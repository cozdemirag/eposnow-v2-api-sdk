







using EposNowAPI.Core.EndPoints;
using EposNowAPI.Models;

namespace EposNowAPI.Core
{
    public partial class EposNowApi
    {
		public BookKeepingReportsEndPoint BookKeepingReports { get; internal set; }
		public BrandsEndPoint Brands { get; internal set; }
		public CategoriesEndPoint Categories { get; internal set; }
		public CustomersEndPoint Customers { get; internal set; }
		public DailySalesEndPoint DailySales { get; internal set; }
		public DevicesEndPoint Devices { get; internal set; }
		public EndOfDayReportsEndPoint EndOfDayReports { get; internal set; }
		public LocationsEndPoint Locations { get; internal set; }
		public LocationAreasEndPoint LocationAreas { get; internal set; }
		public ProductsEndPoint Products { get; internal set; }
		public ProductStocksEndPoint ProductStocks { get; internal set; }
		public PurchaseOrdersEndPoint PurchaseOrders { get; internal set; }
		public PurchaseOrderItemsEndPoint PurchaseOrderItems { get; internal set; }
		public StaffEndPoint Staff { get; internal set; }
		public SuppliersEndPoint Suppliers { get; internal set; }
		public TaxRatesEndPoint TaxRates { get; internal set; }
		public TendersEndPoint Tenders { get; internal set; }
		public TenderTypesEndPoint TenderTypes { get; internal set; }
		public TransactionsEndPoint Transactions { get; internal set; }
		public TransactionDetailsEndPoint TransactionDetails { get; internal set; }
		public ProductReferenceCodesEndPoint ProductReferenceCodes { get; internal set; }
		public CustomerReferenceCodesEndPoint CustomerReferenceCodes { get; internal set; }
		public SupplierReferenceCodesEndPoint SupplierReferenceCodes { get; internal set; }
		public EndOfDayReferenceCodesEndPoint EndOfDayReferenceCodes { get; internal set; }
		public StaffReferenceCodesEndPoint StaffReferenceCodes { get; internal set; }
		public CategoryReferenceCodesEndPoint CategoryReferenceCodes { get; internal set; }
		public TransactionReferenceCodesEndPoint TransactionReferenceCodes { get; internal set; }
		public TransactionItemsEndPoint TransactionItems { get; internal set; }
        public EposNowQuery<Product> Product2 { get; internal set; }
	
        public EposNowApi(Authenticator authenticator)
        {
			 BookKeepingReports = new BookKeepingReportsEndPoint(authenticator); 
			 Brands = new BrandsEndPoint(authenticator); 
			 Categories = new CategoriesEndPoint(authenticator); 
			 Customers = new CustomersEndPoint(authenticator); 
			 DailySales = new DailySalesEndPoint(authenticator); 
			 Devices = new DevicesEndPoint(authenticator); 
			 EndOfDayReports = new EndOfDayReportsEndPoint(authenticator); 
			 Locations = new LocationsEndPoint(authenticator); 
			 LocationAreas = new LocationAreasEndPoint(authenticator); 
			 Products = new ProductsEndPoint(authenticator); 
			 ProductStocks = new ProductStocksEndPoint(authenticator); 
			 PurchaseOrders = new PurchaseOrdersEndPoint(authenticator); 
			 PurchaseOrderItems = new PurchaseOrderItemsEndPoint(authenticator); 
			 Staff = new StaffEndPoint(authenticator); 
			 Suppliers = new SuppliersEndPoint(authenticator); 
			 TaxRates = new TaxRatesEndPoint(authenticator); 
			 Tenders = new TendersEndPoint(authenticator); 
			 TenderTypes = new TenderTypesEndPoint(authenticator); 
			 Transactions = new TransactionsEndPoint(authenticator); 
			 TransactionDetails = new TransactionDetailsEndPoint(authenticator); 
			 ProductReferenceCodes = new ProductReferenceCodesEndPoint(authenticator); 
			 CustomerReferenceCodes = new CustomerReferenceCodesEndPoint(authenticator); 
			 SupplierReferenceCodes = new SupplierReferenceCodesEndPoint(authenticator); 
			 EndOfDayReferenceCodes = new EndOfDayReferenceCodesEndPoint(authenticator); 
			 StaffReferenceCodes = new StaffReferenceCodesEndPoint(authenticator); 
			 CategoryReferenceCodes = new CategoryReferenceCodesEndPoint(authenticator); 
			 TransactionReferenceCodes = new TransactionReferenceCodesEndPoint(authenticator); 
			 TransactionItems = new TransactionItemsEndPoint(authenticator);
            Product2 = new EposNowQuery<Product>(authenticator);
        }
    }
}
