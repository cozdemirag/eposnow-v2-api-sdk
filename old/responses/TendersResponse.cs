using EposNowAPI.Models;
using System;
using System.Collections.Generic;
using EposNowAPI.Architecture.Interfaces;

namespace EposNowAPI.Core.Responses
{
    public class TendersResponse : EposNowResponse<Tender>, IEndPoint<Tender>
    {
        public TendersResponse() : base() { }

        public TendersResponse(Authenticator auth)
            : base(auth)
        {

        }

        public new Tender Create(Tender itemToCreate)
        {
            return base.Create<TendersResponse>(itemToCreate);
        }

        public new Tender Update(Tender itemToUpdate)
        {
            return base.Update<TendersResponse>(itemToUpdate);
        }

        public bool Delete(Tender item)
        {
            return base.Delete<TendersResponse>(item);
        }

        public new IEnumerable<Tender> Get()
        {
            return base.Get<TendersResponse>();
        }

        public new IEnumerable<Tender> Get(Int32 id)
        {
            return base.Get<TendersResponse>(id);
        }

        public IEnumerable<Tender> Get(Int32 page, String where, String order)
        {
            return base.Get<TendersResponse>(page, where, order);
        }

        public IEnumerable<Tender> Get<TKey1, TKey2>(System.Linq.Expressions.Expression<Func<Tender, TKey1>> whereClause = null, System.Linq.Expressions.Expression<Func<Tender, TKey2>> orderByClause = null)
        {
            return base.Get<TendersResponse, TKey1, TKey2>(whereClause, orderByClause);
        }
    }
}
