﻿using System;
using ApiData.Attributes;

namespace ApiData.Models
{
    [EposModel("StockTransferItem", "StockTransferItems")]
    public class StockTransferItem
    {
        [EposID("StockTransferItemID")]
        public Int32 StockTransferItemID { get; set; }
        public Int32 StockTransferID { get; set; }
        public Int32 ProductID { get; set; }
        public Int32 QtySent { get; set; }
        public Int32 ItemReasonID { get; set; }
        public Int32 QtyNewFrom { get; set; }
        public Int32 QtyNewTo { get; set; }
    }
}
