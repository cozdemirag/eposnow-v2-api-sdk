using EposNowAPI.Architecture.BaseClasses;
using EposNowAPI.Core.Tools.QueryTools;
using EposNowAPI.Models;
using System;
using System.Collections.Generic;

namespace EposNowAPI.Core.Responses
{
    public sealed class TransactionsResponse : ResponseBase<Transaction>
    {
        public TransactionsResponse() : base() { }

        public TransactionsResponse(Authenticator auth)
            : base(auth)
        {
        }

        public Transaction Create(Transaction itemToCreate)
        {
            return base.Create<TransactionsResponse>(itemToCreate);
        }

        public Transaction Update(Transaction itemToUpdate)
        {
            return base.Update<TransactionsResponse>(itemToUpdate);
        }

        public Boolean Delete(Transaction itemToDelete)
        {
            return base.Delete<TransactionsResponse>(itemToDelete);
        }

        public Boolean Delete(Int32 id)
        {
            return base.Delete<TransactionsResponse>(id);
        }

        public Transaction Find(Int32 id)
        {
            return base.Find<TransactionsResponse>(id);
        }

        public IEnumerable<Transaction> Find(QueryParameters<Transaction> parameters)
        {
            return base.Find<TransactionsResponse>(parameters);
        }

        public IEnumerable<Transaction> FindAll(QueryParameters<Transaction> parameters)
        {
            return base.Find<TransactionsResponse>(parameters);
        }
    }
}
