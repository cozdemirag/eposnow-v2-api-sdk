﻿using System;
using EposNowAPI.Architecture.Attributes;
using EposNowAPI.Architecture.BaseClasses;

namespace EposNowAPI.Models
{
    [EposModel("Device", "Devices")]
    public class Device : ModelBase
    {
        [EposID("DeviceID")]
        public Int32 DeviceID { get; set; }
        public String Name { get; set; }
        public String Description { get; set; }
        public Int32 LocationID { get; set; }
        public Boolean Enabled { get; set; }
    }
}
