using EposNowAPI.Architecture.BaseClasses;
using EposNowAPI.Core.Tools.QueryTools;
using EposNowAPI.Models;
using System;
using System.Collections.Generic;

namespace EposNowAPI.Core.Responses
{
    public sealed class DailySalesResponse : ResponseBase<DailySalesReport>
    {
        public DailySalesResponse() : base() { }

        public DailySalesResponse(Authenticator auth)
            : base(auth)
        {
        }

        public DailySalesReport Create(DailySalesReport itemToCreate)
        {
            return base.Create<DailySalesResponse>(itemToCreate);
        }

        public DailySalesReport Update(DailySalesReport itemToUpdate)
        {
            return base.Update<DailySalesResponse>(itemToUpdate);
        }

        public Boolean Delete(DailySalesReport itemToDelete)
        {
            return base.Delete<DailySalesResponse>(itemToDelete);
        }

        public Boolean Delete(Int32 id)
        {
            return base.Delete<DailySalesResponse>(id);
        }

        public DailySalesReport Find(Int32 id)
        {
            return base.Find<DailySalesResponse>(id);
        }

        public IEnumerable<DailySalesReport> Find(QueryParameters<DailySalesReport> parameters)
        {
            return base.Find<DailySalesResponse>(parameters);
        }

        public IEnumerable<DailySalesReport> FindAll(QueryParameters<DailySalesReport> parameters)
        {
            return base.Find<DailySalesResponse>(parameters);
        }
    }
}
