﻿using System;
using EposNowAPI.Architecture.Attributes;

namespace EposNowAPI.Models
{
    [EposModel("StockTransferReason", "StockTransferReasons")]
    public class StockTransferReason
    {
        [EposID("StockTransferReasonID")]
        public Int32 StockTransferReasonID { get; set; }
        public String Description { get; set; }
    }
}
