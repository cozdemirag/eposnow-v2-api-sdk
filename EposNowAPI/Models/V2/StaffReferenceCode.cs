﻿using EposNowAPI.Architecture.Attributes;
using EposNowAPI.Architecture.BaseClasses;
using System;

namespace EposNowAPI.Models
{
    [EposModel("StaffReferenceCode", "StaffReferenceCodes")]
    public class StaffReferenceCode : ModelBase
    {
        [EposID("StaffReferenceCodeID")]
        public Int32 StaffReferenceCodeID { get; set; }
        public Int32 StaffID { get; set; }
        public Int32 AppID { get; set; }
        public String ReferenceCode { get; set; }
    }
}
