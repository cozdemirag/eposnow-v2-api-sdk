﻿using ApiData.Attributes;
using ApiData.BaseClasses;
using ApiData.Models.Constants;
using System;

namespace ApiData.Models
{
    [EposModel("Customer", "Customers", IsCreatable = true, IsDeletable = true, IsUpdatable = true)]
    public class Customer : ModelBase
    {
        [EposID("CustomerID")]
        public Int32 CustomerID { get; set; }
        public CustomerTitle Title { get; set; }
        public String Forename { get; set; }
        public String Surname { get; set; }
        public String BusinessName { get; set; }
        public DateTime? DateOfBirth { get; set; }
        public Int32? MainAddressID { get; set; }
        public String ContactNumber { get; set; }
        public String ContactNumber2 { get; set; }
        public String EmailAddress { get; set; }
        public Int32? Type { get; set; }
        public Double MaxCredit { get; set; }
        public Double CurrentBalance { get; set; }
        public DateTime? ExpiryDate { get; set; }
        public String CardNumber { get; set; }
        public Int32 CurrentPoints { get; set; }
        public DateTime SignUpDate { get; set; }
        public String Notes { get; set; }
    }
}
