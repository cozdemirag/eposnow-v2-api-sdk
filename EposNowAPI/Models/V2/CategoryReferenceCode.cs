﻿using EposNowAPI.Architecture.Attributes;
using EposNowAPI.Architecture.BaseClasses;
using System;

namespace EposNowAPI.Models
{
    [EposModel("CategoryReferenceCode", "CategoryReferenceCodes")]
    public class CategoryReferenceCode : ModelBase
    {
        [EposID("CategoryReferenceCodeID")]
        public Int32 CategoryReferenceCodeID { get; set; }
        public Int32 CategoryID { get; set; }
        public Int32 AppID { get; set; }
        public String ReferenceCode { get; set; }
    }
}
