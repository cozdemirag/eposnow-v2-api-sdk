using EposNowAPI.Models;
using System;
using System.Collections.Generic;
using EposNowAPI.Architecture.Interfaces;

namespace EposNowAPI.Core.Responses
{
    public class TaxRatesResponse : EposNowResponse<TaxRate>, IEndPoint<TaxRate>
    {
        public TaxRatesResponse() : base() { }

        public TaxRatesResponse(Authenticator auth)
            : base(auth)
        {

        }

        public new TaxRate Create(TaxRate itemToCreate)
        {
            return base.Create<TaxRatesResponse>(itemToCreate);
        }

        public new TaxRate Update(TaxRate itemToUpdate)
        {
            return base.Update<TaxRatesResponse>(itemToUpdate);
        }

        public bool Delete(TaxRate item)
        {
            return base.Delete<TaxRatesResponse>(item);
        }

        public new IEnumerable<TaxRate> Get()
        {
            return base.Get<TaxRatesResponse>();
        }

        public new IEnumerable<TaxRate> Get(Int32 id)
        {
            return base.Get<TaxRatesResponse>(id);
        }

        public IEnumerable<TaxRate> Get(Int32 page, String where, String order)
        {
            return base.Get<TaxRatesResponse>(page, where, order);
        }

        public IEnumerable<TaxRate> Get<TKey1, TKey2>(System.Linq.Expressions.Expression<Func<TaxRate, TKey1>> whereClause = null, System.Linq.Expressions.Expression<Func<TaxRate, TKey2>> orderByClause = null)
        {
            return base.Get<TaxRatesResponse, TKey1, TKey2>(whereClause, orderByClause);
        }
    }
}
