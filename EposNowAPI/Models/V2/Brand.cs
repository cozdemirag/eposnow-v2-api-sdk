﻿using System;
using EposNowAPI.Architecture.Attributes;
using EposNowAPI.Architecture.BaseClasses;

namespace EposNowAPI.Models
{
    [EposModel("Brand", "Brands")]
    public class Brand : ModelBase
    {
        [EposID("BrandID")]
        public Int32 BrandID { get; set; }
        public String Name { get; set; }
        public String Description { get; set; }
    }
}
