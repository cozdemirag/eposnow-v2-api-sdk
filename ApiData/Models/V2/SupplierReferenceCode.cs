﻿using ApiData.Attributes;
using ApiData.BaseClasses;
using System;

namespace ApiData.Models
{
    [EposModel("SupplierReferenceCode", "SupplierReferenceCodes")]
    public class SupplierReferenceCode : ModelBase
    {
        [EposID("SupplierReferenceCodeID")]
        public Int32 SupplierReferenceCodeID { get; set; }
        public Int32 SupplierID { get; set; }
        public Int32 AppID { get; set; }
        public String ReferenceCode { get; set; }
    }
}
