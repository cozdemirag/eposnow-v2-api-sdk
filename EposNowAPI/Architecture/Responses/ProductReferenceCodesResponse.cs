using EposNowAPI.Architecture.BaseClasses;
using EposNowAPI.Core.Tools.QueryTools;
using EposNowAPI.Models;
using System;
using System.Collections.Generic;

namespace EposNowAPI.Core.Responses
{
    public sealed class ProductReferenceCodesResponse : ResponseBase<ProductReferenceCode>
    {
        public ProductReferenceCodesResponse() : base() { }

        public ProductReferenceCodesResponse(Authenticator auth)
            : base(auth)
        {
        }

        public ProductReferenceCode Create(ProductReferenceCode itemToCreate)
        {
            return base.Create<ProductReferenceCodesResponse>(itemToCreate);
        }

        public ProductReferenceCode Update(ProductReferenceCode itemToUpdate)
        {
            return base.Update<ProductReferenceCodesResponse>(itemToUpdate);
        }

        public Boolean Delete(ProductReferenceCode itemToDelete)
        {
            return base.Delete<ProductReferenceCodesResponse>(itemToDelete);
        }

        public Boolean Delete(Int32 id)
        {
            return base.Delete<ProductReferenceCodesResponse>(id);
        }

        public ProductReferenceCode Find(Int32 id)
        {
            return base.Find<ProductReferenceCodesResponse>(id);
        }

        public IEnumerable<ProductReferenceCode> Find(QueryParameters<ProductReferenceCode> parameters)
        {
            return base.Find<ProductReferenceCodesResponse>(parameters);
        }

        public IEnumerable<ProductReferenceCode> FindAll(QueryParameters<ProductReferenceCode> parameters)
        {
            return base.Find<ProductReferenceCodesResponse>(parameters);
        }
    }
}
