﻿using EposNowAPI.Architecture.Attributes;

namespace EposNowAPI.Models.Constants
{
    public enum PaymentStatus
    {
        [EnumValue("Hold")]
        Hold,
        [EnumValue("Pending")]
        Pending,
        [EnumValue("Ordered")]
        Ordered,
        [EnumValue("Complete")]
        Complete,
        [EnumValue("DeletedAdmin")]
        DeletedAdmin,
        [EnumValue("DeletedMag")]
        DeletedMagento,
        [EnumValue("DeletedTil")]
        DeletedTill,
        [EnumValue("DeletedPend")]
        DeletedPending,
        [EnumValue("APIDeleted")]
        DeletedAPI
    }
}
