using EposNowAPI.Architecture.BaseClasses;
using EposNowAPI.Architecture.Extensions;
using EposNowAPI.Architecture.Interfaces;
using EposNowAPI.Core.Responses;
using EposNowAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace EposNowAPI.Core.EndPoints
{
    public sealed class CustomerReferenceCodesEndPoint : EndPointBase<CustomerReferenceCodesEndPoint, CustomerReferenceCode> 
    {
        private CustomerReferenceCodesResponse Response { get; set; }

        public CustomerReferenceCodesEndPoint(Authenticator auth) : base()
        {
            Response = new CustomerReferenceCodesResponse(auth);
        }


        public override CustomerReferenceCode Find(Int32 id)
        {
            return Response.Find(id);
        }

        public override IEnumerable<CustomerReferenceCode> Find()
        {
            IEnumerable<CustomerReferenceCode> listToReturn = Response.Find(QueryParameters);
            QueryParameters.Reset();
            return listToReturn;
        }

        public override IEnumerable<CustomerReferenceCode> FindAll()
        {
			QueryParameters.GetAll = true;
            QueryParameters.PageNo = 1;
            IEnumerable<CustomerReferenceCode> listToReturn = Response.FindAll(QueryParameters);
            QueryParameters.Reset();
            return listToReturn;
        }

        public override CustomerReferenceCodesEndPoint Where(Expression<Func<CustomerReferenceCode, Boolean>> expression)
        {
            QueryParameters.Where.AddRange(expression.ToEposQueries());
            return this;
        }

        public override CustomerReferenceCodesEndPoint And(Expression<Func<CustomerReferenceCode, Boolean>> expression)
        {
            return Where(expression);
        }

        public override CustomerReferenceCodesEndPoint OrderBy<T>(Expression<Func<CustomerReferenceCode, T>> expression)
        {
            QueryParameters.Order = expression.ToEposOrderByQuery();
            return this;
        }

        public override CustomerReferenceCodesEndPoint OrderByDesc<T>(Expression<Func<CustomerReferenceCode, T>> expression)
        {
            QueryParameters.Order = expression.ToEposOrderByQuery(true);
            return this;
        }

        public override CustomerReferenceCodesEndPoint Page(Int32 pageNo)
        {
            QueryParameters.PageNo = pageNo;
            return this;
        }
    }
}
