﻿using System;
using EposNowAPI.Architecture.Attributes;
using EposNowAPI.Architecture.BaseClasses;

namespace EposNowAPI.Models
{
    [EposModel("TenderType", "TenderTypes")]
    public class TenderType : ModelBase
    {
        [EposID("TenderTypeID")]
        public Int32 TenderTypeID { get; set; }
        public Int32 TypeID { get; set; }
        public String Name { get; set; }
        public String Description { get; set; }
    }
}
