﻿<#@ template debug="false" hostspecific="true" language="C#" #>
<#@ assembly name="System.Core" #>
<#@ import namespace="System.Linq" #>
<#@ import namespace="System.Text" #>
<#@ import namespace="System.Collections.Generic" #>
<#@ output extension=".cs"#>
<#+ void GenerateEndPoint(List<String> CLASS_TYPE_NAME_LIST, String OUTPUT_PATH){
foreach(String CLASS_TYPE_NAME in CLASS_TYPE_NAME_LIST) { #>
using EposNowAPI.Core.Responses;
using EposNowAPI.Models;
using System.Collections.Generic;
using System;
using System.Linq.Expressions;
using System.Text.RegularExpressions;

namespace EposNowAPI.Core.EndPoints
{
    public class <#= CLASS_TYPE_NAME#>sEndPoint : IEndPoint<<#= CLASS_TYPE_NAME#>>
    {
        private <#= CLASS_TYPE_NAME#>sResponse Response;
        private Int32 pageNo;
        private String whereParams;
        private String orderParams;

        public <#= CLASS_TYPE_NAME#>sEndPoint(Authenticator auth)
        {
            Response = new <#= CLASS_TYPE_NAME#>sResponse(auth);
        }

        public <#= CLASS_TYPE_NAME#> Create(<#= CLASS_TYPE_NAME#> item)
        {
            return Response.Create(item);
        }

        public bool Delete(<#= CLASS_TYPE_NAME#> item)
        {
            return Response.Delete(item);
        }

        public IEnumerable<<#= CLASS_TYPE_NAME#>> Get()
        {
            IEnumerable<<#= CLASS_TYPE_NAME#>> listToReturn = Response.Get(pageNo, whereParams, orderParams);
            pageNo = 0;
            whereParams = "";
            orderParams = "";
            return listToReturn;
        }

        public IEnumerable<<#= CLASS_TYPE_NAME#>> Get(int id)
        {
            return Response.Get(id);
        }

        public IEnumerable<<#= CLASS_TYPE_NAME#>> Get<TKey1, TKey2>(System.Linq.Expressions.Expression<Func<<#= CLASS_TYPE_NAME#>, TKey1>> whereClause = null, System.Linq.Expressions.Expression<Func<<#= CLASS_TYPE_NAME#>, TKey2>> orderByClause = null)
        {
            return Response.Get<TKey1, TKey2>(whereClause, orderByClause);
        }

        public <#= CLASS_TYPE_NAME#> Update(<#= CLASS_TYPE_NAME#> item)
        {
            return Response.Update(item);
        }


        public <#= CLASS_TYPE_NAME#>sEndPoint Where(String param)
        {
            whereParams += param;
            return this;
        }

        public <#= CLASS_TYPE_NAME#>sEndPoint OrderBy(String param)
        {
            orderParams = "OrderBy=" + param;
            return this;
        }

        public <#= CLASS_TYPE_NAME#>sEndPoint OrderByDesc(String param)
        {
            orderParams = "OrderByDesc=" + param;
            return this;
        }

        public <#= CLASS_TYPE_NAME#>sEndPoint OrderByDesc<T>(Expression<Func<<#= CLASS_TYPE_NAME#>, T>> order)
        {
            orderParams = "OrderByDesc=" + new Regex(@"\s*").Replace(new Regex(@"(==)+").Replace(new Regex(@"([\(\)""]+)").Replace(new Regex(@"(" + order.Parameters[0].Name + @"{1}[\.]{1})").Replace(new Regex(@"(Convert\()+").Replace(order.Body.ToString(), "", 99), "", 99), "", 99), "=", 99), "", 99);
            return this;
        }

        public <#= CLASS_TYPE_NAME#>sEndPoint OrderBy<T>(Expression<Func<<#= CLASS_TYPE_NAME#>, T>> order)
        {
            orderParams = "OrderByDesc=" + new Regex(@"\s*").Replace(new Regex(@"(==)+").Replace(new Regex(@"([\(\)""]+)").Replace(new Regex(@"(" + order.Parameters[0].Name + @"{1}[\.]{1})").Replace(new Regex(@"(Convert\()+").Replace(order.Body.ToString(), "", 99), "", 99), "", 99), "=", 99), "", 99);
            return this;
        }

        public <#= CLASS_TYPE_NAME#>sEndPoint Page(Int32 page)
        {
            pageNo = page;
            return this;
        }

        public <#= CLASS_TYPE_NAME#>sEndPoint Where(params Expression<Func<<#= CLASS_TYPE_NAME#>, Boolean>>[] e)
        {
            whereParams = String.IsNullOrWhiteSpace(whereParams) ? "?" : whereParams;

            foreach (Expression<Func<<#= CLASS_TYPE_NAME#>, Boolean>> exp in e)
            {
                Func<<#= CLASS_TYPE_NAME#>, Boolean> daaa = exp.Compile();

                if (whereParams != "?" && !whereParams.EndsWith("&"))
                {
                    whereParams += "&";
                }

                if (exp.Body.ToString().Contains(".Contains("))
                {
                    whereParams += String.Format("{0}{1}{2}", "search=(", new Regex(@"\s*").Replace(new Regex(@"(==)+").Replace(new Regex(@"([\(\)""]+)").Replace(new Regex(@"(" + exp.Parameters[0].Name + @"{1}[\.]{1})").Replace(new Regex(@"(.Contains\()+").Replace(exp.Body.ToString(), "|like|", 99), "", 99), "", 99), "=", 99), "", 99), ")");
                }
                else if (exp.Body.ToString().Contains(".EndsWith("))
                {
                    whereParams += String.Format("{0}{1}{2}", "search=(", new Regex(@"\s*").Replace(new Regex(@"(==)+").Replace(new Regex(@"([\(\)""]+)").Replace(new Regex(@"(" + exp.Parameters[0].Name + @"{1}[\.]{1})").Replace(new Regex(@"(.EndsWith\()+").Replace(exp.Body.ToString(), "|EndsWith|", 99), "", 99), "", 99), "=", 99), "", 99), ")");
                }
                else if (exp.Body.ToString().Contains(".StartsWith("))
                {
                    whereParams += String.Format("{0}{1}{2}", "search=(", new Regex(@"\s*").Replace(new Regex(@"(==)+").Replace(new Regex(@"([\(\)""]+)").Replace(new Regex(@"(" + exp.Parameters[0].Name + @"{1}[\.]{1})").Replace(new Regex(@"(.StartsWith\()+").Replace(exp.Body.ToString(), "|StartsWith|", 99), "", 99), "", 99), "=", 99), "", 99), ")");
                }
                else if (exp.Body.ToString().Contains(">"))
                {
                    whereParams += String.Format("{0}{1}{2}", "search=(", new Regex(@"\s*").Replace(new Regex(@"(>)+").Replace(new Regex(@"([\(\)""]+)").Replace(new Regex(@"(" + exp.Parameters[0].Name + @"{1}[\.]{1})").Replace(new Regex(@"(Convert\()+").Replace(exp.Body.ToString(), "", 99), "", 99), "", 99), "|>|", 99), "", 99), ")");
                }
                else if (exp.Body.ToString().Contains(">="))
                {
                    whereParams += String.Format("{0}{1}{2}", "search=(", new Regex(@"\s*").Replace(new Regex(@"(>=)+").Replace(new Regex(@"([\(\)""]+)").Replace(new Regex(@"(" + exp.Parameters[0].Name + @"{1}[\.]{1})").Replace(new Regex(@"(Convert\()+").Replace(exp.Body.ToString(), "", 99), "", 99), "", 99), "|>=|", 99), "", 99), ")");
                }
                else if (exp.Body.ToString().Contains("<"))
                {
                    whereParams += String.Format("{0}{1}{2}", "search=(", new Regex(@"\s*").Replace(new Regex(@"(<)+").Replace(new Regex(@"([\(\)""]+)").Replace(new Regex(@"(" + exp.Parameters[0].Name + @"{1}[\.]{1})").Replace(new Regex(@"(Convert\()+").Replace(exp.Body.ToString(), "", 99), "", 99), "", 99), "|<|", 99), "", 99), ")");
                }
                else if (exp.Body.ToString().Contains("<="))
                {
                    whereParams += String.Format("{0}{1}{2}", "search=(", new Regex(@"\s*").Replace(new Regex(@"(<=)+").Replace(new Regex(@"([\(\)""]+)").Replace(new Regex(@"(" + exp.Parameters[0].Name + @"{1}[\.]{1})").Replace(new Regex(@"(Convert\()+").Replace(exp.Body.ToString(), "", 99), "", 99), "", 99), "|<=|", 99), "", 99), ")");
                }
                else if (exp.Body.ToString().Contains("=="))
                {
                    whereParams += new Regex(@"\s*").Replace(new Regex(@"(==)+").Replace(new Regex(@"([\(\)""]+)").Replace(new Regex(@"(" + exp.Parameters[0].Name + @"{1}[\.]{1})").Replace(new Regex(@"(Convert\()+").Replace(exp.Body.ToString(), "", 99), "", 99), "", 99), "=", 99), "", 99);
                }
                else
                {
                    throw new Exception(exp.Body + " has invalid operations for Epos Now API");
                }
            }
            return this;
        }
    }
}
<#+
SaveOutput(CLASS_TYPE_NAME + "sEndPoint.cs", OUTPUT_PATH); 
}}
#>