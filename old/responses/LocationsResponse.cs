using EposNowAPI.Models;
using System;
using System.Collections.Generic;
using EposNowAPI.Architecture.Interfaces;

namespace EposNowAPI.Core.Responses
{
    public class LocationsResponse : EposNowResponse<Location>, IEndPoint<Location>
    {
        public LocationsResponse() : base() { }

        public LocationsResponse(Authenticator auth)
            : base(auth)
        {

        }

        public new Location Create(Location itemToCreate)
        {
            return base.Create<LocationsResponse>(itemToCreate);
        }

        public new Location Update(Location itemToUpdate)
        {
            return base.Update<LocationsResponse>(itemToUpdate);
        }

        public bool Delete(Location item)
        {
            return base.Delete<LocationsResponse>(item);
        }

        public new IEnumerable<Location> Get()
        {
            return base.Get<LocationsResponse>();
        }

        public new IEnumerable<Location> Get(Int32 id)
        {
            return base.Get<LocationsResponse>(id);
        }

        public IEnumerable<Location> Get(Int32 page, String where, String order)
        {
            return base.Get<LocationsResponse>(page, where, order);
        }

        public IEnumerable<Location> Get<TKey1, TKey2>(System.Linq.Expressions.Expression<Func<Location, TKey1>> whereClause = null, System.Linq.Expressions.Expression<Func<Location, TKey2>> orderByClause = null)
        {
            return base.Get<LocationsResponse, TKey1, TKey2>(whereClause, orderByClause);
        }
    }
}
