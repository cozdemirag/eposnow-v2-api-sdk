using EposNowAPI.Architecture.BaseClasses;
using EposNowAPI.Architecture.Extensions;
using EposNowAPI.Architecture.Interfaces;
using EposNowAPI.Core.Responses;
using EposNowAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace EposNowAPI.Core.EndPoints
{
    public sealed class CategoriesEndPoint : EndPointBase<CategoriesEndPoint, Category> 
    {
        private CategoriesResponse Response { get; set; }

        public CategoriesEndPoint(Authenticator auth) : base()
        {
            Response = new CategoriesResponse(auth);
        }


        public override Category Find(Int32 id)
        {
            return Response.Find(id);
        }

        public override IEnumerable<Category> Find()
        {
            IEnumerable<Category> listToReturn = Response.Find(QueryParameters);
            QueryParameters.Reset();
            return listToReturn;
        }

        public override IEnumerable<Category> FindAll()
        {
			QueryParameters.GetAll = true;
            QueryParameters.PageNo = 1;
            IEnumerable<Category> listToReturn = Response.FindAll(QueryParameters);
            QueryParameters.Reset();
            return listToReturn;
        }

        public override CategoriesEndPoint Where(Expression<Func<Category, Boolean>> expression)
        {
            QueryParameters.Where.AddRange(expression.ToEposQueries());
            return this;
        }

        public override CategoriesEndPoint And(Expression<Func<Category, Boolean>> expression)
        {
            return Where(expression);
        }

        public override CategoriesEndPoint OrderBy<T>(Expression<Func<Category, T>> expression)
        {
            QueryParameters.Order = expression.ToEposOrderByQuery();
            return this;
        }

        public override CategoriesEndPoint OrderByDesc<T>(Expression<Func<Category, T>> expression)
        {
            QueryParameters.Order = expression.ToEposOrderByQuery(true);
            return this;
        }

        public override CategoriesEndPoint Page(Int32 pageNo)
        {
            QueryParameters.PageNo = pageNo;
            return this;
        }
    }
}
