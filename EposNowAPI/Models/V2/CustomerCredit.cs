﻿using System;
using EposNowAPI.Architecture.Attributes;

namespace EposNowAPI.Models
{
    [EposModel("CustomerCredit", "CustomerCredits")]
    public class CustomerCredit
    {
        [EposID("CustomerCreditID")]
        public Int32 CustomerID { get; set; }
        public Decimal CreditToAdd { get; set; }
        public Int32 StaffID { get; set; }
        public Int32 TenderTypeID { get; set; }
    }
}
