﻿using EposNowAPI.Architecture.Attributes;
using System;

namespace EposNowAPI.Models
{
    [EposModel("Booking", "Bookings")]
    public class Booking
    {
        [EposID("BookingID")]
        public Int32 BookingID { get; set; }
        public Int32 BookingItemID { get; set; }
        public Int32 BookingStatusID { get; set; }
        public Int32 StaffID { get; set; }
        public DateTime StartDateTime { get; set; }
        public Decimal Duration { get; set; }
        public String Notes { get; set; }
        public Int32 CreatedStaffID { get; set; }
        public DateTime CreatedDateTime { get; set; }
        public Int32 UpdatedStaffID { get; set; }
        public DateTime UpdatedDateTime { get; set; }
    }
}
