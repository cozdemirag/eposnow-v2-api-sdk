using EposNowAPI.Architecture.BaseClasses;
using EposNowAPI.Architecture.Extensions;
using EposNowAPI.Architecture.Interfaces;
using EposNowAPI.Core.Responses;
using EposNowAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace EposNowAPI.Core.EndPoints
{
    public sealed class TransactionsEndPoint : EndPointBase<TransactionsEndPoint, Transaction> 
    {
        private TransactionsResponse Response { get; set; }

        public TransactionsEndPoint(Authenticator auth) : base()
        {
            Response = new TransactionsResponse(auth);
        }


        public override Transaction Find(Int32 id)
        {
            return Response.Find(id);
        }

        public override IEnumerable<Transaction> Find()
        {
            IEnumerable<Transaction> listToReturn = Response.Find(QueryParameters);
            QueryParameters.Reset();
            return listToReturn;
        }

        public override IEnumerable<Transaction> FindAll()
        {
			QueryParameters.GetAll = true;
            QueryParameters.PageNo = 1;
            IEnumerable<Transaction> listToReturn = Response.FindAll(QueryParameters);
            QueryParameters.Reset();
            return listToReturn;
        }

        public override TransactionsEndPoint Where(Expression<Func<Transaction, Boolean>> expression)
        {
            QueryParameters.Where.AddRange(expression.ToEposQueries());
            return this;
        }

        public override TransactionsEndPoint And(Expression<Func<Transaction, Boolean>> expression)
        {
            return Where(expression);
        }

        public override TransactionsEndPoint OrderBy<T>(Expression<Func<Transaction, T>> expression)
        {
            QueryParameters.Order = expression.ToEposOrderByQuery();
            return this;
        }

        public override TransactionsEndPoint OrderByDesc<T>(Expression<Func<Transaction, T>> expression)
        {
            QueryParameters.Order = expression.ToEposOrderByQuery(true);
            return this;
        }

        public override TransactionsEndPoint Page(Int32 pageNo)
        {
            QueryParameters.PageNo = pageNo;
            return this;
        }
    }
}
