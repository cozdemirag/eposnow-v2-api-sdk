﻿using System;
using EposNowAPI.Architecture.Attributes;

namespace EposNowAPI.Models
{
    [EposModel("CustomerPoints", "CustomerPoints")]
    public class CustomerPoint
    {
        public Int32 CustomerID { get; set; }
        public Int32 PointsToAdd { get; set; }
        public Int32 StaffID { get; set; }
    }
}
